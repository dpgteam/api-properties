# DPG Properties
These classes can be used to work with Data retrieved from the DPG Platform API.
## Classes
* Properties: 	  Contains data and helper methods for interacting with a list of properties.
* Address:    	  Contains data and helper methods for interacting with a physical address.
* Property:       Contains data and helper methods for interacting with a single property.
* PropertyMarker: Contains data and helper methods for creating a property map marker.
* Agent:          Contains data and helper methods for interacting with a single agent.
* Auction:        Contains data and helper methods for interacting with a single auction.
* Inspection:     Contains data and helper methods for interacting with a single inspection.
* functions:	  Set of general helper functions.