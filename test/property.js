/* eslint no-eval: 0 */
import test from 'ava';
import Property from '../src/Property.js';
import Address from '../src/Address.js';
import data from './data/property';

test.beforeEach( t => {
    t.context.testProperty = new Property(data.dpg);
    t.context.rent = new Property(data.rent);
});
test(
  'test that empty Property does not exist',
  t => t.is(new Property({}).exists, false)
);
test(
  'test that dpg Property exists',
  t => t.is(t.context.testProperty.exists, true)
);
test(
  'test that dpg property id is "9392391967675"',
  t => t.is(t.context.testProperty.id, "9392391967675")
);
test(
  'test that rent property id is "xngess-1p2146"',
  t => t.is(t.context.rent.id, "xngess-1p2146")
);
test(
  'test that dpg property isResidential is true',
  t => t.is(t.context.testProperty.isResidential, true)
);
test(
  'test that empty {property} is null',
  t => t.is(new Property().property, null)
);
test(
  'test that rent {property} is not empty',
  t => t.is(Object.keys(t.context.rent.property).length > 0, true)
);
test(
  'test that rent {isSale} is false',
  t => t.is(t.context.rent.isSale, false)
);
test.todo('test that sale {isSale} is true');

test(
  'test that rent {isLease} is true',
  t => t.is(t.context.rent.isLease, true)
);
test.todo('test that sale {isLease} is false');

test(
  'test that rent {isCommercial} is false',
  t => t.is(t.context.rent.isCommercial, false)
);
test.todo('test that sale {isCommercial} is false');

test(
  'test that rent {category} is lease',
  t => t.is(t.context.rent.category.toLowerCase(), 'lease')
);
test.todo('test that sale {category} is sale');

test(
  'test that rent {type} is townhouse',
  t => t.is(t.context.rent.type.toLowerCase(), 'townhouse')
);
test.todo('test that sale {type} is type');

test(
  'test that rent {branch} is branch',
  t => t.is(t.context.rent.branch, 31)
);
test.todo('test that sale {branch} is 31');

test(
  'test that rent {status} is "curent"',
  t => t.is(t.context.rent.status, 'current')
);
test.todo('test that sale {status} is status');
test(
  'test that rent {headline} is "QUALITY"',
  t => t.is(t.context.rent.headline, 'QUALITY')
);
test.todo('test that sale {headline} is headline');
test(
  'test that rent {description} is "<p>Rental Description</p>"',
  t => t.is(t.context.rent.description, '<p>Rental Description</p>')
);
test.todo('test that sale {description} is description');
test(
  'test that rent {video} is null',
  t => t.is(t.context.rent.video, null)
);
test.todo('test that sale {video} is video');

test(
  'test that rent {coords.lat} is null',
  t => t.is(t.context.rent.coords.lat, null)
);
test(
  'test that rent {coords.lon} is null',
  t => t.is(t.context.rent.coords.lon, null)
);
test.todo('test that sale {coords.lat} is coords.lat');
test.todo('test that sale {coords.lon} is coords.lon');

test(
  'test that rent {address} is instanceof Address',
  t => t.is(t.context.rent.address instanceof Address, true)
);
test.todo('test that sale {address} is instanceof Address');
test(
  'test that rent {floorplan} is null',
  t => t.is(t.context.rent.floorplan, null)
);
test.todo('test that sale {floorplan} is floorplan');

test(
  'test that rent {images} is instanceof Array',
  t => t.is(t.context.rent.images instanceof Array, true)
);
test.todo('test that sale {images} is instanceof Array');

test(
  'test that rent {images[0]} is "http://sherylupton.com.au/lt-1-1P2146-146526009210546417-rsd.jpg"',
  t => t.is(t.context.rent.images[0], 'http://sherylupton.com.au/lt-1-1P2146-146526009210546417-rsd.jpg')
);
test.todo('test that sale {images[0]} is value');

test(
  'test that rent {auction._data} is null',
  t => t.is(t.context.rent.auction._data, null)
);
test.todo('test that sale {auction} is instanceof Auction');

test(
  'test that rent {images[0]} is "http://sherylupton.com.au/lt-1-1P2146-146526009210546417-rsd.jpg"',
  t => t.is(t.context.rent.images[0], 'http://sherylupton.com.au/lt-1-1P2146-146526009210546417-rsd.jpg')
);

test(
  'test that rent {hasInspections} is false',
  t => t.is(t.context.rent.hasInspections, false)
);
test.todo('test that sale {hasInspections} is hasInspections');

test(
  'test that rent {hasAuction} is false',
  t => t.is(t.context.rent.hasAuction, false)
);
test.todo('test that sale {hasAuction} is hasAuction');

test(
  'test that rent {hasInspectionsOrAuctions} is false',
  t => t.is(t.context.rent.hasInspectionsOrAuctions, false)
);
test.todo('test that sale {hasInspectionsOrAuctions} is hasInspectionsOrAuctions');

test.todo('test that sale {inspections}[0] is instanceof Inspection');

test.todo('test that sale {auction} is instanceof Auction');

test(
  'test that rent {hasInspectionsOrAuctions} is false',
  t => t.is(t.context.rent.hasInspectionsOrAuctions, false)
);

test(
  'test that rent {available} is true',
  t => t.is(t.context.rent.available, true)
);
test.todo('test that sale {available} is available');

test(
  'test that rent getFeature("Bedrooms") is "3"',
  t => t.is(t.context.rent.getFeature("bedrooms"), 3)
);
test.todo('test that sale {getFeature} is getFeature');

test(
  'test that rent {bedrooms} is 3',
  t => t.is(t.context.rent.bedrooms, 3)
);
test.todo('test that sale {bedrooms} is bedrooms');

test(
  'test that rent {bathrooms} is 1',
  t => t.is(t.context.rent.bathrooms, 1)
);
test.todo('test that sale {bathrooms} is bathrooms');

test(
  'test that rent {carspaces} is 1',
  t => t.is(t.context.rent.carspaces, 1)
);
test.todo('test that sale {carspaces} is carspaces');

test(
  'test that rent {weeklyRent} is 485',
  t => t.is(t.context.rent.weeklyRent, 485)
);

test.todo('test that sale {weeklyRent} is weeklyRent');

test(
  'test that rent {annualRent} is 25289',
  t => t.is(t.context.rent.annualRent, 25289)
);
test.todo('test that sale {annualRent} is annualRent');

test.todo('test that rent {rent} is rent');
test.todo('test that sale {rent} is rent');

test(
  'test that rent {price} is "$485"',
  t => t.is(t.context.rent.price, "$485")
);
test.todo('test that sale {price} is price');

test(
  'test that rent {hasAgents} is true',
  t => t.is(t.context.rent.hasAgents, true)
);
test.todo('test that sale {hasAgents} is hasAgents');

test(
  'test that rent {agents[0].name} is "Mitch Ladas"',
  t => t.is(t.context.rent.agents[0].name, "Mitch Ladas")
);
test.todo('test that sale {agents} is agents');

test(
  'test that rent {hasCategory(lease)} is true',
  t => t.is(t.context.rent.hasCategory('lease'), true)
);
test.todo('test that sale {hasCategory} is hasCategory');

test(
  'test that rent {hasType(townhouse)} is true',
  t => t.is(t.context.rent.hasType('townhouse'), true)
);
test.todo('test that sale {hasType} is hasType');

test(
  'test that rent {hasMinPrice(400)} is true',
  t => t.is(t.context.rent.hasMinPrice(400), true)
);
test.todo('test that sale {hasMinPrice} is hasMinPrice');

test(
  'test that rent {hasMaxPrice(500)} is true',
  t => t.is(t.context.rent.hasMaxPrice(500), true)
);
test.todo('test that sale {hasMaxPrice} is hasMaxPrice');

test.todo('test that rent {hasFeature} is hasFeature');
test.todo('test that sale {hasFeature} is hasFeature');







