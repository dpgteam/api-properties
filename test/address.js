import test from 'ava';
import Address from '../src/Address.js';
/**
 * Any auction date in the past should always
 * return null values for all properties.
 */
test.beforeEach(t => {
    t.context.nodata = new Address();
    t.context.empty = new Address({
        sub_number: null,
        lot_number: null,
        number    : null,
        street    : null,
        suburb    : null,
        state     : null,
        postcode  : null,
        lat       : null,
        lon       : null,
    });
    t.context.full = new Address({
        sub_number: '100',
        lot_number: '200',
        number    : '300',
        street    : 'Smith Street',
        suburb    : 'Jonestown',
        state     : 'VIC',
        postcode  : '3122',
        lat       : '2',
        lon       : '-2',
    });

});

test(
  'test that no data address does not exist',
   t => t.is(t.context.nodata.exists, false)
);
test(
  'test that non empty address exists',
   t => t.is(t.context.full.exists, true)
);
test(
  'test that non empty address full is "100/300 Smith Street, Jonestown 3122"',
   t => t.is(t.context.full.full, "100/300 Smith Street, Jonestown 3122")
);

test(
  'test that no data address sub_number is null',
   t => t.is(t.context.nodata.sub_number, null)
);
test(
  'test that non empty address sub_number is 100',
   t => t.is(t.context.full.sub_number, 100)
);

test(
  'test that no data address lot_number is null',
   t => t.is(t.context.nodata.lot_number, null)
);
test(
  'test that non empty address lot_number is 200',
   t => t.is(t.context.full.lot_number, 200)
);

test(
  'test that no data address number is null',
   t => t.is(t.context.nodata.number, null)
);
test(
  'test that non empty address number is 300',
   t => t.is(t.context.full.number, 300)
);
// street
test(
  'test that no data address street is null',
   t => t.is(t.context.nodata.street, null)
);
test(
  'test that non empty address street is Smith Street',
   t => t.is(t.context.full.street, 'Smith Street')
);
// suburb
test(
  'test that no data address suburb is null',
   t => t.is(t.context.nodata.suburb, null)
);
test(
  'test that non empty address suburb is Jonestown',
   t => t.is(t.context.full.suburb, 'Jonestown')
);
// state
test(
  'test that no data address state is null',
   t => t.is(t.context.nodata.state, null)
);
test(
  'test that non empty address state is VIC',
   t => t.is(t.context.full.state, 'VIC')
);
// postcode
test(
  'test that no data address postcode is null',
   t => t.is(t.context.nodata.postcode, null)
);
test(
  'test that non empty address poscode is 3122',
   t => t.is(t.context.full.postcode, '3122')
);
// lat
test(
  'test that no data address lat is null',
   t => t.is(t.context.nodata.lat, null)
);
test(
  'test that non empty address lat is 2',
   t => t.is(t.context.full.lat, '2')
);
// lon
test(
  'test that no data address lon is null',
   t => t.is(t.context.nodata.lon, null)
);
test(
  'test that non empty address lon is -2',
   t => t.is(t.context.full.lon, '-2')
);
// coords
test(
  'test that no data address coords is null',
   t => t.is(t.context.nodata.coords, null)
);
test(
  'test that non empty address coords.lat is 2, -2',
   (t) => {
      t.is(t.context.full.coords.lat, '2');
  }
);
test(
  'test that non empty address coords.lon is -2',
   (t) => {
      t.is(t.context.full.coords.lon, '-2');
  }
);
