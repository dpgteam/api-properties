import test from 'ava';
import Agent from '../src/Agent.js';
import data from './data/agent';
let agent = new Agent(data);

let expected = {
	id       : 2803,
	name     : 'Julie Gloster',
	mobile   : '0408 323 056',
	email    : 'jgloster@bradteal.com.au',
	photo_url: 'https://www.gravatar.com/avatar/541a721c731a51521717771b090fa96e.jpg?s=200&d=mm',
};

test('test that agent id is "' + expected.id +'"', t => {
	t.is(agent.id, expected.id);
});
test('test that agent name is "' + expected.name +'"', t => {
	t.is(agent.name, expected.name);
});
test('test that agent mobile is "' + expected.mobile +'"', t => {
	t.is(agent.mobile, expected.mobile);
});
test('test that agent email is "' + expected.email +'"', t => {
	t.is(agent.email, expected.email);
});
test('test that agent photo_url is "' + expected.photo_url +'"', t => {
	t.is(agent.photo_url, expected.photo_url);
});

