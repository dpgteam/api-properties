import test from 'ava';
import fn from '../src/functions.js';
import data from './data/inspection';
/** Test ucwords */
test(
  'test that inspection "nice sentence" is "Nice Sentence"',
  t => t.is(fn.ucwords('nice sentence'), 'Nice Sentence')
);
test(
  'test that inspection "NICE sentence" is "Nice Sentence"',
  t => t.is(fn.ucwords('NICE sentence'), 'Nice Sentence')
);
test(
  'test that inspection "NICE sentence" is "Nice Sentence"',
  t => t.is(fn.ucwords('niCe sentenCe'), 'Nice Sentence')
);
/** Test isAfterToday */
let dates = {
	inspection_past  : "25-Jun-2016 11:00AM to 11:30AM",
	inspection_future: "25-Jun-2020 11:00AM to 11:30AM",
	auction_past     : "2016-01-31 11:30:00",
	auction_future   : "2020-01-31 11:30:00",
};
let now = data.inspectionDateString(new Date());
test(
	'test that inspection date - ' + now + ' is on or after today',
	t => t.is(fn.isAfterToday(now), true)
);
test(
	'test that inspection date - ' + dates.inspection_past + ' is before today',
	t => t.is(fn.isAfterToday(dates.inspection_past), false)
);
test(
	'test that inspection date - ' + dates.inspection_future + ' is on or after today',
	t => t.is(fn.isAfterToday(dates.inspection_future), true)
);
test(
	'test that auction date - ' + dates.auction_past + ' is before today',
	t => t.is(fn.isAfterToday(dates.auction_past), false)
);
test(
	'test that auction date - ' + dates.auction_future + ' is on or after today',
	t => t.is(fn.isAfterToday(dates.inspection_future), true)
);
/** Test Weekdays */
/** Test Months */
test(
	'test that months() is an Array',
	t => t.is(fn.months().constructor, Array)
);
test(
	'test that months()[0] value is Jan',
	t => t.is(fn.months()[0], 'Jan')
);
test(
	'test that months()[1] value is Feb',
	t => t.is(fn.months()[1], 'Feb')
);
test(
	'test that months()[2] value is Mar',
	t => t.is(fn.months()[2], 'Mar')
);
test(
	'test that months()[3] value is Apr',
	t => t.is(fn.months()[3], 'Apr')
);
test(
	'test that months()[4] value is May',
	t => t.is(fn.months()[4], 'May')
);
test(
	'test that months()[5] value is Jun',
	t => t.is(fn.months()[5], 'Jun')
);
test(
	'test that months()[6] value is Jul',
	t => t.is(fn.months()[6], 'Jul')
);
test(
	'test that months()[7] value is Aug',
	t => t.is(fn.months()[7], 'Aug')
);
test(
	'test that months()[8] value is Sep',
	t => t.is(fn.months()[8], 'Sep')
);
test(
	'test that months()[9] value is Oct',
	t => t.is(fn.months()[9], 'Oct')
);
test(
	'test that months()[10] value is Nov',
	t => t.is(fn.months()[10], 'Nov')
);
test(
	'test that months()[11] value is Dec',
	t => t.is(fn.months()[11], 'Dec')
);
