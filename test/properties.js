import test from 'ava';
import Properties from '../src/Properties.js';
import data from './data/properties';

test.beforeEach( t => {
	t.context.properties = new Properties(data);
});

test(
	'test that non empty filters returns default values. ie: category = sale, ',
	(t) => {
		let expected = {
			category   : 'sale',
			type       :  null,
			price_max  :  0,
			price_min  :  0,
			bed        :  0,
			bath       :  0,
			car        :  0,
			auction    :  false,
			inspections:  false,
		};
		for ( let key of Object.keys(expected) ) {
			t.is(t.context.properties.filters[key], expected[key]);
		}
	}
);
test(
  	'test that empty {properties} does not have properties',
  	t => t.is(new Properties({}).hasProperties, false)
);
test(
  	'test that non empty {properties} list has properties',
  	t => t.is(t.context.properties.hasProperties, true)
);
test(
	'test that property type countss add up to 15',
	t => t.is(t.context.properties.typeCount.total, 15)
);
test(
	'test that default list category counts add up to 15',
	t => t.is(t.context.properties.categoryCount.total, 15)
);
test(
	'test that {properties} returns category count object',
	t => t.is(typeof t.context.properties.categoryCount, 'object')
);
test(
	'test that {properties} default filtered list total is 15',
	t => t.is(t.context.properties.list.length, 15)
);
test(
	'test that {properties} filter type set to "house" filtered list total is 10',
	t => {
		t.context.properties.filters = {type:'house'};
		t.is(t.context.properties.list.length, 10);
	}
);
test(
	'test that {properties} price count is ...',
	t => {
		let expected =[
			{ key: 900000, val: 2 },
			{ key: 600000, val: 1 },
			{ key: 400000, val: 2 },
			{ key: 300000, val: 3 },
			{ key: 200000, val: 5 },
			{ key: 100000, val: 2 },
		];
		expected.forEach( test => {
			t.is(t.context.properties.priceCount[test.key], test.val);
		});
	}
);
test(
	'test that {properties} price average is ...',
	t => {
		t.is(t.context.properties.priceAverage, 440000);
	}
);
test.todo('test filters: category, type, price, rent, features, inpsections');
