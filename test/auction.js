import test from 'ava';
import Auction from '../src/Auction.js';
import data from './data/auction';
/**
 * Any auction date in the past should always
 * return null values for all properties.
 */
let past = new Auction(data.past);
test(
	'test that ' + data.past + ' auction month is null',
	t => t.is(past.month, null)
);
test(
	'test that ' + data.past + ' auction weekday is null',
	t => t.is(past.weekday, null)
);
test(
	'test that ' + data.past + ' auction weekday is null',
	t => t.is(past.weekday, null)
);
test(
	'test that ' + data.past + ' auction am is null',
	t => t.is(past.am, null)
);
test(
	'test that ' + data.past + ' auction hour is null',
	t => t.is(past.hour, null)
);
test(
	'test that ' + data.past + ' auction minute is null',
	t => t.is(past.minute, null)
);
test(
	'test that ' + data.past + ' auction date is null',
	t => t.is(past.date, null)
);

let future_am = new Auction(data.future_am);
test(
	'test that ' + data.future_am + ' auction month is jan',
	t => t.is(future_am.month, 'Jan')
);
test(
	'test that ' + data.future_am + ' auction weekday is Wednesday',
	t => t.is(future_am.weekday, 'Wednesday')
);
test(
	'test that ' + data.future_am + ' auction am is am',
	t => t.is(future_am.am, 'am')
);
test(
	'test that ' + data.future_am + ' auction hour is 11',
	t => t.is(future_am.hour, 11)
);
test(
	'test that ' + data.future_am + ' auction minute is "30"',
	t => t.is(future_am.minute, "30")
);
test(
	'test that ' + data.future_am + ' auction date is 31',
	t => t.is(future_am.date, 31)
);
let future_pm = new Auction(data.future_pm);
test(
	'test that ' + data.future_pm + ' auction am is pm',
	t => t.is(future_pm.am, 'pm')
);
test(
	'test that ' + data.future_pm + ' auction hour is 1',
	t => t.is(future_pm.hour, 1)
);
