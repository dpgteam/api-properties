let Data = [
  {
    id: 175082,
    property_unique_id: "26606-11092377",
    agency_id: 62,
    branch_id: 190,
    country_id: 0,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "58a Marlow Street, Wembley, Wa 6014, Australia",
    authority: "exclusive",
    headline: "BRAND NEW AND READY FOR YOU!",
    description: "Home opens\nSaturday 15th April 10:00am-11:00am\nMonday 17th April 12:45pm-2:00pm (extra-long home open)\n\nBRAND NEW custom built RIVERSTONE street front home on a well located 528m2 block! \n\nAMAZING residence built to please the fussiest of buyers who are seeking low maintenance living without having to compromise on SPACE, quality or PRIVACY. \n\nThe cleverly thought out design offers a home that flows exceptionally well and is extremely functional and flexible easily satisfying the needs of a modern growing family.\n\nPerched equal distance to the coast and city, Wembley offers a variety of lifestyle choicesWalk Lake Monger, cycle to the beach or enjoy the many cafes / restaurants / bars that are all on your doorstep. \n\nOnly one street to Wembley Primary School and sitting in the sought after Churchlands Senior High School catchment area, don't miss YOUR chance to secure this OPPORTUNITY! \n\nWHY YOU SHOULD BUY ME:\n\nDOWNSTAIRS:\n\n-Our gourmet kitchen is designed around performance, convenience and looks meaning you'll never miss a beat no matter how big or small the occasion.  Fitted with Miele appliances such as a 4 burner gas stove, dishwasher, oven, rangehood, stone bench tops, tiled splashbacks, double stainless steel sunken sinks, breakfast bar, walk in butler's pantry and quality cabinets with an abundance of storage\n\n-Open plan living and dining cleverly designed to flow from the kitchen through to the backyard and is perfect for entertaining and hosting the biggest of dinner parties\n\n-Perfect sized backyard with a paved alfresco for entertaining under the main roof and a manageable grassed area perfect for kids, pets or sliding in a pool. \n\n-4th bedroom / optional 2nd master bedroom with built in robes and direct access to the 3rd bathroom great for downsizers, teenagers, or extended family and friends that pop in from time to time\n\n-3rd bathroom with hobless shower, plenty of bench space, quality fixtures and fittings, soft close cabinetry and toilet\n\n-MASSIVE theatre room great for that much needed separation \n\n-BIG laundry with linen cupboard, plenty of storage and access to the separate drying quarter \n\n-Study nook with data and power points \n\n-Double auto garage with shoppers' entrance and direct access through to the backyard\n\n-Handy under stair storage \n\nUPSTAIRS\n\n-Enormous master bedroom with a walk in robe and private ensuite finished with his and hers vanities, stone tops, soft close cabinets, hobless shower, quality fixtures and fittings and a separate toilet \n\n-Bedrooms 2 and 3 both with built in robes and generous in size\n\n-2nd family bathroom finished with quality fixtures and fittings, bath tub, hobless shower, soft close cabinets and stone bench tops \n\n-Linen cupboard \n\n-2nd study nook \n\nEXTRAS: Rinnai instant gas hot water system, LED down-lights through most rooms, LG reverse cycle ducted zoned air conditioning throughout, Jason aluminium windows, colorbond roof and gutters, r3.5 batts insulation throughout, skirtings and cornice throughout, TV and phone/data points, roller blinds, reticulated off the mains.\n\nAPPROXIMATE DISTANCES TO: \nWembley Primary school: 95m\nRutter Park: 250m \nCambridge Street cafes: 600m\nHerdsman Lake: 620m\nHerdsman Fresh: 670m\nWembley 24hour IGA: 690m\nLake Monger: 1.2km\nFloreat Forum: 1.5km \nChurchlands High School: 2.5km\nPerth CBD: 4.4km\nFloreat Beach: 5km\n\nBUILT: 2017 \nBUILDER: Riverstone Custom Homes \nLAND: 528m2 approx. \nHOUSE: 350m2 approx. \nWATER RATES: TBA\nCOUNCIL RATES: TBA",
    video_link: null,
    lat: -31.9348691,
    lon: 115.8081991,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "58A",
    address_street: "Marlow Street",
    address_suburb: "WEMBLEY",
    address_suburb_display: "yes",
    address_state: "WA",
    address_region: null,
    address_postcode: "6014",
    address_country: "Australia",
    rea_agency_id: "26606",
    rea_listing_unique_id: "11092377",
    price: 1250000,
    price_display: "yes",
    price_min: 1250000,
    price_max: 1250000,
    price_view: "Expressions of Interest",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 19:31:00",
    created_at: "2017-04-13 22:01:02",
    updated_at: "2017-04-13 22:01:02",
    deleted_at: null,
    agency: {
      id: 62,
      name: "Altitude Real Estate",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183207,
        property_id: 175082,
        user_id: 1634,
        created_at: "2017-04-13 22:01:02",
        updated_at: "2017-04-13 22:01:02",
        agent: {
          id: 1634,
          name: "Paul Tonich",
          email: "paul-tonich-altitude-real-estate@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/bbf2bde0548db1b0e715e85758f34701.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0478 180 765",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 21:42:20",
          updated_at: "2016-07-08 21:42:20",
          email_aliases: [
            {
              id: 1656,
              user_id: 1634,
              email: "paul@altitudere.com.au",
              created_at: "2016-07-08 21:42:20",
              updated_at: "2016-07-08 21:42:20"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      {
        id: 96142,
        property_id: 175082,
        building_attribute: "area",
        building_attribute_value: "350",
        building_attribute_unit: "squareMeter",
        building_attribute_of: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      }
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483946,
        property_id: 175082,
        feature_name: "Bedrooms",
        feature_value: "4",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483947,
        property_id: 175082,
        feature_name: "Bathrooms",
        feature_value: "3",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483948,
        property_id: 175082,
        feature_name: "Garages",
        feature_value: "2",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483949,
        property_id: 175082,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483950,
        property_id: 175082,
        feature_name: "Air Conditioning",
        feature_value: "yes",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483951,
        property_id: 175082,
        feature_name: "Pool",
        feature_value: "no",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483952,
        property_id: 175082,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483953,
        property_id: 175082,
        feature_name: "Built-in Wardrobes",
        feature_value: "yes",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483954,
        property_id: 175082,
        feature_name: "Close To Schools",
        feature_value: "yes",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483955,
        property_id: 175082,
        feature_name: "Close To Shops",
        feature_value: "yes",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483956,
        property_id: 175082,
        feature_name: "Close To Transport",
        feature_value: "yes",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 25483957,
        property_id: 175082,
        feature_name: "Garden",
        feature_value: "yes",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303215,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/5732-03c.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303216,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/26718-10.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303217,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/26803-09.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303218,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/27212-11-HDR.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303219,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/31549-13.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303220,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/29606-14a.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303221,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/24891-15.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303222,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/19162-21.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303223,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/27368-18.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303224,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/29862-19-HDR.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303225,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/19586-38-HDR.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303226,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/30320-39a.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303227,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/21806-40a.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303228,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/31777-23.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303229,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/21376-25.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303230,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/29922-27.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303231,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/965-28-HDR.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303232,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/27678-29.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303233,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/30111-30.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303234,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/19432-31a.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303235,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/27288-34.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303236,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/27375-36.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303237,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/28906-WembleyPrimary2.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303238,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/24679-Beach.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303239,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/26823-LakeMonger.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 3303240,
        property_id: 175082,
        url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/16237-PerthCBD.jpg",
        format: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      }
    ],
    property_inspection_times: [
      {
        id: 1014252,
        property_id: 175082,
        inspection_time: "15-Apr-2017 10:00AM to 11:00AM",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      },
      {
        id: 1014253,
        property_id: 175082,
        inspection_time: "17-Apr-2017 12:45PM to 02:00PM",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      }
    ],
    property_land_details: [
      {
        id: 489993,
        property_id: 175082,
        land_attribute: "area",
        land_attribute_value: "528",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      }
    ],
    property_objects: [
      {
        id: 397723,
        property_id: 175082,
        object_type: "floorplan",
        object_url: "http://mydesktop.aunz.s3-website-ap-southeast-2.amazonaws.com/altitude/photos/18603-Floorplans.jpg",
        created_at: "2017-04-13 22:01:04",
        updated_at: "2017-04-13 22:01:04"
      }
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175078,
    property_unique_id: "11911-sge7866",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "land",
    category: null,
    property_status: "current",
    address_full: "21 Nash St, Kapunda SA 5373, Australia",
    authority: "exclusive",
    headline: "Amazing Results! Under Contract!",
    description: "Looking for a country lifestyle with the opportunity to build your dream home? Then this is the block for you!\n\nLarge allotment of 1725m&#xb2;\n\nMore than enough room for a pool, shed or a workshop. \n\nPower and water is to the front of the property.\n\nKapunda is a 25 minute drive from Gawler and with the northern express way, you can be in town in no time.\n\nOnly a short distance to the main street and close to schools and hospital. \n\nWhat better location could you ask for?\n\nWith services to the property, ring today to avoid disappointment.\n\nCassandra Washington 0403 167 458",
    video_link: null,
    lat: -34.3308956,
    lon: 138.9182073,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "21",
    address_street: "Nash Street",
    address_suburb: "KAPUNDA",
    address_suburb_display: "yes",
    address_state: "SA",
    address_region: null,
    address_postcode: "5373",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "SGE7866",
    price: 45000,
    price_display: "yes",
    price_min: 45000,
    price_max: 45000,
    price_view: "$45,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "yes",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: "Residential",
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:06:01",
    created_at: "2017-04-13 21:02:28",
    updated_at: "2017-04-13 21:02:29",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183203,
        property_id: 175078,
        user_id: 934,
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28",
        agent: {
          id: 934,
          name: "Cassandra Washington",
          email: "cassandra-washington-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/c0e35e9f22dd850b7b2ebb7d73aa9995.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0403 167 458",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:28:08",
          updated_at: "2016-07-08 17:28:08",
          email_aliases: [
            {
              id: 945,
              user_id: 934,
              email: "cassandra.washington@harcourts.com.au",
              created_at: "2016-07-08 17:28:08",
              updated_at: "2016-07-08 17:28:08"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303139,
        property_id: 175078,
        url: "https://images.realestateview.com.au/pics/134/10991134ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303140,
        property_id: 175078,
        url: "https://images.realestateview.com.au/pics/134/10991134bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303141,
        property_id: 175078,
        url: "https://images.realestateview.com.au/pics/134/10991134cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303142,
        property_id: 175078,
        url: "https://images.realestateview.com.au/pics/134/10991134dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489987,
        property_id: 175078,
        land_attribute: "area",
        land_attribute_value: "1725",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175079,
    property_unique_id: "11911-tsh4872",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "2 Beattie Ave, Bicheno TAS 7215, Australia",
    authority: "exclusive",
    headline: "Affordable Living At Bicheno",
    description: "With \"good bones\" this three bedroom home is in a very handy location and within walking distance to the beach. In ever so sunny Bicheno, on Tassies spectacular east coast this home will be an excellent rental/weekender or great start for those entering the property market. The open plan kitchen/dining /living looks out to the deck through sliding glass doors, and is winter warm with the reverse cycle air conditioner. The kitchen offers upright stove and could use some loving. \nThe bathroom is in good order and offers bath with shower over, vanity and w/c.\nThe yard is a blank canvas for you to make your own, fully fenced it offers one garage and a dog/chook yard.\nCall Harcourts St Helens to inspect.",
    video_link: null,
    lat: -41.8753098,
    lon: 148.3006886,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "2",
    address_street: "Beattie Avenue",
    address_suburb: "BICHENO",
    address_suburb_display: "yes",
    address_state: "TAS",
    address_region: null,
    address_postcode: "7215",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "TSH4872",
    price: 220000,
    price_display: "yes",
    price_min: 220000,
    price_max: 220000,
    price_view: "$220,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:06:05",
    created_at: "2017-04-13 21:02:28",
    updated_at: "2017-04-13 21:02:29",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183204,
        property_id: 175079,
        user_id: 1043,
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28",
        agent: {
          id: 1043,
          name: "Kate Chapple",
          email: "kate-chapple-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/e2ca2dc27c27ae115757f9dff3410b20.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0409978617",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:30:22",
          updated_at: "2016-07-08 17:30:22",
          email_aliases: [
            {
              id: 1054,
              user_id: 1043,
              email: "kate.chapple@harcourts.com.au",
              created_at: "2016-07-08 17:30:22",
              updated_at: "2016-07-08 17:30:22"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      {
        id: 96139,
        property_id: 175079,
        building_attribute: "area",
        building_attribute_value: "86",
        building_attribute_unit: "squareMeter",
        building_attribute_of: null,
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      }
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483872,
        property_id: 175079,
        feature_name: "Bedrooms",
        feature_value: "3",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483873,
        property_id: 175079,
        feature_name: "Bathrooms",
        feature_value: "1",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483874,
        property_id: 175079,
        feature_name: "Garages",
        feature_value: "1",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483875,
        property_id: 175079,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483876,
        property_id: 175079,
        feature_name: "Open Spaces",
        feature_value: "3",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483877,
        property_id: 175079,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483878,
        property_id: 175079,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483879,
        property_id: 175079,
        feature_name: "Air Conditioning",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483880,
        property_id: 175079,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483881,
        property_id: 175079,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483882,
        property_id: 175079,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483883,
        property_id: 175079,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483884,
        property_id: 175079,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483885,
        property_id: 175079,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483886,
        property_id: 175079,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483887,
        property_id: 175079,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483888,
        property_id: 175079,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483889,
        property_id: 175079,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483890,
        property_id: 175079,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483891,
        property_id: 175079,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483892,
        property_id: 175079,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483893,
        property_id: 175079,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483894,
        property_id: 175079,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483895,
        property_id: 175079,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483896,
        property_id: 175079,
        feature_name: "Property Type: House\nHouse Style: Contemporary\nGaraging / Carparking: Single Lock-up",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303143,
        property_id: 175079,
        url: "https://images.realestateview.com.au/pics/135/10991135ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303144,
        property_id: 175079,
        url: "https://images.realestateview.com.au/pics/135/10991135bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303145,
        property_id: 175079,
        url: "https://images.realestateview.com.au/pics/135/10991135cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303146,
        property_id: 175079,
        url: "https://images.realestateview.com.au/pics/135/10991135dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303147,
        property_id: 175079,
        url: "https://images.realestateview.com.au/pics/135/10991135ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303148,
        property_id: 175079,
        url: "https://images.realestateview.com.au/pics/135/10991135fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303149,
        property_id: 175079,
        url: "https://images.realestateview.com.au/pics/135/10991135gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303150,
        property_id: 175079,
        url: "https://images.realestateview.com.au/pics/135/10991135hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303151,
        property_id: 175079,
        url: "https://images.realestateview.com.au/pics/135/10991135ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489988,
        property_id: 175079,
        land_attribute: "area",
        land_attribute_value: "680",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175080,
    property_unique_id: "11911-vng27597",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "land",
    category: null,
    property_status: "current",
    address_full: "2 Montreal Ave, Corio VIC 3214, Australia",
    authority: "exclusive",
    headline: "Developers/Investors Alert",
    description: "Great opportunity to acquire this well located parcel of land. With plans and permits for a 2 bedroom plus garage townhouse already in place and its own street frontage with power, water and sewer works completed this project is ready to go. If you are a developer, builder or investor wanting to break into the development sector of real estate then this site is the one for you.\nClose to Corio Shopping Centre, local schools, public transport. Easy access to the Princes Freeway and Geelong Ring Road makes commuting a breeze. This one won't last long. Enquire today.\n\nDue Diligence checklist are available at www.consumer.vic.gov.au/duediligencechecklist",
    video_link: null,
    lat: -38.0767218,
    lon: 144.3626084,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "2A",
    address_street: "Montreal Avenue",
    address_suburb: "CORIO",
    address_suburb_display: "yes",
    address_state: "VIC",
    address_region: null,
    address_postcode: "3214",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "VNG27597",
    price: 101000,
    price_display: "yes",
    price_min: 101000,
    price_max: 101000,
    price_view: "Price by Negotiation $101,000 - $115,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: "Residential",
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:06:10",
    created_at: "2017-04-13 21:02:28",
    updated_at: "2017-04-13 21:02:30",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183205,
        property_id: 175080,
        user_id: 1080,
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28",
        agent: {
          id: 1080,
          name: "Nick De Stefano",
          email: "nick-de-stefano-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/a8780ae2a6c9388dba8fd51d3b451efc.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0431 230 124",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:31:25",
          updated_at: "2016-07-08 17:31:25",
          email_aliases: [
            {
              id: 1091,
              user_id: 1080,
              email: "nick.destefano@harcourts.com.au",
              created_at: "2016-07-08 17:31:25",
              updated_at: "2016-07-08 17:31:25"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303152,
        property_id: 175080,
        url: "https://images.realestateview.com.au/pics/136/10991136ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303153,
        property_id: 175080,
        url: "https://images.realestateview.com.au/pics/136/10991136bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303154,
        property_id: 175080,
        url: "https://images.realestateview.com.au/pics/136/10991136cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303155,
        property_id: 175080,
        url: "https://images.realestateview.com.au/pics/136/10991136dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489989,
        property_id: 175080,
        land_attribute: "area",
        land_attribute_value: "265.5",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175081,
    property_unique_id: "11911-vng27668",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "11 Moa St, Norlane VIC 3214, Australia",
    authority: "exclusive",
    headline: "Close to Everything for Next to Nothing",
    description: "Located close to Library, Pool and Corio shopping Centre, medical facilities and with easy access to princes Highway makes this property an ideal investment. Comprising of 3 generous bedrooms, separate lounge and combined kitchen meals area and separate laundry. Outside offers large back yard for the kids to play and a large garage. Make sure you book your inspection today  \n\nDue Diligence checklist are available at www.consumer.vic.gov.au/duediligencechecklist",
    video_link: null,
    lat: -38.08537,
    lon: 144.3548805,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "11",
    address_street: "Moa Street",
    address_suburb: "NORLANE",
    address_suburb_display: "yes",
    address_state: "VIC",
    address_region: null,
    address_postcode: "3214",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "VNG27668",
    price: 269000,
    price_display: "yes",
    price_min: 269000,
    price_max: 269000,
    price_view: "Price by Negotiation $269,000 - $289,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:06:12",
    created_at: "2017-04-13 21:02:28",
    updated_at: "2017-04-13 21:02:30",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183206,
        property_id: 175081,
        user_id: 1080,
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28",
        agent: {
          id: 1080,
          name: "Nick De Stefano",
          email: "nick-de-stefano-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/a8780ae2a6c9388dba8fd51d3b451efc.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0431 230 124",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:31:25",
          updated_at: "2016-07-08 17:31:25",
          email_aliases: [
            {
              id: 1091,
              user_id: 1080,
              email: "nick.destefano@harcourts.com.au",
              created_at: "2016-07-08 17:31:25",
              updated_at: "2016-07-08 17:31:25"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483897,
        property_id: 175081,
        feature_name: "Bedrooms",
        feature_value: "3",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483898,
        property_id: 175081,
        feature_name: "Bathrooms",
        feature_value: "1",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483899,
        property_id: 175081,
        feature_name: "Garages",
        feature_value: "2",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483900,
        property_id: 175081,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483901,
        property_id: 175081,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483902,
        property_id: 175081,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483903,
        property_id: 175081,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 25483904,
        property_id: 175081,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483905,
        property_id: 175081,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483906,
        property_id: 175081,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483907,
        property_id: 175081,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483908,
        property_id: 175081,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483909,
        property_id: 175081,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483910,
        property_id: 175081,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483911,
        property_id: 175081,
        feature_name: "Air Conditioning",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483912,
        property_id: 175081,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483913,
        property_id: 175081,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483914,
        property_id: 175081,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483915,
        property_id: 175081,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483916,
        property_id: 175081,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483917,
        property_id: 175081,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483918,
        property_id: 175081,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483919,
        property_id: 175081,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483920,
        property_id: 175081,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 25483921,
        property_id: 175081,
        feature_name: "Property Condition: Fair\nProperty Type: House",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303156,
        property_id: 175081,
        url: "https://images.realestateview.com.au/pics/137/10991137ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 3303157,
        property_id: 175081,
        url: "https://images.realestateview.com.au/pics/137/10991137bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 3303158,
        property_id: 175081,
        url: "https://images.realestateview.com.au/pics/137/10991137cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 3303159,
        property_id: 175081,
        url: "https://images.realestateview.com.au/pics/137/10991137dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      },
      {
        id: 3303160,
        property_id: 175081,
        url: "https://images.realestateview.com.au/pics/137/10991137ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489990,
        property_id: 175081,
        land_attribute: "area",
        land_attribute_value: "577",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:29",
        updated_at: "2017-04-13 21:02:29"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175076,
    property_unique_id: "11911-tne6885",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "AcreageSemi-rural",
    property_status: "current",
    address_full: "36 Cape Portland Rd, Gladstone TAS 7264, Australia",
    authority: "exclusive",
    headline: "Inspired Tree Change or Country Retreat on 1.33ha",
    description: "Dreaming of a country getaway?  This beautiful property located in the North East has amazing views, a large collection of outbuildings and land suitable for horses or running some sheep or cattle.\n- fully fenced 1.33ha in picturesque, quiet rural location\n- inside the 1930s steel clad character cottage are 3 bedrooms, open plan living and efficient wood heater\n- property has two bathrooms, one outside\n- outbuildings include a secure 3 bay garage (9m x 6m), newly refurbished sleep out, original two bay shelter and separate large workshop and storage.\n- Located next to the Ringarooma River\n- Gladstone is conveniently located 28km to Ansons Bay, 32 km to Tomahawk, 60km to Bridport, 80km to St Helens or Scottsdale & around 25 minutes by car to Derby",
    video_link: null,
    lat: -40.9536101,
    lon: 148.0133587,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "36",
    address_street: "Cape Portland Road",
    address_suburb: "GLADSTONE",
    address_suburb_display: "yes",
    address_state: "TAS",
    address_region: null,
    address_postcode: "7264",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "TNE6885",
    price: 175000,
    price_display: "yes",
    price_min: 175000,
    price_max: 175000,
    price_view: "$175,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:05:39",
    created_at: "2017-04-13 21:02:27",
    updated_at: "2017-04-13 21:02:29",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183201,
        property_id: 175076,
        user_id: 954,
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27",
        agent: {
          id: 954,
          name: "Andrew Bennett",
          email: "andrew-bennett-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/26a5b6b83eddccda3f8b84bd4bfd113d.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0427 224 155",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:28:26",
          updated_at: "2016-07-08 17:28:26",
          email_aliases: [
            {
              id: 965,
              user_id: 954,
              email: "andrew.bennett@harcourts.com.au",
              created_at: "2016-07-08 17:28:26",
              updated_at: "2016-07-08 17:28:26"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      {
        id: 96138,
        property_id: 175076,
        building_attribute: "area",
        building_attribute_value: "115",
        building_attribute_unit: "squareMeter",
        building_attribute_of: null,
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483822,
        property_id: 175076,
        feature_name: "Bedrooms",
        feature_value: "4",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483823,
        property_id: 175076,
        feature_name: "Bathrooms",
        feature_value: "2",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483824,
        property_id: 175076,
        feature_name: "Garages",
        feature_value: "2",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483825,
        property_id: 175076,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483826,
        property_id: 175076,
        feature_name: "Open Spaces",
        feature_value: "4",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483827,
        property_id: 175076,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483828,
        property_id: 175076,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483829,
        property_id: 175076,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483830,
        property_id: 175076,
        feature_name: "Air Conditioning",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483831,
        property_id: 175076,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483832,
        property_id: 175076,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483833,
        property_id: 175076,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483834,
        property_id: 175076,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483835,
        property_id: 175076,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483836,
        property_id: 175076,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483837,
        property_id: 175076,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483838,
        property_id: 175076,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483839,
        property_id: 175076,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483840,
        property_id: 175076,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483841,
        property_id: 175076,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483842,
        property_id: 175076,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483843,
        property_id: 175076,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483844,
        property_id: 175076,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483845,
        property_id: 175076,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483846,
        property_id: 175076,
        feature_name: "Land Area: Land Area Ha: 1.33\nProperty Type: Acreage/semi Rural",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303108,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303109,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303110,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303111,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303112,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303113,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303114,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303115,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303116,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303117,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303118,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303119,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303120,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303121,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132nx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303122,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132ox.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303123,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132px.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303124,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132qx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303125,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132rx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303126,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132sx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303127,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132tx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303128,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132ux.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303129,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132vx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303130,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132wx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303131,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132xx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303132,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132yx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303133,
        property_id: 175076,
        url: "https://images.realestateview.com.au/pics/132/10991132zx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489986,
        property_id: 175076,
        land_attribute: "area",
        land_attribute_value: "13300",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175077,
    property_unique_id: "11911-scl9489",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "50 Harvey St, Collinswood SA 5081, Australia",
    authority: "exclusive",
    headline: "Hit new highs on Harvey Street.",
    description: "Ready to build, double level designer home. Buy \"off-plan\" and save!\n\nLooking for family style a stone's throw from the city? A modern makeover is evolving on Harvey Street making way for your new boutique, double storey home metres from leafy D'Erlanger Avenue.\n \nThe blueprints are done and ready to come alive. The stylish render and brick aesthetic proposed by Oxford Architects is geared to deliver three glorious bedrooms, two bathrooms, multiple entertaining options plus side by side garaging with internal access set over a compact 295sqm allotment.\n \nClean lines and contemporary themes are exclusively yours, but act quickly - buy off the plan and put your stamp duty savings to better use.\n \nSavour exciting new beginnings in the characterful enclave of Collinswood. Landscaping and ornamental planting will greet you at day's end - a brilliant bonus to an outstanding house and land offering.\n \nThe ground floor comprises of a master bedroom to the front of the home with walk-in robe and en-suite, a central hallway leading past a guest powder room towards the entertaining haven. \n \nSought after open plan kitchen, dining and living areas salute the north-facing rear garden and share the landscaped grounds with a relaxed, all-weather alfresco dining space.\n \nUpstairs concludes with a laid-back chill zone. Two spacious bedrooms make exclusive use of a family bathroom and separate w.c. while a practical TV room/teen retreat spans the communal landing atop the stairs. To the rear, the games room offers flexibility to model itself around your lifestyle.\n \nExpect location envy from your friends - the lush green parkland of Pash Reserve with BBQ and play equipment will recharge the kid's batteries as will the nearby Linear Park trails.\n \nRetail therapy, curb-side caf&#xe9;s and premier schools in surrounding Walkerville, North Adelaide, and Prospect mean everything is literally at your fingertips. There is nothing more satisfying than a short commute to and from the city knowing that your home is turn-key ready to enjoy.\n \nFamily buyers, busy executives or right-sizers if you crave an urban, city fringe feel with a family flavour, set your sights on Harvey Street today.\n\nWhy you will love this home:\nStylish new construction on 295sqm (approx.)\nThree double sized bedrooms with built-ins\nFamily friendly neighbourhood\nClose to North Adelaide, Walkerville & Prospect precincts\n6kms to the CBD \n\nFeatures: \nAppliances:  \"SMEG\": under mount range hood, oven, dishwasher, induction cooktop and microwave oven.  \nGlass splashback \nAir conditioning: Daikin 14KW inverter ducted split system.\nIntercom: Colour indoor video monitors, 1 upstairs and 1 downstairs.\nIntercom: 1 Stainless steel for out door \nSecurity system, LCD Keypad, wireless remote. \n\nFor further information on this superb opportunity or to obtain a specifications list, please contact Sandy Thai on 0452 410 928.\n\n\nImage Disclaimer. Please be aware that the swimming pool shown on the floorplan and images are for illustration purposes only.",
    video_link: null,
    lat: -34.888379,
    lon: 138.6091295,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "50",
    address_street: "Harvey Street",
    address_suburb: "COLLINSWOOD",
    address_suburb_display: "yes",
    address_state: "SA",
    address_region: null,
    address_postcode: "5081",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "SCL9489",
    price: 1100000,
    price_display: "no",
    price_min: 1100000,
    price_max: 1100000,
    price_view: "",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:05:59",
    created_at: "2017-04-13 21:02:27",
    updated_at: "2017-04-13 21:02:28",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183202,
        property_id: 175077,
        user_id: 1040,
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27",
        agent: {
          id: 1040,
          name: "Sandy Thai",
          email: "sandy-thai-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/362941fd1c6758f598bbcb79f2ed91f5.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0452 410 928",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:30:18",
          updated_at: "2016-07-08 17:30:18",
          email_aliases: [
            {
              id: 1051,
              user_id: 1040,
              email: "sandy.thai@harcourts.com.au",
              created_at: "2016-07-08 17:30:18",
              updated_at: "2016-07-08 17:30:18"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483847,
        property_id: 175077,
        feature_name: "Bedrooms",
        feature_value: "3",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483848,
        property_id: 175077,
        feature_name: "Bathrooms",
        feature_value: "2",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483849,
        property_id: 175077,
        feature_name: "Garages",
        feature_value: "2",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483850,
        property_id: 175077,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483851,
        property_id: 175077,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483852,
        property_id: 175077,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483853,
        property_id: 175077,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483854,
        property_id: 175077,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483855,
        property_id: 175077,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483856,
        property_id: 175077,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483857,
        property_id: 175077,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483858,
        property_id: 175077,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483859,
        property_id: 175077,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483860,
        property_id: 175077,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483861,
        property_id: 175077,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483862,
        property_id: 175077,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483863,
        property_id: 175077,
        feature_name: "Air Conditioning",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483864,
        property_id: 175077,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483865,
        property_id: 175077,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483866,
        property_id: 175077,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483867,
        property_id: 175077,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483868,
        property_id: 175077,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483869,
        property_id: 175077,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483870,
        property_id: 175077,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483871,
        property_id: 175077,
        feature_name: "Property Type: House\nMain Bathroom: Additional Bathrooms",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303134,
        property_id: 175077,
        url: "https://images.realestateview.com.au/pics/133/10991133ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303135,
        property_id: 175077,
        url: "https://images.realestateview.com.au/pics/133/10991133bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303136,
        property_id: 175077,
        url: "https://images.realestateview.com.au/pics/133/10991133cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303137,
        property_id: 175077,
        url: "https://images.realestateview.com.au/pics/133/10991133dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      },
      {
        id: 3303138,
        property_id: 175077,
        url: "https://images.realestateview.com.au/pics/133/10991133ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:28",
        updated_at: "2017-04-13 21:02:28"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      
    ],
    property_objects: [
      {
        id: 397721,
        property_id: 175077,
        object_type: "floorplan",
        object_url: "https://images.realestateview.com.au/pics/133/10991133ao.gif",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175074,
    property_unique_id: "11911-tla4898",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "46 Meander Valley Rd, Hagley TAS 7292, Australia",
    authority: "exclusive",
    headline: "Your Own Country Estate (circa 1856)",
    description: "If you dream of escaping the rat-race and yearn for country living in your own little piece of paradise, then this is the property for you.\n \nPerhaps you dream of keeping chickens, ducks, maybe goats or alpacas, or you simply want to grow your own fruit and vegetables -  then you can make your dream a reality now by utilising this unique property which sits on just under 2 acres of land.\n \nAs soon as you walk through the electric gates you go back in time to a place where  a home is where life-long memories are created.  \n \nThe true country kitchen is perfect to cook up a storm for the family whilst they watch on - enjoying the warmth of the wood-burning fire. You will find yourself enjoying the sun stream through the Morning Room which also includes an open fire place - a great space to read a book, play the piano or just enjoy the company of others.  The Drawing Room is another great space with a fan-forced wood heater with views out onto the rose garden which are simply spectacular when in bloom.  You also have the added bonus of two new heat pumps to supplement the heating on those lovely winter Tassie days.\n \nThe upstairs bedrooms are expansive in size, with the master having the potential of putting in an ensuite whilst still affording space for a parents retreat.  There is also the option of some of the beautiful furniture to be retained which complement the rooms so well.\n \nThere are two bathrooms, one upstairs and one downstairs and plenty of storage space to accommodate a growing family.\n \nWhen you step outside it is really where you get a feel for how Mother Nature intended a garden to be. Whilst there are too many plants, fruit trees and bulbs to mention, the easy-care garden was designed so that in each season there is something new to discover and enjoy. You will be so excited the first year you live here as the garden continues to surprise you with its fruit bearing trees, the amazing collection of bulbs including peonies and an exotic herb and vegetable garden.\n \nFor the practical people in the family there are also many different outbuildings to suit many varied needs.  The massive shed with 3 phase power could be used for a business or the handy person in the family, or accommodate a vintage car collection.  There is an artist's studio with a cosy wood fire and a garden workshop which includes wood and ride-on mower to ensure the easy maintenance of the lawns. There is even a fenced paddock out the back big enough for your livestock.\n \nThere truly are too many features to mention but if you would like a detailed report and photos from every season, then call us today and we can provide these to you.\n \nSo, if it's peace and quiet you seek, and a great environment in which to bring up your family, this is it.\nYou'll never regret making this lifestyle change and all located only 20 minutes from the Launceston CBD and 23 minutes to the Launceston Airport.  You can retreat to the country yet be back in the thick of it before you know it.\n\nHouse Size: 278m&#xb2;\nLand: 7190m&#xb2;\nYear Built: 1860",
    video_link: null,
    lat: -41.5268978,
    lon: 146.8922397,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "46",
    address_street: "Meander Valley Road",
    address_suburb: "HAGLEY",
    address_suburb_display: "yes",
    address_state: "TAS",
    address_region: null,
    address_postcode: "7292",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "TLA4898",
    price: 675000,
    price_display: "yes",
    price_min: 675000,
    price_max: 675000,
    price_view: "Best offer over $675,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:05:20",
    created_at: "2017-04-13 21:02:26",
    updated_at: "2017-04-13 21:02:28",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183199,
        property_id: 175074,
        user_id: 1240,
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26",
        agent: {
          id: 1240,
          name: "Jeremy Wilkinson",
          email: "jeremy-wilkinson-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/224464966d3034ac77ed7ca6c88a6326.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0400 895 022",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:35:23",
          updated_at: "2016-07-08 17:35:23",
          email_aliases: [
            {
              id: 1252,
              user_id: 1240,
              email: "jeremy.wilkinson@harcourts.com.au",
              created_at: "2016-07-08 17:35:23",
              updated_at: "2016-07-08 17:35:23"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      {
        id: 96137,
        property_id: 175074,
        building_attribute: "area",
        building_attribute_value: "278",
        building_attribute_unit: "squareMeter",
        building_attribute_of: null,
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      }
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483772,
        property_id: 175074,
        feature_name: "Bedrooms",
        feature_value: "3",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483773,
        property_id: 175074,
        feature_name: "Bathrooms",
        feature_value: "2",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483774,
        property_id: 175074,
        feature_name: "Garages",
        feature_value: "5",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483775,
        property_id: 175074,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483776,
        property_id: 175074,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483777,
        property_id: 175074,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483778,
        property_id: 175074,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483779,
        property_id: 175074,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483780,
        property_id: 175074,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483781,
        property_id: 175074,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483782,
        property_id: 175074,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483783,
        property_id: 175074,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483784,
        property_id: 175074,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483785,
        property_id: 175074,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483786,
        property_id: 175074,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483787,
        property_id: 175074,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483788,
        property_id: 175074,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483789,
        property_id: 175074,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483790,
        property_id: 175074,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483791,
        property_id: 175074,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483792,
        property_id: 175074,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483793,
        property_id: 175074,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483794,
        property_id: 175074,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483795,
        property_id: 175074,
        feature_name: "Air Conditioning",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483796,
        property_id: 175074,
        feature_name: "Property Condition: Good\nProperty Type: House\nHouse Style: Georgian",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303075,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303076,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303077,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303078,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303079,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303080,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303081,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303082,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303083,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303084,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303085,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303086,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303087,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303088,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130nx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303089,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130ox.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303090,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130px.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303091,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130qx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303092,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130rx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303093,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130sx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303094,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130tx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303095,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130ux.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303096,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130vx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303097,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130wx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303098,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130xx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303099,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130yx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303100,
        property_id: 175074,
        url: "https://images.realestateview.com.au/pics/130/10991130zx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489984,
        property_id: 175074,
        land_attribute: "area",
        land_attribute_value: "7190",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175075,
    property_unique_id: "11911-vbt34997",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "6 Wood St, Soldiers Hill VIC 3350, Australia",
    authority: "exclusive",
    headline: "Rare Soldiers Hill Development Opportunity With Permits",
    description: "No delays, no expensive Planning Permit costs, no frustrating Council hassles.  Just get straight in and get on with the job! All the hard work has been done!!!\nThis 1928 sqm development opportunity in the sought-after Soldiers Hill precinct comes with a City of Ballarat approved demolition permit for the existing dwelling and a City of Ballarat approved planning permit. \nThe Approved planning permit allows for five high quality 3 Bedroom, 2 Bathroom homes, all with Study, Alfresco area, secluded private open space, double lock up garage and 2 Living areas. The front home is on its own title and has an additional 3rd Living area.\nThe existing 3 Bedroom house and large semi industrial shed can be rented out potentially returning $ 300.00 dollars per week whilst the 4 rear houses are built, which further reduces project costs.\nApproved Demolition and Planning Permits as well as Concept Drawings are available upon enquiry.\nDo not miss this opportunity, book your inspection today!",
    video_link: null,
    lat: -37.5439285,
    lon: 143.8523393,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "6",
    address_street: "Wood Street",
    address_suburb: "SOLDIERS HILL",
    address_suburb_display: "yes",
    address_state: "VIC",
    address_region: null,
    address_postcode: "3350",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "VBT34997",
    price: 430000,
    price_display: "yes",
    price_min: 430000,
    price_max: 430000,
    price_view: "Price by Negotiation $430,000 - $460,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:05:30",
    created_at: "2017-04-13 21:02:26",
    updated_at: "2017-04-13 21:02:29",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183200,
        property_id: 175075,
        user_id: 3488,
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26",
        agent: {
          id: 3488,
          name: "Dean Parish",
          email: "dean-parish-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/80d9dd87781e030107fc63d5de34b88c.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0490 382 264",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-14 17:15:29",
          updated_at: "2016-07-14 17:15:29",
          email_aliases: [
            {
              id: 3579,
              user_id: 3488,
              email: "dean.parish@harcourts.com.au",
              created_at: "2016-07-14 17:15:29",
              updated_at: "2016-07-14 17:15:29"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      {
        id: 12098,
        property_id: 175075,
        url: "http://www.youtube.com/watch?v=IFOITqSkwWc",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_features: [
      {
        id: 25483797,
        property_id: 175075,
        feature_name: "Bedrooms",
        feature_value: "3",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483798,
        property_id: 175075,
        feature_name: "Bathrooms",
        feature_value: "1",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483799,
        property_id: 175075,
        feature_name: "Garages",
        feature_value: "2",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483800,
        property_id: 175075,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483801,
        property_id: 175075,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483802,
        property_id: 175075,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483803,
        property_id: 175075,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483804,
        property_id: 175075,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483805,
        property_id: 175075,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483806,
        property_id: 175075,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483807,
        property_id: 175075,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483808,
        property_id: 175075,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483809,
        property_id: 175075,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483810,
        property_id: 175075,
        feature_name: "Air Conditioning",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483811,
        property_id: 175075,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483812,
        property_id: 175075,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483813,
        property_id: 175075,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483814,
        property_id: 175075,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483815,
        property_id: 175075,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483816,
        property_id: 175075,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483817,
        property_id: 175075,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483818,
        property_id: 175075,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483819,
        property_id: 175075,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483820,
        property_id: 175075,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 25483821,
        property_id: 175075,
        feature_name: "Property Type: House",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303101,
        property_id: 175075,
        url: "https://images.realestateview.com.au/pics/131/10991131ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303102,
        property_id: 175075,
        url: "https://images.realestateview.com.au/pics/131/10991131bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303103,
        property_id: 175075,
        url: "https://images.realestateview.com.au/pics/131/10991131cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303104,
        property_id: 175075,
        url: "https://images.realestateview.com.au/pics/131/10991131dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303105,
        property_id: 175075,
        url: "https://images.realestateview.com.au/pics/131/10991131ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303106,
        property_id: 175075,
        url: "https://images.realestateview.com.au/pics/131/10991131fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      },
      {
        id: 3303107,
        property_id: 175075,
        url: "https://images.realestateview.com.au/pics/131/10991131gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489985,
        property_id: 175075,
        land_attribute: "area",
        land_attribute_value: "1928",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:27",
        updated_at: "2017-04-13 21:02:27"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175072,
    property_unique_id: "11911-sng2548",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "9 Somerset Rd, Para Hills SA 5096, Australia",
    authority: "exclusive",
    headline: "Beautifully Renovated 3 Bedroom Family Home!",
    description: "This lovely solid brick family home is superbly located close to Para Hills Shopping Centre, schooling, walking distance to public transport and all other amenities.\n\nFeaturing 3 spacious bedrooms all with built in robes and ceiling fans, bedroom 1 with \"Fujita\" split system air-conditioning, generous formal lounge with gas heating, large dining area, updated kitchen with dishwasher, gas cook top and oven, overhead cupboards and pantry.\n\nOther features of the property include 2.7m high ceilings, stunning light fittings, floating timber floorboards throughout, linen cupboards, modern curtains, 2 outside entertaining areas, 2 tool sheds ideal for the handyman and great for storage, single under-cover carport and set on 528sqm allotment (approx).\n\nWill suit small families, first home buyers or investors, do not miss out on this amazing opportunity to purchase your well-priced home!",
    video_link: null,
    lat: -34.7994871,
    lon: 138.6573999,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "9",
    address_street: "Somerset Road",
    address_suburb: "PARA HILLS",
    address_suburb_display: "yes",
    address_state: "SA",
    address_region: null,
    address_postcode: "5096",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "SNG2548",
    price: 265000,
    price_display: "yes",
    price_min: 265000,
    price_max: 265000,
    price_view: "$265,000 - $285,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "yes",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:05:04",
    created_at: "2017-04-13 21:02:25",
    updated_at: "2017-04-13 21:02:26",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183197,
        property_id: 175072,
        user_id: 5799,
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25",
        agent: {
          id: 5799,
          name: "Reem State",
          email: "reem-state-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/275650740a8e980fd44829d6dfb4d3de.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0435 209 427",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2017-04-13 21:02:25",
          updated_at: "2017-04-13 21:02:25",
          email_aliases: [
            {
              id: 6667,
              user_id: 5799,
              email: "reem.state@harcourts.com.au",
              created_at: "2017-04-13 21:02:25",
              updated_at: "2017-04-13 21:02:25"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483722,
        property_id: 175072,
        feature_name: "Bedrooms",
        feature_value: "3",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483723,
        property_id: 175072,
        feature_name: "Bathrooms",
        feature_value: "1",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483724,
        property_id: 175072,
        feature_name: "Garages",
        feature_value: "0",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483725,
        property_id: 175072,
        feature_name: "Carports",
        feature_value: "1",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483726,
        property_id: 175072,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483727,
        property_id: 175072,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483728,
        property_id: 175072,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483729,
        property_id: 175072,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483730,
        property_id: 175072,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483731,
        property_id: 175072,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483732,
        property_id: 175072,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483733,
        property_id: 175072,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483734,
        property_id: 175072,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483735,
        property_id: 175072,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483736,
        property_id: 175072,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483737,
        property_id: 175072,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483738,
        property_id: 175072,
        feature_name: "Air Conditioning",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483739,
        property_id: 175072,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483740,
        property_id: 175072,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483741,
        property_id: 175072,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483742,
        property_id: 175072,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483743,
        property_id: 175072,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483744,
        property_id: 175072,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483745,
        property_id: 175072,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483746,
        property_id: 175072,
        feature_name: "Property Type: House",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303045,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303046,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303047,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303048,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303049,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303050,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303051,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303052,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303053,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303054,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303055,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303056,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303057,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303058,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128nx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303059,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128ox.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303060,
        property_id: 175072,
        url: "https://images.realestateview.com.au/pics/128/10991128px.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      
    ],
    property_objects: [
      {
        id: 397720,
        property_id: 175072,
        object_type: "floorplan",
        object_url: "https://images.realestateview.com.au/pics/128/10991128ao.gif",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      }
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175073,
    property_unique_id: "11911-tne6259",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "Unit",
    property_status: "current",
    address_full: "2/10 Kent Pl, Bridport TAS 7262, Australia",
    authority: "exclusive",
    headline: "Neat & Private Bridport Unit",
    description: "Peacefully set within a quiet cul de sac this two bedroom unit offers great potential for retirees or a holiday unit investment.  It boasts wonderful bay views and a private, easy care backyard.\n\n- One of only two units\n- Quiet, private cul de sac location\n- Bay Views\n- Open Plan Living\n- Neat backyard complete with garden shed\n- Carport\n- Freshly painted inside",
    video_link: null,
    lat: -41.0069766,
    lon: 147.3932443,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: "2",
    address_lot_number: null,
    address_site: null,
    address_street_number: "10",
    address_street: "Kent Place",
    address_suburb: "BRIDPORT",
    address_suburb_display: "yes",
    address_state: "TAS",
    address_region: null,
    address_postcode: "7262",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "TNE6259",
    price: 189000,
    price_display: "yes",
    price_min: 189000,
    price_max: 189000,
    price_view: "$189,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "yes",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:05:08",
    created_at: "2017-04-13 21:02:25",
    updated_at: "2017-04-13 21:02:27",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183198,
        property_id: 175073,
        user_id: 4032,
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25",
        agent: {
          id: 4032,
          name: "Lynda Woolley",
          email: "lynda-woolley-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/3a9bf34a45065eca84d61e254b1bcdfa.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0437635611",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-09-08 21:14:08",
          updated_at: "2016-09-08 21:14:09",
          email_aliases: [
            {
              id: 4237,
              user_id: 4032,
              email: "lynda.woolley@harcourts.com.au",
              created_at: "2016-09-08 21:14:09",
              updated_at: "2016-09-08 21:14:09"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      {
        id: 96136,
        property_id: 175073,
        building_attribute: "area",
        building_attribute_value: "140",
        building_attribute_unit: "squareMeter",
        building_attribute_of: null,
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      }
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483747,
        property_id: 175073,
        feature_name: "Bedrooms",
        feature_value: "2",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483748,
        property_id: 175073,
        feature_name: "Bathrooms",
        feature_value: "1",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483749,
        property_id: 175073,
        feature_name: "Garages",
        feature_value: "0",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483750,
        property_id: 175073,
        feature_name: "Carports",
        feature_value: "1",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483751,
        property_id: 175073,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483752,
        property_id: 175073,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483753,
        property_id: 175073,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483754,
        property_id: 175073,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483755,
        property_id: 175073,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483756,
        property_id: 175073,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483757,
        property_id: 175073,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483758,
        property_id: 175073,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483759,
        property_id: 175073,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483760,
        property_id: 175073,
        feature_name: "Air Conditioning",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483761,
        property_id: 175073,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483762,
        property_id: 175073,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483763,
        property_id: 175073,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483764,
        property_id: 175073,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483765,
        property_id: 175073,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483766,
        property_id: 175073,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483767,
        property_id: 175073,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483768,
        property_id: 175073,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483769,
        property_id: 175073,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483770,
        property_id: 175073,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 25483771,
        property_id: 175073,
        feature_name: "Tenure: Freehold\nProperty Condition: Good\nProperty Type: Unit",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303061,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303062,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303063,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303064,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303065,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303066,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303067,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303068,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303069,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303070,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303071,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303072,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303073,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      },
      {
        id: 3303074,
        property_id: 175073,
        url: "https://images.realestateview.com.au/pics/129/10991129nx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489983,
        property_id: 175073,
        land_attribute: "area",
        land_attribute_value: "254",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:26",
        updated_at: "2017-04-13 21:02:26"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175069,
    property_unique_id: "11911-vbt33933",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "Townhouse",
    property_status: "current",
    address_full: "18 Specimen Vale S, Ballarat East VIC 3350, Australia",
    authority: "exclusive",
    headline: "Brand New Premium Quality Homes Minutes' Walk To City",
    description: "An absolute outstanding opportunity to secure these brand new 2 or 3 bedroom townhouses, just minutes' walk to the heart of Ballarat and overlooking parklands. Offering up to three spacious bedrooms all with built-in-robes. A formal living room & separate family/dining area. The kitchen comes complete with stainless steel appliances and stone bench tops. Modern d&#xe9;cor bathroom. The townhouses are complimented by quality floor coverings, window furnishings, gas central heating, split system air conditioning, landscaped gardens and lock up brick garage. Prices start from $280,000.",
    video_link: null,
    lat: -37.5642287,
    lon: 143.8720512,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "18",
    address_street: "Specimen Vale South",
    address_suburb: "BALLARAT EAST",
    address_suburb_display: "yes",
    address_state: "VIC",
    address_region: null,
    address_postcode: "3350",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "VBT33933",
    price: 270000,
    price_display: "yes",
    price_min: 270000,
    price_max: 270000,
    price_view: "Price by Negotiation $270,000 - $280,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:04:29",
    created_at: "2017-04-13 21:02:24",
    updated_at: "2017-04-13 21:02:27",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183194,
        property_id: 175069,
        user_id: 1369,
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24",
        agent: {
          id: 1369,
          name: "Gerard Cosgrave",
          email: "gerard-cosgrave-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/3e6590573caa4871bccf6184399e1bf0.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0418 508 525",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 18:05:40",
          updated_at: "2016-07-08 18:05:40",
          email_aliases: [
            {
              id: 1382,
              user_id: 1369,
              email: "gerard.cosgrave@harcourts.com.au",
              created_at: "2016-07-08 18:05:40",
              updated_at: "2016-07-08 18:05:40"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483672,
        property_id: 175069,
        feature_name: "Bedrooms",
        feature_value: "2",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483673,
        property_id: 175069,
        feature_name: "Bathrooms",
        feature_value: "1",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483674,
        property_id: 175069,
        feature_name: "Garages",
        feature_value: "0",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483675,
        property_id: 175069,
        feature_name: "Carports",
        feature_value: "1",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483676,
        property_id: 175069,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483677,
        property_id: 175069,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483678,
        property_id: 175069,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483679,
        property_id: 175069,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483680,
        property_id: 175069,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483681,
        property_id: 175069,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483682,
        property_id: 175069,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483683,
        property_id: 175069,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483684,
        property_id: 175069,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483685,
        property_id: 175069,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483686,
        property_id: 175069,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483687,
        property_id: 175069,
        feature_name: "Air Conditioning",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483688,
        property_id: 175069,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483689,
        property_id: 175069,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483690,
        property_id: 175069,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483691,
        property_id: 175069,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483692,
        property_id: 175069,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483693,
        property_id: 175069,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483694,
        property_id: 175069,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483695,
        property_id: 175069,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 25483696,
        property_id: 175069,
        feature_name: "Property Type: Townhouse\nHouse Style: Cottage\nGaraging / Carparking: Single Lock-up",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303003,
        property_id: 175069,
        url: "https://images.realestateview.com.au/pics/125/10991125ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303004,
        property_id: 175069,
        url: "https://images.realestateview.com.au/pics/125/10991125bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303005,
        property_id: 175069,
        url: "https://images.realestateview.com.au/pics/125/10991125cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303006,
        property_id: 175069,
        url: "https://images.realestateview.com.au/pics/125/10991125dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303007,
        property_id: 175069,
        url: "https://images.realestateview.com.au/pics/125/10991125ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303008,
        property_id: 175069,
        url: "https://images.realestateview.com.au/pics/125/10991125fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303009,
        property_id: 175069,
        url: "https://images.realestateview.com.au/pics/125/10991125gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175070,
    property_unique_id: "11911-thh7542",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "land",
    category: null,
    property_status: "current",
    address_full: "1 Jacobsons Rd, Judbury TAS 7109, Australia",
    authority: "exclusive",
    headline: "Rural Paradise",
    description: "Dreaming of a country lifestyle? A north facing acerage on which to build your perfect home? Peaceful views over the hills? A quiet place to call home not too far from shops and services? Then you need to see this wonderful spot! \n\n22acres (9ha) of rolling pasture that is partly framed by a meandering creek, this idyllic property has views over the neighbouring hills and valleys. Spread over two titles there are more options to grow and/or develop, depending on your needs. Currently home to a heard of happy cows, the property is full fenced. There is the a scattering of Gum trees and option to have an old wooden barn which has power connected. With fertile soil and a creek for water - this would be a great property to run a market garden or simply set up a self sufficient lifestyle for your family. \n\nIn the heart of Judbury,  a welcoming friendly community and just 15 minutes on a easy sealed road to Huonville this delightful spot offers the best of quiet country living and convenience. Huonville and neighbouring Glen Huon offer schools and community groups. There are shops, services and transport in Huonville and another 30 minutes will have you in Hobart. Make this property a good base to commute from or to set up a home office and escape the rat race.",
    video_link: "https://www.youtube.com/watch?v=yQOzDneCCrM",
    lat: -42.9897437,
    lon: 146.918257,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "Lot 1",
    address_street: "Jacobsons Road",
    address_suburb: "JUDBURY",
    address_suburb_display: "yes",
    address_state: "TAS",
    address_region: null,
    address_postcode: "7109",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "THH7542",
    price: 320000,
    price_display: "yes",
    price_min: 320000,
    price_max: 320000,
    price_view: "Price by Negotiation over $320,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: "Residential",
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:04:37",
    created_at: "2017-04-13 21:02:24",
    updated_at: "2017-04-13 21:02:25",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183195,
        property_id: 175070,
        user_id: 951,
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24",
        agent: {
          id: 951,
          name: "Nick Bond",
          email: "nick-bond-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/54828e5164b22cee96a62a82764094c5.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0488 640 024",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:28:24",
          updated_at: "2016-07-08 17:28:24",
          email_aliases: [
            {
              id: 962,
              user_id: 951,
              email: "nick.bond@harcourts.com.au",
              created_at: "2016-07-08 17:28:24",
              updated_at: "2016-07-08 17:28:24"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303010,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303011,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303012,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303013,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303014,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303015,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303016,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303017,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303018,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303019,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303020,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303021,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303022,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303023,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126nx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303024,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126ox.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303025,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126px.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303026,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126qx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303027,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126rx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303028,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126sx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303029,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126tx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303030,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126ux.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303031,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126vx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303032,
        property_id: 175070,
        url: "https://images.realestateview.com.au/pics/126/10991126wx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489982,
        property_id: 175070,
        land_attribute: "area",
        land_attribute_value: "9.27",
        land_attribute_unit: "hectare",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175071,
    property_unique_id: "11911-qbm6785",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "Unit",
    property_status: "current",
    address_full: "6/33 Deviney St, Morningside QLD 4170, Australia",
    authority: "exclusive",
    headline: "Price smashed to SELL!!",
    description: "- Over 5.6% yield for this low maintenance unit\n\n- Almost \"retro\" styled affordable fully renovated and just a treat! \n\n- Soak up the morning sun from the east facing balcony which is accessed from the fully tiled open plan lounge and dining area.\n\n- 70's style unit - more spacious than most!\n\n- There are 2 large bedrooms with polished timber floors and built ins.\n\n- The over sized single lockup garage is ideal to store the pushbike, boxing bag PLUS the car!\n\n- Currently tenanted at $370 per week LOW Body Corps $1821.00 pa",
    video_link: null,
    lat: -27.4651263,
    lon: 153.0752742,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: "6",
    address_lot_number: null,
    address_site: null,
    address_street_number: "33",
    address_street: "Deviney Street",
    address_suburb: "MORNINGSIDE",
    address_suburb_display: "yes",
    address_state: "QLD",
    address_region: null,
    address_postcode: "4170",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "QBM6785",
    price: 343000,
    price_display: "yes",
    price_min: 343000,
    price_max: 343000,
    price_view: "$343,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:04:50",
    created_at: "2017-04-13 21:02:24",
    updated_at: "2017-04-13 21:02:26",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183196,
        property_id: 175071,
        user_id: 5669,
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24",
        agent: {
          id: 5669,
          name: "Annie Abra",
          email: "annie-abra-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/01c09a82f8afc194ebd8752bd9825011.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0499 112 261",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2017-04-11 17:01:18",
          updated_at: "2017-04-11 17:01:18",
          email_aliases: [
            {
              id: 6525,
              user_id: 5669,
              email: "annie.abra@harcourts.com.au",
              created_at: "2017-04-11 17:01:18",
              updated_at: "2017-04-11 17:01:18"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483697,
        property_id: 175071,
        feature_name: "Bedrooms",
        feature_value: "2",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483698,
        property_id: 175071,
        feature_name: "Bathrooms",
        feature_value: "1",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483699,
        property_id: 175071,
        feature_name: "Garages",
        feature_value: "1",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483700,
        property_id: 175071,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483701,
        property_id: 175071,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483702,
        property_id: 175071,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483703,
        property_id: 175071,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483704,
        property_id: 175071,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483705,
        property_id: 175071,
        feature_name: "Air Conditioning",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483706,
        property_id: 175071,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483707,
        property_id: 175071,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483708,
        property_id: 175071,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483709,
        property_id: 175071,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483710,
        property_id: 175071,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483711,
        property_id: 175071,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483712,
        property_id: 175071,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483713,
        property_id: 175071,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483714,
        property_id: 175071,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483715,
        property_id: 175071,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483716,
        property_id: 175071,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483717,
        property_id: 175071,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483718,
        property_id: 175071,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483719,
        property_id: 175071,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483720,
        property_id: 175071,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 25483721,
        property_id: 175071,
        feature_name: "Tenure: Freehold\nProperty Condition: Renovated\nProperty Type: Unit",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3303033,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303034,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303035,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303036,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303037,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303038,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303039,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303040,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303041,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303042,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303043,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      },
      {
        id: 3303044,
        property_id: 175071,
        url: "https://images.realestateview.com.au/pics/127/10991127lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:25",
        updated_at: "2017-04-13 21:02:25"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175067,
    property_unique_id: "11911-tsh2578",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "9 Heather Pl, St Helens TAS 7216, Australia",
    authority: "open",
    headline: "Dream home.",
    description: "Located in central St Helens and within walking distance to the shops, town centre and RSL, this three bedroom, two bathroom home is immaculate. Offering open plan kitchen and dining, you will be impressed by the quality of the fittings and stainless steel appliances, including dishwasher which look like they are brand new. Beautiful timber cabinets and massive pantry plus breakfast bar make this a very user friendly gourmet kitchen. The master bedroom is spacious and you will be spoiled by the walk in robes and ensuite which offers w/c, vanity and shower. Conveniently located away from the master, the two other double bedrooms offer built in robes and share the main bathroom, which offers w/c, vanity separate bath and shower. The living area is generous open, light and airy with reverse cycle air condition for winter cosiness and to keep you cool in summer. Offering excellent value for money, this home would suit retirees, families and anyone who appreciates quality and peace and quiet.\nLocated on a lovely flat block, with verandas for whiling away the afternoon, low maintenance gardens and double garage. What are you waiting for?",
    video_link: null,
    lat: -41.3196516,
    lon: 148.2371365,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "9",
    address_street: "Heather Place",
    address_suburb: "ST HELENS",
    address_suburb_display: "yes",
    address_state: "TAS",
    address_region: null,
    address_postcode: "7216",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "TSH2578",
    price: 320000,
    price_display: "yes",
    price_min: 320000,
    price_max: 320000,
    price_view: "$320,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:04:11",
    created_at: "2017-04-13 21:02:23",
    updated_at: "2017-04-13 21:02:24",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183192,
        property_id: 175067,
        user_id: 1043,
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23",
        agent: {
          id: 1043,
          name: "Kate Chapple",
          email: "kate-chapple-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/e2ca2dc27c27ae115757f9dff3410b20.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0409978617",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:30:22",
          updated_at: "2016-07-08 17:30:22",
          email_aliases: [
            {
              id: 1054,
              user_id: 1043,
              email: "kate.chapple@harcourts.com.au",
              created_at: "2016-07-08 17:30:22",
              updated_at: "2016-07-08 17:30:22"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      {
        id: 96134,
        property_id: 175067,
        building_attribute: "area",
        building_attribute_value: "147",
        building_attribute_unit: "squareMeter",
        building_attribute_of: null,
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      }
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483622,
        property_id: 175067,
        feature_name: "Bedrooms",
        feature_value: "3",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483623,
        property_id: 175067,
        feature_name: "Bathrooms",
        feature_value: "2",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483624,
        property_id: 175067,
        feature_name: "Garages",
        feature_value: "2",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483625,
        property_id: 175067,
        feature_name: "Carports",
        feature_value: "2",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483626,
        property_id: 175067,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483627,
        property_id: 175067,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483628,
        property_id: 175067,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483629,
        property_id: 175067,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483630,
        property_id: 175067,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483631,
        property_id: 175067,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483632,
        property_id: 175067,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483633,
        property_id: 175067,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483634,
        property_id: 175067,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483635,
        property_id: 175067,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483636,
        property_id: 175067,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483637,
        property_id: 175067,
        feature_name: "Air Conditioning",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483638,
        property_id: 175067,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483639,
        property_id: 175067,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483640,
        property_id: 175067,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483641,
        property_id: 175067,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483642,
        property_id: 175067,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483643,
        property_id: 175067,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483644,
        property_id: 175067,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483645,
        property_id: 175067,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483646,
        property_id: 175067,
        feature_name: "Property Condition: Excellent\nProperty Type: House\nHouse Style: Contemporary",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3302967,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302968,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302969,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302970,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302971,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302972,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302973,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302974,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302975,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302976,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302977,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302978,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302979,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302980,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123nx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302981,
        property_id: 175067,
        url: "https://images.realestateview.com.au/pics/123/10991123ox.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489981,
        property_id: 175067,
        land_attribute: "area",
        land_attribute_value: "740",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175068,
    property_unique_id: "11911-swv25675",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "Townhouse",
    property_status: "current",
    address_full: "32 To 34 Gulfview Road, Christies Beach, Sa 5165, Aus",
    authority: "exclusive",
    headline: "15 Brand New Soon To Be Constructed Homes!",
    description: "Four being released now! These four Townhouses all face Gulfview Road, from this position you can take advantage of the ocean.\nAnother 11 Duplex homes will be constructed here on this site. Quality built by leading builders Distinctive Homes.\nAll within walking distance to the Esplanade offering you a relaxed beach side life style while having every convenience at your fingertips.\nEach of these homes offers smart street appeal, all with balconies extending across the front of each home. Stepping inside with generous open plan living with lounge and dining combine, well-appointed kitchens with ample bench and cupboard space along with modern appliances that include, hot plates, under bench ovens - exhaust ventilation and much more. Separate laundry to each home and a 3rd separate toilet with vanity located downstairs for convenience. The upper level has three generous bedrooms, the main bedroom with built in robe, ensuite bathrooms with toilet, separate shower alcove + vanity. Main bathroom is positioned near bedrooms 2 and 3; the main bathroom has a bath, separate shower alcove + vanity and another toilet. Bedrooms 2 and 3 each with builders built in robe and both able to accommodate a double bed if required.\nThe landing from the stair case on the upper level is extra-large 3.6 x 3.5 metres, it can be adapted as a second living room comfortably therefore an ideal teenage retreat games room. This extra living room is perfect as a casual living room for the evenings, once you've packed up down stairs this room becomes the place to be when you've almost finished your day. This area would make an ideal home office for the busy self-employed person, a nice roomy study area for all the family to utilize. Having the extra space will be nice to have.\nDecor throughout will be finished in neutral colour tones, flooring down stairs will be timber laminate all throughout and on the second storey it will be wall to wall carpet.\nRear access with each home having a double lock up garage and all having direct internal access to gain entry into the homes. This design allowing you to drive straight in and enter your home is not only convenient with shopping but offers that little bit of extra security.\nThe double drive access to all homes individually allows visitors parking for two cars per home.",
    video_link: null,
    lat: null,
    lon: null,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "32 to 34",
    address_street: "Gulfview Road",
    address_suburb: "CHRISTIES BEACH",
    address_suburb_display: "yes",
    address_state: "SA",
    address_region: null,
    address_postcode: "5165",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "SWV25675",
    price: 420000,
    price_display: "yes",
    price_min: 420000,
    price_max: 420000,
    price_view: "$420,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:04:21",
    created_at: "2017-04-13 21:02:23",
    updated_at: "2017-04-13 21:02:23",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183193,
        property_id: 175068,
        user_id: 1049,
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23",
        agent: {
          id: 1049,
          name: "Greg Bolto",
          email: "greg-bolto-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/779b7f707075a9e110661d4c9f32a415.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0400 354 255",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:30:30",
          updated_at: "2016-07-08 17:30:30",
          email_aliases: [
            {
              id: 1060,
              user_id: 1049,
              email: "greg@harcourtsproperty.com.au",
              created_at: "2016-07-08 17:30:30",
              updated_at: "2016-07-08 17:30:30"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      {
        id: 96135,
        property_id: 175068,
        building_attribute: "area",
        building_attribute_value: "155",
        building_attribute_unit: "squareMeter",
        building_attribute_of: null,
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      }
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483647,
        property_id: 175068,
        feature_name: "Bedrooms",
        feature_value: "3",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483648,
        property_id: 175068,
        feature_name: "Bathrooms",
        feature_value: "2",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483649,
        property_id: 175068,
        feature_name: "Garages",
        feature_value: "2",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483650,
        property_id: 175068,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483651,
        property_id: 175068,
        feature_name: "Open Spaces",
        feature_value: "2",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483652,
        property_id: 175068,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483653,
        property_id: 175068,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483654,
        property_id: 175068,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483655,
        property_id: 175068,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483656,
        property_id: 175068,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483657,
        property_id: 175068,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483658,
        property_id: 175068,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483659,
        property_id: 175068,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483660,
        property_id: 175068,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483661,
        property_id: 175068,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483662,
        property_id: 175068,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483663,
        property_id: 175068,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483664,
        property_id: 175068,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483665,
        property_id: 175068,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483666,
        property_id: 175068,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483667,
        property_id: 175068,
        feature_name: "Air Conditioning",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483668,
        property_id: 175068,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483669,
        property_id: 175068,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483670,
        property_id: 175068,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 25483671,
        property_id: 175068,
        feature_name: "Property Type: Townhouse\nHouse Style: Contemporary",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3302982,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302983,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302984,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302985,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302986,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302987,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302988,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302989,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302990,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302991,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302992,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302993,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302994,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302995,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124nx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302996,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124ox.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302997,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124px.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302998,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124qx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3302999,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124rx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303000,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124sx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303001,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124tx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      },
      {
        id: 3303002,
        property_id: 175068,
        url: "https://images.realestateview.com.au/pics/124/10991124ux.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:24",
        updated_at: "2017-04-13 21:02:24"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175064,
    property_unique_id: "11911-vbt33422",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "land",
    category: null,
    property_status: "current",
    address_full: "105 Pasco St, Creswick VIC 3363, Australia",
    authority: "exclusive",
    headline: "Fabulous Allotment For Your Home Or Homes!",
    description: "This is an exciting opportunity to secure a rare parcel in the township of Creswick. Measuring approximately 739m2 this property has the unusual option of two street frontages. Both street boundaries are a generous 14.69m wide and with a length of approximately 50.29m there is a fantastic opportunity for subdivision (STCA). Actually the precedence has occurred neighbouring this property. The Pasco Street position has you just one block off the main road of town, while the Macs Street side overlooks the football ground of the Creswick North Primary School. The block is a two minute drive into the hub of town. With a great demand for townhouses of recent time in Creswick this is a great time to build. The land also grants those seeking to build a large home and with shedding to fit it all together in town.",
    video_link: null,
    lat: -37.408848,
    lon: 143.8897032,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "105",
    address_street: "Pasco Street",
    address_suburb: "CRESWICK",
    address_suburb_display: "yes",
    address_state: "VIC",
    address_region: null,
    address_postcode: "3363",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "VBT33422",
    price: 115000,
    price_display: "yes",
    price_min: 115000,
    price_max: 115000,
    price_view: "$115,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: "Residential",
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:03:36",
    created_at: "2017-04-13 21:02:22",
    updated_at: "2017-04-13 21:02:25",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183189,
        property_id: 175064,
        user_id: 1044,
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22",
        agent: {
          id: 1044,
          name: "Jason Gigliotti",
          email: "jason-gigliotti-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/bf8b5d2d64ba9a66b99f73cb884289d8.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0407 100 744",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:30:23",
          updated_at: "2017-03-17 17:01:42",
          email_aliases: [
            {
              id: 1055,
              user_id: 1044,
              email: "jason.gigliotti@harcourts.com.au",
              created_at: "2016-07-08 17:30:23",
              updated_at: "2016-07-08 17:30:23"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3302925,
        property_id: 175064,
        url: "https://images.realestateview.com.au/pics/120/10991120ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302926,
        property_id: 175064,
        url: "https://images.realestateview.com.au/pics/120/10991120bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302927,
        property_id: 175064,
        url: "https://images.realestateview.com.au/pics/120/10991120cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302928,
        property_id: 175064,
        url: "https://images.realestateview.com.au/pics/120/10991120dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489978,
        property_id: 175064,
        land_attribute: "area",
        land_attribute_value: "739",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175065,
    property_unique_id: "11911-tkh7549",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "280 Simpsons Bay Rd, Simpsons Bay TAS 7150, Australia",
    authority: "exclusive",
    headline: "Far from the everyday",
    description: "PLACE YOUR OFFER TODAY! \nHow often does the chance present to own approximately 4 Acres of land with over a 150m stretch of beachfront accessible form the property and with enviable privacy in the beautiful Simpsons Bay, Bruny Island.\nThe Land is a combination of cleared pasture and forest with roughly 3 Acres zoned Rural Resources enabling use of land for primary production which involves the use, the planting, growing, harvesting of crops and keeping of livestock. A superb opportunity waits for the Hobby farmer and may be particularly appealing for those interested in self sustainable living. \nPrivacy is assured with approximately 1 acre zoned Environmental Management reserved for the maintenance, protection or management of environment. Walking tracks and fencing are permitted to be constructed in this area.\nThe well maintained 2 bedroom shack allows comfortable living and sits elevated at the crest of the gently sloping lot, looking out toward the water. A Sun deck off the living space provides the perfect place to plan your dream home by the seaside, limited only by your imagination.\nWater and Electricity are already on site and septic on-site.\nApproximately a 30minute drive south from the Bruny Island Ferry and conveniently situated close to the amenities of Alonnah. Call today for more information and to place your offer.\nA full list of land use is available through the Kingborough Council and development of land is also STCA.",
    video_link: "https://youtu.be/MYLARqIA4v4",
    lat: -43.2914479,
    lon: 147.3053954,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "280",
    address_street: "Simpsons Bay Road",
    address_suburb: "SIMPSONS BAY",
    address_suburb_display: "yes",
    address_state: "TAS",
    address_region: null,
    address_postcode: "7150",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "TKH7549",
    price: 290000,
    price_display: "yes",
    price_min: 290000,
    price_max: 290000,
    price_view: "Price by Negotiation over $290,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:03:52",
    created_at: "2017-04-13 21:02:22",
    updated_at: "2017-04-13 21:02:23",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183190,
        property_id: 175065,
        user_id: 1243,
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22",
        agent: {
          id: 1243,
          name: "Melody Simmonds",
          email: "melody-simmonds-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/85365ebbe8517db75245acc66ebf9e41.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0400122611",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:35:31",
          updated_at: "2016-12-07 21:04:02",
          email_aliases: [
            {
              id: 1255,
              user_id: 1243,
              email: "melody.simmonds@harcourts.com.au",
              created_at: "2016-07-08 17:35:31",
              updated_at: "2016-07-08 17:35:31"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      {
        id: 96133,
        property_id: 175065,
        building_attribute: "area",
        building_attribute_value: "69",
        building_attribute_unit: "squareMeter",
        building_attribute_of: null,
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      }
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483596,
        property_id: 175065,
        feature_name: "Bedrooms",
        feature_value: "2",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483597,
        property_id: 175065,
        feature_name: "Bathrooms",
        feature_value: "1",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483598,
        property_id: 175065,
        feature_name: "Garages",
        feature_value: "0",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483599,
        property_id: 175065,
        feature_name: "Carports",
        feature_value: "0",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483600,
        property_id: 175065,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483601,
        property_id: 175065,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483602,
        property_id: 175065,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483603,
        property_id: 175065,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483604,
        property_id: 175065,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483605,
        property_id: 175065,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483606,
        property_id: 175065,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483607,
        property_id: 175065,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483608,
        property_id: 175065,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483609,
        property_id: 175065,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483610,
        property_id: 175065,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483611,
        property_id: 175065,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483612,
        property_id: 175065,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483613,
        property_id: 175065,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483614,
        property_id: 175065,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483615,
        property_id: 175065,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483616,
        property_id: 175065,
        feature_name: "Air Conditioning",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483617,
        property_id: 175065,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483618,
        property_id: 175065,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483619,
        property_id: 175065,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483620,
        property_id: 175065,
        feature_name: "Property Condition: Good\nProperty Type: Acreage/semi Rural",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 25483621,
        property_id: 175065,
        feature_name: "House\nHouse Style: Beach House",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3302929,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302930,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302931,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302932,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302933,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302934,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302935,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302936,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302937,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302938,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302939,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302940,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302941,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302942,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121nx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302943,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121ox.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      },
      {
        id: 3302944,
        property_id: 175065,
        url: "https://images.realestateview.com.au/pics/121/10991121px.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489979,
        property_id: 175065,
        land_attribute: "area",
        land_attribute_value: "17300",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      }
    ],
    property_objects: [
      {
        id: 397719,
        property_id: 175065,
        object_type: "floorplan",
        object_url: "https://images.realestateview.com.au/pics/121/10991121ao.gif",
        created_at: "2017-04-13 21:02:22",
        updated_at: "2017-04-13 21:02:22"
      }
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175066,
    property_unique_id: "11911-thh7552",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "land",
    category: null,
    property_status: "current",
    address_full: "8692 Huon Hwy, Southport TAS 7109, Australia",
    authority: "exclusive",
    headline: "Private Bush Retreat",
    description: "A blank canvas retreat - your personal mini national park of 5.727ha (14acres). On the road to the beach, most people would drive by and notice only a green patch of pasture. On close inspection there is a seat in the shade of a large gum tree and from there beautifully created trails lead to a clearing that no one else would ever know was there. \n\nSurrounded by privacy and trees of all sizes a grassy clearing is a pretty cool place to escape too! Large enough for quite a few tents to circle round a camp fire or build a more deluxe glamping escape (STCA).\n\nUnique in so many ways to many of the other properties on the market, it is most definitely worth a look. Located just a couple of minutes drive from the shop, pub and beaches of Southport. On the doorstep to the far south, Ida bay railway and many more entertaining adventures to give you even more excuse to get away from it all and enjoy this parcel of Tasmanian bliss!",
    video_link: null,
    lat: -43.4197093,
    lon: 146.9696327,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "8692",
    address_street: "Huon Highway",
    address_suburb: "SOUTHPORT",
    address_suburb_display: "yes",
    address_state: "TAS",
    address_region: null,
    address_postcode: "7109",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "THH7552",
    price: 135000,
    price_display: "yes",
    price_min: 135000,
    price_max: 135000,
    price_view: "Price by Negotiation over $135,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: "Residential",
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:03:58",
    created_at: "2017-04-13 21:02:22",
    updated_at: "2017-04-13 21:02:23",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183191,
        property_id: 175066,
        user_id: 951,
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23",
        agent: {
          id: 951,
          name: "Nick Bond",
          email: "nick-bond-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/54828e5164b22cee96a62a82764094c5.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0488 640 024",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2016-07-08 17:28:24",
          updated_at: "2016-07-08 17:28:24",
          email_aliases: [
            {
              id: 962,
              user_id: 951,
              email: "nick.bond@harcourts.com.au",
              created_at: "2016-07-08 17:28:24",
              updated_at: "2016-07-08 17:28:24"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3302945,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302946,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302947,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302948,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302949,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302950,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302951,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302952,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302953,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302954,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302955,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302956,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302957,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302958,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122nx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302959,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122ox.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302960,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122px.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302961,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122qx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302962,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122rx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302963,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122sx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302964,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122tx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302965,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122ux.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      },
      {
        id: 3302966,
        property_id: 175066,
        url: "https://images.realestateview.com.au/pics/122/10991122vx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489980,
        property_id: 175066,
        land_attribute: "area",
        land_attribute_value: "5.73",
        land_attribute_unit: "hectare",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:23",
        updated_at: "2017-04-13 21:02:23"
      }
    ],
    property_objects: [
      
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  },
  {
    id: 175062,
    property_unique_id: "11911-qap28925",
    agency_id: 43,
    branch_id: 152,
    country_id: 36,
    property_type: "residential",
    category: "House",
    property_status: "current",
    address_full: "24 Karenia St, Bray Park QLD 4500, Australia",
    authority: "exclusive",
    headline: "Looking for a top starter home?",
    description: "634m2 land with new colourbond fencing\nAir conditioned throughout \nHuge outdoor living area with sink and hot & cold water (great for BBQs)\n2 bay shed plus 2 bay carport big enough for a caravan\n3 bedrooms all with wardrobes\n2 way bathroom\nGas hot water - with temperature control\nDishwasher\nGas cooking\nSecurity alarm\nSolar panels\nCubby house\nSide Access to the rear\nThis neat freshly-painted starter home in a top location is ready for you to move into.  The 2-bay man-cave in the back yard gives you ample storage space as well as workshop area.\nThe quiet street with good neighbours is a great spot for you to put down roots and enjoy life without breaking the bank.\nThe outdoor living space is great for extra room to enjoy casual living, barbeques with friends or a play space for the little ones.  The cubby house is an imaginative space for children to play and enjoy themselves.\nThis central location provides easy access to schools, shops and public transport. \nThis won't last long, so book your inspection today.",
    video_link: null,
    lat: -27.2845805,
    lon: 152.9563513,
    has_geolocation_issue: 0,
    geolocation_error_message: null,
    address_display: "yes",
    address_sub_number: null,
    address_lot_number: null,
    address_site: null,
    address_street_number: "24",
    address_street: "Karenia Street",
    address_suburb: "BRAY PARK",
    address_suburb_display: "yes",
    address_state: "QLD",
    address_region: null,
    address_postcode: "4500",
    address_country: "AUS",
    rea_agency_id: "11911",
    rea_listing_unique_id: "QAP28925",
    price: 389000,
    price_display: "yes",
    price_min: 389000,
    price_max: 389000,
    price_view: "Price by Negotiation over $389,000",
    date_available: null,
    bond: null,
    auction_date: null,
    exclusivity: null,
    under_offer: "no",
    is_home_land_package: "no",
    new_construction: "no",
    car_spaces: 0,
    parking_comments: null,
    zone: null,
    is_multiple: null,
    pet_friendly: null,
    tenancy: null,
    current_lease_end_date: null,
    council_rates: null,
    municipality: null,
    property_extent: null,
    commercial_listing_type: null,
    commercial_category: null,
    commercial_authority: null,
    commercial_rent: null,
    commercial_rent_period: null,
    commercial_rent_plus_outgoings: null,
    terms: null,
    property_return: null,
    takings: null,
    franchise: null,
    rural_category: null,
    land_category: null,
    holiday: null,
    holiday_category: null,
    mod_time: "2017-04-13 20:03:25",
    created_at: "2017-04-13 21:02:21",
    updated_at: "2017-04-13 21:02:23",
    deleted_at: null,
    agency: {
      id: 43,
      name: "Harcourts",
      approved: 1,
      actioned_by_user_id: 1,
      actioned_on: "2016-07-08 14:29:11",
      created_at: "2016-07-08 14:29:11",
      updated_at: "2016-07-08 14:29:11"
    },
    property_agents: [
      {
        id: 183187,
        property_id: 175062,
        user_id: 5726,
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21",
        agent: {
          id: 5726,
          name: "Marilyn Davies",
          email: "marilyn-davies-harcourts@digitalpropertygroup.com",
          photo_url: "https://www.gravatar.com/avatar/a99aa1f90af9490534472ce52df35661.jpg?s=200&d=mm",
          uses_two_factor_auth: false,
          mobile: "0407 149 309",
          two_factor_reset_code: null,
          current_team_id: null,
          stripe_id: null,
          current_billing_plan: null,
          billing_state: null,
          vat_id: null,
          trial_ends_at: null,
          last_read_announcements_at: null,
          created_at: "2017-04-12 13:03:34",
          updated_at: "2017-04-12 13:03:34",
          email_aliases: [
            {
              id: 6587,
              user_id: 5726,
              email: "marilyn.davies@harcourts.com.au",
              created_at: "2017-04-12 13:03:34",
              updated_at: "2017-04-12 13:03:34"
            }
          ],
          tax_rate: 0
        }
      }
    ],
    property_allowance: [
      
    ],
    property_building_details: [
      
    ],
    property_business_categories: [
      
    ],
    property_commercial_categories: [
      
    ],
    property_estates: [
      
    ],
    property_external_links: [
      
    ],
    property_features: [
      {
        id: 25483546,
        property_id: 175062,
        feature_name: "Bedrooms",
        feature_value: "3",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483547,
        property_id: 175062,
        feature_name: "Bathrooms",
        feature_value: "1",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483548,
        property_id: 175062,
        feature_name: "Garages",
        feature_value: "2",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483549,
        property_id: 175062,
        feature_name: "Carports",
        feature_value: "2",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483550,
        property_id: 175062,
        feature_name: "Open Spaces",
        feature_value: "0",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483551,
        property_id: 175062,
        feature_name: "Living Areas",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483552,
        property_id: 175062,
        feature_name: "Study",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483553,
        property_id: 175062,
        feature_name: "Dishwasher",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483554,
        property_id: 175062,
        feature_name: "Intercom",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483555,
        property_id: 175062,
        feature_name: "Ducted Cooling",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483556,
        property_id: 175062,
        feature_name: "Built In Robes",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483557,
        property_id: 175062,
        feature_name: "Ducted Heating",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483558,
        property_id: 175062,
        feature_name: "Alarm System",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483559,
        property_id: 175062,
        feature_name: "Air Conditioning",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483560,
        property_id: 175062,
        feature_name: "Outside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483561,
        property_id: 175062,
        feature_name: "Workshop",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483562,
        property_id: 175062,
        feature_name: "Inside Spa",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483563,
        property_id: 175062,
        feature_name: "Tennis Court",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483564,
        property_id: 175062,
        feature_name: "Floorboards",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483565,
        property_id: 175062,
        feature_name: "Pool Above Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483566,
        property_id: 175062,
        feature_name: "Pool In Ground",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483567,
        property_id: 175062,
        feature_name: "Open Fire Place",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483568,
        property_id: 175062,
        feature_name: "Pay T V",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483569,
        property_id: 175062,
        feature_name: "Balcony",
        feature_value: "no",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 25483570,
        property_id: 175062,
        feature_name: "Property Type: House",
        feature_value: "yes",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      }
    ],
    property_highlights: [
      
    ],
    property_ideal_for: [
      
    ],
    property_images: [
      {
        id: 3302896,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118ax.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302897,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118bx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302898,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118cx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302899,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118dx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302900,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118ex.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302901,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118fx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302902,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118gx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302903,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118hx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302904,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118ix.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302905,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118jx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302906,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118kx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302907,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118lx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      },
      {
        id: 3302908,
        property_id: 175062,
        url: "https://images.realestateview.com.au/pics/118/10991118mx.jpg",
        format: "jpg",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      }
    ],
    property_inspection_times: [
      
    ],
    property_land_details: [
      {
        id: 489976,
        property_id: 175062,
        land_attribute: "area",
        land_attribute_value: "634",
        land_attribute_unit: "squareMeter",
        land_attribute_of: null,
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      }
    ],
    property_objects: [
      {
        id: 397718,
        property_id: 175062,
        object_type: "floorplan",
        object_url: "https://images.realestateview.com.au/pics/118/10991118ao.gif",
        created_at: "2017-04-13 21:02:21",
        updated_at: "2017-04-13 21:02:21"
      }
    ],
    property_rents: [
      
    ],
    property_rural_features: [
      
    ],
    property_sold_details: [
      
    ],
    property_vendors: [
      
    ],
    property_views: [
      
    ]
  }
];
export default Data;