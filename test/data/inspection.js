import fn from '../../src/functions';
let past   = new Date().getFullYear() - 1,
    future = past + 2;
export default {
    past: {
        id             : 352041,
        property_id    : 23199,
        inspection_time: "25-Jun-" + past + " 11:00AM to 11:30AM",
        created_at     : past + "-09-18 17:01:19",
        updated_at     : past + "-09-18 17:01:19"
    },

    future: {
        id             : 352041,
        property_id    : 23199,
        inspection_time: "25-Jun-" + future + " 11:00AM to 11:30AM",
        created_at     : future + "-09-18 17:01:19",
        updated_at     : future + "-09-18 17:01:19"
    },
    /**
     * Formats a Date to an example inspection date string.
     * @param  {Date} d
     * @return {String}
     */
    inspectionDateString : (d) => {
        let dd   = d.getDate() < 10 ? "0" + d.getDate() : d.getDate();
        let mmm  = fn.months()[d.getMonth()];
        let yyyy = d.getFullYear();
        let hh   = d.getHours() < 10 ? "0" + d.getHours() : d.getHours();
        let mm   = d.getMinutes() < 12 ? d.getMinutes() : d.getMinutes() - 12;
            mm   = mm < 10 ? "0" + mm : mm;
        let am   = hh < 12 ? 'AM' : 'PM';
        return dd + '-' + mmm + '-' + yyyy + ' ' + hh + ':' + mm + am + ' to ' + hh + ':' + mm + am;
    },
};
