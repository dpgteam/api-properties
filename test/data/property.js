export default  {
    dpg : {
        id: 1,
          property_unique_id: "9392391967675",
          agency_id: 1,
          branch_id: null,
          country_id: 36,
          property_type: "residential",
          category: null,
          property_status: "current",
          address_full: "39-41 Mount St, Prahran, Vic 3181, Australia",
          authority: null,
          headline: "DPG",
          description: "Lorem Ipsum.",
          video_link: null,
          lat: -37.850349,
          lon: 144.9958826,
          has_geolocation_issue: 0,
          geolocation_error_message: null,
          address_display: "no",
          address_sub_number: null,
          address_lot_number: null,
          address_site: null,
          address_street_number: "39-41",
          address_street: "Mount St",
          address_suburb: "Prahran",
          address_suburb_display: "no",
          address_state: "Vic",
          address_region: null,
          address_postcode: "3181",
          address_country: "Australia",
          rea_agency_id: null,
          rea_listing_unique_id: null,
          price: null,
          price_display: "yes",
          price_min: null,
          price_max: null,
          price_view: null,
          date_available: null,
          bond: null,
          auction_date: null,
          exclusivity: null,
          under_offer: "no",
          is_home_land_package: "no",
          new_construction: "no",
          car_spaces: 0,
          parking_comments: null,
          zone: null,
          is_multiple: null,
          pet_friendly: null,
          tenancy: null,
          current_lease_end_date: null,
          council_rates: null,
          municipality: null,
          property_extent: null,
          commercial_listing_type: null,
          commercial_category: null,
          commercial_authority: null,
          commercial_rent: null,
          commercial_rent_period: null,
          commercial_rent_plus_outgoings: null,
          terms: "",
          property_return: null,
          takings: null,
          franchise: null,
          rural_category: null,
          land_category: null,
          holiday: null,
          holiday_category: null,
          mod_time: "2016-01-01 00:00:00",
          created_at: "2016-07-08 14:29:12",
          updated_at: "2016-10-31 09:13:04",
          deleted_at: null,
          agency: {
            id: 1,
            name: "DPG - Digital Property Group",
            approved: 1,
            actioned_by_user_id: 1,
            actioned_on: "2016-07-08 14:29:11",
            created_at: "2016-07-08 14:29:11",
            updated_at: "2016-07-08 14:29:11"
          },
          property_agents: [

          ],
          property_allowance: [

          ],
          property_building_details: [

          ],
          property_business_categories: [

          ],
          property_commercial_categories: [

          ],
          property_estates: [

          ],
          property_external_links: [

          ],
          property_features: [

          ],
          property_highlights: [

          ],
          property_ideal_for: [

          ],
          property_images: [
            {
              id: 580844,
              property_id: 1,
              url: "http://depo8.com/wp-content/fancygallery/2/1/depo8-18-1B7A0298.jpg",
              format: "jpg",
              created_at: "2016-07-08 16:25:08",
              updated_at: "2016-07-08 16:25:08"
            }
          ],
          property_inspection_times: [

          ],
          property_land_details: [

          ],
          property_objects: [

          ],
          property_rents: [

          ],
          property_rural_features: [

          ],
          property_sold_details: [

          ],
          property_vendors: [

          ],
          property_views: [

          ]
    },
    rent:  {
        id: 1818,
        property_unique_id: "xngess-1p2146",
        agency_id: 30,
        branch_id: 31,
        country_id: 36,
        property_type: "rental",
        category: "Townhouse",
        property_status: "current",
        address_full: null,
        authority: null,
        headline: "QUALITY",
        description: "Rental Description",
        video_link: null,
        lat: null,
        lon: null,
        has_geolocation_issue: 0,
        geolocation_error_message: null,
        address_display: "yes",
        address_sub_number: null,
        address_lot_number: null,
        address_site: null,
        address_street_number: "2/969",
        address_street: "Mt Alexander Road",
        address_suburb: "Essendon",
        address_suburb_display: "yes",
        address_state: "VIC",
        address_region: null,
        address_postcode: "3040",
        address_country: "AUS",
        rea_agency_id: "XNGESS",
        rea_listing_unique_id: "1P2146",
        price: null,
        price_display: "yes",
        price_min: null,
        price_max: null,
        price_view: "$485",
        date_available: "2016-06-20 00:00:00",
        bond: "2108",
        auction_date: null,
        exclusivity: "exclusive",
        under_offer: "no",
        is_home_land_package: "no",
        new_construction: "no",
        car_spaces: 0,
        parking_comments: null,
        zone: null,
        is_multiple: "no",
        pet_friendly: null,
        tenancy: null,
        current_lease_end_date: null,
        council_rates: null,
        municipality: null,
        property_extent: null,
        commercial_listing_type: null,
        commercial_category: null,
        commercial_authority: null,
        commercial_rent: null,
        commercial_rent_period: null,
        commercial_rent_plus_outgoings: null,
        terms: null,
        property_return: null,
        takings: null,
        franchise: null,
        rural_category: null,
        land_category: null,
        holiday: null,
        holiday_category: null,
        mod_time: "2016-01-01 00:00:00",
        created_at: "2016-07-08 16:55:07",
        updated_at: "2016-07-08 16:56:27",
        deleted_at: null,
        agency: {
          id: 30,
          name: "Paul McDonald",
          approved: 1,
          actioned_by_user_id: 1,
          actioned_on: "2016-07-08 14:29:11",
          created_at: "2016-07-08 14:29:11",
          updated_at: "2016-07-08 14:29:11"
        },
        property_agents: [
          {
            id: 2592,
            property_id: 1818,
            user_id: 310,
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07",
            agent: {
              id: 310,
              name: "Mitch Ladas",
              email: "mitch-ladas-paul-mcdonald@digitalpropertygroup.com",
              photo_url: "https://www.gravatar.com/avatar/db1ae562a2f97e6a09e5bb22f1b1219c.jpg?s=200&d=mm",
              uses_two_factor_auth: false,
              mobile: "0478 536 456",
              two_factor_reset_code: null,
              current_team_id: null,
              stripe_id: null,
              current_billing_plan: null,
              billing_state: null,
              vat_id: null,
              trial_ends_at: null,
              last_read_announcements_at: null,
              created_at: "2016-07-08 16:55:07",
              updated_at: "2017-01-25 10:01:06",
              email_aliases: [
                {
                  id: 310,
                  user_id: 310,
                  email: "mitch@sherylupton.com.au",
                  created_at: "2016-07-08 16:55:07",
                  updated_at: "2016-07-08 16:55:07"
                },
                {
                  id: 5562,
                  user_id: 310,
                  email: "mitch@mcdonaldupton.com.au",
                  created_at: "2017-01-25 10:01:06",
                  updated_at: "2017-01-25 10:01:06"
                }
              ],
              tax_rate: 0
            }
          }
        ],
        property_allowance: [
          {
            id: 2660,
            property_id: 1818,
            allowance_name: "Pet Friendly",
            allowance_value: "0",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 2661,
            property_id: 1818,
            allowance_name: "Furnished",
            allowance_value: "0",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 2662,
            property_id: 1818,
            allowance_name: "Smokers",
            allowance_value: "0",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          }
        ],
        property_building_details: [],
        property_business_categories: [],
        property_commercial_categories: [],
        property_estates: [],
        property_external_links: [],
        property_features: [
          {
            id: 166071,
            property_id: 1818,
            feature_name: "Open Spaces",
            feature_value: "1",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166072,
            property_id: 1818,
            feature_name: "Bedrooms",
            feature_value: "3",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166073,
            property_id: 1818,
            feature_name: "Bathrooms",
            feature_value: "1",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166074,
            property_id: 1818,
            feature_name: "Garages",
            feature_value: "1",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166075,
            property_id: 1818,
            feature_name: "Carports",
            feature_value: "0",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166076,
            property_id: 1818,
            feature_name: "Toilets",
            feature_value: "2",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166077,
            property_id: 1818,
            feature_name: "Air Conditioning",
            feature_value: "no",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166078,
            property_id: 1818,
            feature_name: "Built In Robes",
            feature_value: "no",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166079,
            property_id: 1818,
            feature_name: "Alarm System",
            feature_value: "no",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166080,
            property_id: 1818,
            feature_name: "Tennis Court",
            feature_value: "no",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166081,
            property_id: 1818,
            feature_name: "Intercom",
            feature_value: "no",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166082,
            property_id: 1818,
            feature_name: "Open Fire Place",
            feature_value: "no",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166083,
            property_id: 1818,
            feature_name: "Vacuum System",
            feature_value: "no",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 166084,
            property_id: 1818,
            feature_name: "Ensuite",
            feature_value: "0",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          }
        ],
        property_highlights: [],
        property_ideal_for: [],
        property_images: [
          {
            id: 14661,
            property_id: 1818,
            url: "http://sherylupton.com.au/lt-1-1P2146-146526009210546417-rsd.jpg",
            format: "jpg",
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07"
          },
          {
            id: 14662,
            property_id: 1818,
            url: "http://sherylupton.com.au/lt-1-1P2146-146526009299889715-rsd.jpg",
            format: "jpg",
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07"
          },
          {
            id: 14663,
            property_id: 1818,
            url: "http://sherylupton.com.au/lt-1-1P2146-146526009322541484-rsd.jpg",
            format: "jpg",
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07"
          },
          {
            id: 14664,
            property_id: 1818,
            url: "http://sherylupton.com.au/lt-1-1P2146-146526009340474647-rsd.jpg",
            format: "jpg",
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07"
          },
          {
            id: 14665,
            property_id: 1818,
            url: "http://sherylupton.com.au/lt-1-1P2146-146526009346837740-rsd.jpg",
            format: "jpg",
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07"
          },
          {
            id: 14666,
            property_id: 1818,
            url: "http://sherylupton.com.au/lt-1-1P2146-146526009349566933-rsd.jpg",
            format: "jpg",
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07"
          },
          {
            id: 14667,
            property_id: 1818,
            url: "http://sherylupton.com.au/lt-1-1P2146-146526009406798223-rsd.jpg",
            format: "jpg",
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07"
          },
          {
            id: 14668,
            property_id: 1818,
            url: "http://sherylupton.com.au/lt-1-1P2146-146526009433385290-rsd.jpg",
            format: "jpg",
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07"
          },
          {
            id: 14669,
            property_id: 1818,
            url: "http://sherylupton.com.au/lt-1-1P2146-146526009561968688-rsd.jpg",
            format: "jpg",
            created_at: "2016-07-08 16:55:07",
            updated_at: "2016-07-08 16:55:07"
          }
        ],
        property_inspection_times: [
          {
            id: 12452,
            property_id: 1818,
            inspection_time: "18-Jun-2016 11:00AM to 11:15AM",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          }
        ],
        property_land_details: [
          {
            id: 3619,
            property_id: 1818,
            land_attribute: "area",
            land_attribute_value: "0",
            land_attribute_unit: "squareMeter",
            land_attribute_of: null,
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          },
          {
            id: 3620,
            property_id: 1818,
            land_attribute: "frontage",
            land_attribute_value: "0",
            land_attribute_unit: "meter",
            land_attribute_of: null,
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          }
        ],
        property_objects: [],
        property_rents: [
          {
            id: 3446,
            property_id: 1818,
            rent_amount: 485,
            rent_period: "weekly",
            rent_display: "yes",
            created_at: "2016-07-08 16:56:27",
            updated_at: "2016-07-08 16:56:27"
          }
        ],
        property_rural_features: [],
        property_sold_details: [],
        property_vendors: [],
        property_views: []
    },
    sale: {
        id: 23556,
        property_unique_id: "4649-10323161",
        agency_id: 121212,
        branch_id: 313,
        country_id: 36,
        property_type: "residential",
        category: "Apartment",
        property_status: "current",
        address_full: "2-4 Glass Street, Essendon, Vic 3040, Aus",
        authority: "exclusive",
        headline: "Boutique Luxury - PRICE REDUCTION MUST BE SOLD $525,000",
        description: "Enjoy an enviable Essendon lifestyle with this superbly located two bedroom, two bathroom top-level apartment situated near Glenbervie Station, Windy Hill sports precinct, trendy cafes, shops and entertainment venues.  One of only three, its boutique status is certain to impress city professionals and investors, while a coveted position in the Strathmore College zone will delight family home-buyers.  Architecturally designed to maximise space, light and an elevated aspect, the property is bathed in natural light and features soaring ceilings with clerestory windows, alfresco balcony and an open plan living domain with skyward vistas.  Smartly appointed, the two-tone kitchen showcases a suite of stainless steel Smeg appliances, stone counters and glass splashbacks.  Generous accommodation is provided by two double bedrooms, one offers built-in-robes, and the main boasts walk-in-robe and stylish ensuite.  Further enhanced with luxury bathroom, separate laundry/toilet, split-system, intercom, storage and automated garage with internal access.  Excitingly close to North Essendon Village, Woodland Street shops, St. Vincents Primary, CityLink and airports.",
        video_link: null,
        lat: -37.7468515,
        lon: 144.9207727,
        has_geolocation_issue: 0,
        geolocation_error_message: null,
        address_display: "yes",
        address_sub_number: "2",
        address_lot_number: null,
        address_site: null,
        address_street_number: "2-4",
        address_street: "Glass Street",
        address_suburb: "ESSENDON",
        address_suburb_display: "yes",
        address_state: "VIC",
        address_region: null,
        address_postcode: "3040",
        address_country: "AUS",
        rea_agency_id: "4649",
        rea_listing_unique_id: "10323161",
        price: 490000,
        price_display: "yes",
        price_min: 490000,
        price_max: 490000,
        price_view: "Private Sale - $525,000",
        date_available: null,
        bond: null,
        auction_date: "2016-07-30 11:00:00",
        exclusivity: null,
        under_offer: "no",
        is_home_land_package: "no",
        new_construction: "no",
        car_spaces: 0,
        parking_comments: null,
        zone: null,
        is_multiple: null,
        pet_friendly: null,
        tenancy: null,
        current_lease_end_date: null,
        council_rates: null,
        municipality: null,
        property_extent: null,
        commercial_listing_type: null,
        commercial_category: null,
        commercial_authority: null,
        commercial_rent: null,
        commercial_rent_period: null,
        commercial_rent_plus_outgoings: null,
        terms: null,
        property_return: null,
        takings: null,
        franchise: null,
        rural_category: null,
        land_category: null,
        holiday: null,
        holiday_category: null,
        mod_time: "2016-11-04 02:20:02",
        created_at: "2016-07-09 02:07:48",
        updated_at: "2016-11-04 10:04:04",
        deleted_at: null,
        agency: null,
        property_agents: [
          {
            id: 27495,
            property_id: 23556,
            user_id: 2830,
            created_at: "2016-07-09 02:07:48",
            updated_at: "2016-07-09 02:07:48",
            agent: {
              id: 2830,
              name: "Catherine Callahan",
              email: "catherine-callahan-brad-teal@digitalpropertygroup.com",
              photo_url: "https://www.gravatar.com/avatar/48c4b7cdfcc3f858f975dc1b1fbff488.jpg?s=200&d=mm",
              uses_two_factor_auth: false,
              mobile: "0408 580 491",
              two_factor_reset_code: null,
              current_team_id: null,
              stripe_id: null,
              current_billing_plan: null,
              billing_state: null,
              vat_id: null,
              trial_ends_at: null,
              last_read_announcements_at: null,
              created_at: "2016-07-09 01:58:04",
              updated_at: "2016-07-09 01:58:04",
              email_aliases: [
                {
                  id: 2873,
                  user_id: 2830,
                  email: "ccallahan@bradteal.com.au",
                  created_at: "2016-07-09 01:58:04",
                  updated_at: "2016-07-09 01:58:04"
                }
              ],
              tax_rate: 0
            }
          },
          {
            id: 69738,
            property_id: 23556,
            user_id: 2826,
            created_at: "2016-09-18 17:06:16",
            updated_at: "2016-09-18 17:06:16",
            agent: {
              id: 2826,
              name: "Rodney Callahan",
              email: "rodney-callahan-brad-teal@digitalpropertygroup.com",
              photo_url: "https://www.gravatar.com/avatar/495eb351483a88c1f13d96596e263493.jpg?s=200&d=mm",
              uses_two_factor_auth: false,
              mobile: "0418 131 123",
              two_factor_reset_code: null,
              current_team_id: null,
              stripe_id: null,
              current_billing_plan: null,
              billing_state: null,
              vat_id: null,
              trial_ends_at: null,
              last_read_announcements_at: null,
              created_at: "2016-07-09 01:57:37",
              updated_at: "2016-09-18 17:43:29",
              email_aliases: [
                {
                  id: 2869,
                  user_id: 2826,
                  email: "rcallahan@bradteal.com.au",
                  created_at: "2016-07-09 01:57:37",
                  updated_at: "2016-07-09 01:57:37"
                }
              ],
              tax_rate: 0
            }
          }
        ],
        property_allowance: [],
        property_building_details: [],
        property_business_categories: [],
        property_commercial_categories: [],
        property_estates: [],
        property_external_links: [],
        property_features: [
          {
            id: 12516363,
            property_id: 23556,
            feature_name: "Bedrooms",
            feature_value: "2",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516364,
            property_id: 23556,
            feature_name: "Bathrooms",
            feature_value: "2",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516365,
            property_id: 23556,
            feature_name: "Garages",
            feature_value: "1",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516366,
            property_id: 23556,
            feature_name: "Carports",
            feature_value: "0",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516367,
            property_id: 23556,
            feature_name: "Open Spaces",
            feature_value: "0",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516368,
            property_id: 23556,
            feature_name: "Living Areas",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516369,
            property_id: 23556,
            feature_name: "Study",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516370,
            property_id: 23556,
            feature_name: "Balcony",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516371,
            property_id: 23556,
            feature_name: "Air Conditioning",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516372,
            property_id: 23556,
            feature_name: "Built In Robes",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516373,
            property_id: 23556,
            feature_name: "Ducted Cooling",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516374,
            property_id: 23556,
            feature_name: "Tennis Court",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516375,
            property_id: 23556,
            feature_name: "Inside Spa",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516376,
            property_id: 23556,
            feature_name: "Ducted Heating",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516377,
            property_id: 23556,
            feature_name: "Pool Above Ground",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516378,
            property_id: 23556,
            feature_name: "Workshop",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516379,
            property_id: 23556,
            feature_name: "Intercom",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516380,
            property_id: 23556,
            feature_name: "Open Fire Place",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516381,
            property_id: 23556,
            feature_name: "Pool In Ground",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516382,
            property_id: 23556,
            feature_name: "Alarm System",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516383,
            property_id: 23556,
            feature_name: "Outside Spa",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516384,
            property_id: 23556,
            feature_name: "Dishwasher",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516385,
            property_id: 23556,
            feature_name: "Floorboards",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516386,
            property_id: 23556,
            feature_name: "Pay T V",
            feature_value: "no",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          },
          {
            id: 12516387,
            property_id: 23556,
            feature_name: "",
            feature_value: "yes",
            created_at: "2016-11-04 10:04:04",
            updated_at: "2016-11-04 10:04:04"
          }
        ],
        property_highlights: [],
        property_ideal_for: [],
        property_images: [
          {
            id: 201237,
            property_id: 23556,
            url: "http://images.realestateview.com.au/pics/161/10323161ao.jpg",
            format: "jpg",
            created_at: "2016-07-09 02:07:49",
            updated_at: "2016-07-09 02:07:49"
          },
          {
            id: 201238,
            property_id: 23556,
            url: "http://images.realestateview.com.au/pics/161/10323161bo.jpg",
            format: "jpg",
            created_at: "2016-07-09 02:07:49",
            updated_at: "2016-07-09 02:07:49"
          },
          {
            id: 201239,
            property_id: 23556,
            url: "http://images.realestateview.com.au/pics/161/10323161co.jpg",
            format: "jpg",
            created_at: "2016-07-09 02:07:49",
            updated_at: "2016-07-09 02:07:49"
          },
          {
            id: 201240,
            property_id: 23556,
            url: "http://images.realestateview.com.au/pics/161/10323161do.jpg",
            format: "jpg",
            created_at: "2016-07-09 02:07:49",
            updated_at: "2016-07-09 02:07:49"
          },
          {
            id: 201241,
            property_id: 23556,
            url: "http://images.realestateview.com.au/pics/161/10323161eo.jpg",
            format: "jpg",
            created_at: "2016-07-09 02:07:49",
            updated_at: "2016-07-09 02:07:49"
          },
          {
            id: 201242,
            property_id: 23556,
            url: "http://images.realestateview.com.au/pics/161/10323161fo.jpg",
            format: "jpg",
            created_at: "2016-07-09 02:07:49",
            updated_at: "2016-07-09 02:07:49"
          },
          {
            id: 201243,
            property_id: 23556,
            url: "http://images.realestateview.com.au/pics/161/10323161go.jpg",
            format: "jpg",
            created_at: "2016-07-09 02:07:49",
            updated_at: "2016-07-09 02:07:49"
          },
          {
            id: 201244,
            property_id: 23556,
            url: "http://images.realestateview.com.au/pics/161/10323161ho.jpg",
            format: "jpg",
            created_at: "2016-07-09 02:07:49",
            updated_at: "2016-07-09 02:07:49"
          }
        ],
        property_inspection_times: [
          {
            id: 523735,
            property_id: 23556,
            inspection_time: "03-Nov-2016 5:30PM to 06:00PM",
            created_at: "2016-11-01 10:04:03",
            updated_at: "2016-11-01 10:04:03"
          }
        ],
        property_land_details: [],
        property_objects: [
          {
            id: 200730,
            property_id: 23556,
            object_type: "floorplan",
            object_url: "http://images.realestateview.com.au/pics/161/10323161ao.gif",
            created_at: "2016-11-04 10:04:05",
            updated_at: "2016-11-04 10:04:05"
          }
        ],
        property_rents: [],
        property_rural_features: [],
        property_sold_details: [],
        property_vendors: [],
        property_views: []
    },
};

