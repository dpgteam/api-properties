export default {
    id: 27120,
    property_id: 23199,
    user_id: 2803,
    created_at: "2016-07-09 01:57:16",
    updated_at: "2016-07-09 01:57:16",
    agent: {
        id: 2803,
        name: "Julie Gloster",
        email: "julie-gloster-brad-teal@digitalpropertygroup.com",
        photo_url: "https://www.gravatar.com/avatar/541a721c731a51521717771b090fa96e.jpg?s=200&d=mm",
        uses_two_factor_auth: false,
        mobile: "0408 323 056",
        two_factor_reset_code: null,
        current_team_id: null,
        stripe_id: null,
        current_billing_plan: null,
        billing_state: null,
        vat_id: null,
        trial_ends_at: null,
        last_read_announcements_at: null,
        created_at: "2016-07-09 01:57:16",
        updated_at: "2016-07-09 01:57:16",
        email_aliases: [
            {
                id: 2846,
                user_id: 2803,
                email: "jgloster@bradteal.com.au",
                created_at: "2016-07-09 01:57:16",
                updated_at: "2016-07-09 01:57:16"
            }
        ],
        tax_rate: 0
    }
};