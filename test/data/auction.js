let past   = new Date().getFullYear() - 1,
    future = past + 2;
export default {
    past  : past + '-01-31 11:30:00',
    future_am: future + '-01-31 11:30:00',
    future_pm: future + '-01-31 13:30:00',
};