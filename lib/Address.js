'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _functions = require('./functions');

var _functions2 = _interopRequireDefault(_functions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Address
 * Interact with data for a single address.
 * @module  Address
 * @link https://platform.digitalpropertygroup.com/api/v1/properties/23199?api_token=KoAViLFXjXR5CTnSCRMaJzn7caBplNe3yvfkRCrff3thKtpQQAwTRd68Y0cd  Example Request
 */
var Address = function () {
  /**
   * constructor ( data )
   * 
   * @category Constructor
   * @param  {Object} data An Object containing Address data.
   * @return {void}    void      
   */
  function Address(data) {
    _classCallCheck(this, Address);

    this._data = data ? data : {};
  }
  /**
   * data
   * The input data for this address.
   * @category Properties
   * @return {Object}
   */


  _createClass(Address, [{
    key: 'data',
    get: function get() {
      return this._data;
    }
    /**
     * data = data
     * Sets this Address's data.
     * @category Setters
     * @param  {Object} data    The data you want to mass assign.
     * @return {void}    void
     */
    ,
    set: function set(data) {
      this._data = data;
    }
    /**
     * exists
     * @category Checks
     * @return {Boolean}    This Address has data.
     */

  }, {
    key: 'exists',
    get: function get() {
      var _this = this;

      return Object.keys(this._data).filter(function (key) {
        return _this._data[key] !== null;
      }).length > 0;
    }
    /**
     * full
     * @category Computed
     * @return {String}     Returns this Address as a pre-formatted string.
     */

  }, {
    key: 'full',
    get: function get() {
      var address = '';
      if (this.exists) {
        if (this.sub_number) {
          address += this.sub_number + '/';
        }
        if (this.number) {
          address += this.number + ' ';
        }
        address += this.street + ', ';
        address += this.suburb + ' ';
        address += this.postcode;
      }
      return address;
    }
    /**
     * sub_number
     * @category Properties
     * @return {Number}     This address's apartment / unit number, if it has one.
     */

  }, {
    key: 'sub_number',
    get: function get() {
      return this._data.sub_number ? parseInt(this._data.sub_number) : null;
    }
    /**
     * lot_number
     * @category Properties
     * @return {Number}     This address's street lot number, if it has one.
     */

  }, {
    key: 'lot_number',
    get: function get() {
      return this._data.lot_number ? parseInt(this._data.lot_number) : null;
    }
    /**
     * number
     * @category Properties 
     * @return {Number} This address's street number.
     */

  }, {
    key: 'number',
    get: function get() {
      return this._data.number ? parseInt(this._data.number) : null;
    }
    /**
     * street
     * @category Properties
     * @return {String}    This address's street.
     */

  }, {
    key: 'street',
    get: function get() {
      return this._data.street ? _functions2.default.ucwords(this._data.street) : null;
    }
    /**
     * suburb
     * @category Properties
     * @return {String}     This address's suburb.
     */

  }, {
    key: 'suburb',
    get: function get() {
      return this._data.suburb ? _functions2.default.ucwords(this._data.suburb) : null;
    }
    /**
     * state
     * @category Properties
     * @return {String}     This address's state acronym.
     */

  }, {
    key: 'state',
    get: function get() {
      return this._data.state ? this._data.state : null;
    }
    /**
     * postcode
     * @category Properties
     * @return {String} This address's postcode.
     */

  }, {
    key: 'postcode',
    get: function get() {
      return this._data.postcode ? this._data.postcode : null;
    }
    /**
     * lat
     * @category Properties
     * @return {String} This address's latitude.
     */

  }, {
    key: 'lat',
    get: function get() {
      return this._data.lat ? this._data.lat : null;
    }
    /**
     * lon
     * @category Properties
     * @return {String} This address's longitude.
     */

  }, {
    key: 'lon',
    get: function get() {
      return this._data.lon ? this._data.lon : null;
    }
    /**
     * coords
     * @category ComputedProperties
     * @return {Object} This address's lat / lon coordinates.
     */

  }, {
    key: 'coords',
    get: function get() {
      return this.lat && this.lon ? { lat: this.lat, lon: this.lon } : null;
    }
  }]);

  return Address;
}();

exports.default = Address;