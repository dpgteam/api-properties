'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _functions = require('./functions');

var _functions2 = _interopRequireDefault(_functions);

var _Address = require('./Address');

var _Address2 = _interopRequireDefault(_Address);

var _Agent = require('./Agent');

var _Agent2 = _interopRequireDefault(_Agent);

var _Auction = require('./Auction');

var _Auction2 = _interopRequireDefault(_Auction);

var _Inspection = require('./Inspection');

var _Inspection2 = _interopRequireDefault(_Inspection);

var _Properties = require('./Properties');

var _Properties2 = _interopRequireDefault(_Properties);

var _Property = require('./Property');

var _Property2 = _interopRequireDefault(_Property);

var _PropertyMarker = require('./PropertyMarker');

var _PropertyMarker2 = _interopRequireDefault(_PropertyMarker);

var _Scratch = require('./Scratch');

var _Scratch2 = _interopRequireDefault(_Scratch);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Dpg Bootstrap
 * Bootstraps Dpg Classes to Dpg Object.
 * @module  Dpg
 * @link functions
 * @link Address
 * @link Agent
 * @link Auction
 * @link Inspection
 * @link Properties
 * @link Property
 * @link PropertyMarker
 */
var Dpg = {
  fn: _functions2.default,
  Address: _Address2.default,
  Agent: _Agent2.default,
  Auction: _Auction2.default,
  Inspection: _Inspection2.default,
  Properties: _Properties2.default,
  Property: _Property2.default,
  PropertyMarker: _PropertyMarker2.default,
  Scratch: _Scratch2.default
};
exports.default = Dpg;