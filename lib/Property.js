'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _functions = require('./functions');

var _functions2 = _interopRequireDefault(_functions);

var _PropertyMarker = require('./PropertyMarker');

var _PropertyMarker2 = _interopRequireDefault(_PropertyMarker);

var _Inspection = require('./Inspection');

var _Inspection2 = _interopRequireDefault(_Inspection);

var _Auction = require('./Auction');

var _Auction2 = _interopRequireDefault(_Auction);

var _Agent = require('./Agent');

var _Agent2 = _interopRequireDefault(_Agent);

var _Address = require('./Address');

var _Address2 = _interopRequireDefault(_Address);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Property
 * Contains data for a single property and a set of dynamic getter to 
 * retrieve only relevant property data.
 *     Examples: 
 *         - Only future auctions | inspections.
 *         - Correct price | rent to display.
 * @link https://platform.digitalpropertygroup.com/api/v1/properties/23199?api_token=KoAViLFXjXR5CTnSCRMaJzn7caBplNe3yvfkRCrff3thKtpQQAwTRd68Y0cd  Example Request
 * @type {Object}
 */

var Property = function () {
    /**
     * constructor ( data )
     * Creates a new Property instance and setups empty properties.
     * @category Constructor
     * @param  {Object} data An object containing Property data.
     * @return {void}    void      
     */
    function Property(data) {
        _classCallCheck(this, Property);

        this._data = data;
        this._inspections = [];
        this._auction = null;
        this._address = null;
        this._images = null;
    }
    /**
     * property
     * @category Data
     * @return {Object} The property's data.
     */


    _createClass(Property, [{
        key: 'getFeature',

        /**
         * getFeature ( featureName )
         * Checks this Property's features and returns the value of {featureName}, if there is one.
         * @category Methods
         * @param  {String} featureName     The name of this Property's feature to retrieve.
         * @return {Number|String}          This Property feature's value.
         */
        value: function getFeature(featureName) {
            if (this.isCommerical) return;

            var feature = this.property.property_features.filter(function (a) {
                return a.feature_name.toLowerCase() === featureName.toLowerCase();
            })[0];
            if (parseInt(feature.feature_value)) {
                feature = parseInt(feature.feature_value);
            }

            return feature.feature_value ? feature.feature_value : feature;
        }
        /**
         * bedrooms
         * The number of bedrooms at this property.
         * @category Properties
         * @return {Number}     How many bedrooms this property has.
         */

    }, {
        key: 'hasCategory',

        /**
         * hasCategory ( category )
         * @category Methods
         * @param  {String}  category The category to check.
         * @return {Boolean}          This Property is in {category}
         */
        value: function hasCategory(category) {
            return category === null || this.category.toLowerCase() === category.toLowerCase();
        }
        /**
         * hasType ( type )
         * @category Methods
         * @param  {String}  type   The type to check.
         * @return {Boolean}        This Property is in {type}
         */

    }, {
        key: 'hasType',
        value: function hasType(type) {
            return type === null || this.type.toLowerCase() === type.toLowerCase();
        }
        /**
         * hasPriceMin ( min )
         * @category Methods
         * @param  {Number}  min    The minimum Price to check.
         * @return {Boolean}        This Property's price is at least {min} or greater.
         */

    }, {
        key: 'hasMinPrice',
        value: function hasMinPrice(min) {
            if (this.isSale) {
                return !min || this.price >= min;
            } else {
                return !min || this.weeklyRent >= min;
            }
        }
        /**
         * hasPriceMax ( max )
         * @category Methods
         * @param  {Number}  max    The maximum Price to check.
         * @return {Boolean}        This Property's price is no greater than {max}.
         */

    }, {
        key: 'hasMaxPrice',
        value: function hasMaxPrice(max) {
            if (this.isSale) {
                return !max || this.price <= max;
            } else {
                return !max || this.weeklyRent <= max;
            }
        }
        /**
         * hasFeature ( val, featureName )
         * Checks this Property's features for {featureName} and returns true if there are {val} or more.
         * @category Methods
         * @param  {Number}  val         The minimum value this Property's feature should have.
         * @param  {String}  featureName The name of this Property's feature to check.
         * @return {Boolean}             This Property's {featureName} is at least {val}.
         */

    }, {
        key: 'hasFeature',
        value: function hasFeature(val, featureName) {
            return this.getFeature(featureName) >= val;
        }
    }, {
        key: 'property',
        get: function get() {
            return this._data ? this._data : null;
        }
        /**
         * exists
         * @category Checks
         * @return {Boolean}    This property contains data.
         */

    }, {
        key: 'exists',
        get: function get() {
            return this.property && Object.keys(this.property).length > 0;
        }
        /**
         * isSale
         * @category Checks
         * @return {Boolean}    This property is for sale.
         */

    }, {
        key: 'isSale',
        get: function get() {
            return this._data.price > 0;
        }
        /**
         * isLease
         * @category Checks
         * @return {Boolean}    This property is for lease.
         */

    }, {
        key: 'isLease',
        get: function get() {
            return !this.isSale;
        }
        /**
         * isCommercial
         * @category Checks
         * @return {Boolean}    This is a commercial site.
         */

    }, {
        key: 'isCommercial',
        get: function get() {
            return this._data.property_type === 'commercial';
        }
        /**
         * isResidential
         * @category Checks
         * @return {Boolean}    This is a residential site.
         */

    }, {
        key: 'isResidential',
        get: function get() {
            return ['residential', 'rental'].indexOf(this.property.property_type.toLowerCase()) > -1;
        }
        /**
         * category
         * @category Properties
         * @return {String} This property's category: sale|lease|commercial.
         */

    }, {
        key: 'category',
        get: function get() {
            var category = '';
            if (this.isResidential) {
                category = this.isSale ? 'sale' : 'lease';
            } else {
                category = this.isSale ? 'commercial_sale' : 'commercial_lease';
            }
            return category;
        }
        /**
         * type
         * @category Properties
         * @return {String}     This Property's type.
         */

    }, {
        key: 'type',
        get: function get() {
            var type = this._data.category;
            if (!type) // fallback to property_type if null.
                type = this._data.property_type;
            return type.toLowerCase() === 'apartment' ? 'unit' : type;
        }
        /**
         * id
         * @category Properties
         * @return {String} This property's unique id.
         */

    }, {
        key: 'id',
        get: function get() {
            return this.property.property_unique_id;
        }
        /**
         * branch
         * @category Properties
         * @return {String}     The unique id for the branch this property belongs to.
         */

    }, {
        key: 'branch',
        get: function get() {
            return this.property.branch_id;
        }
        /**
         * status
         * @category Properties
         * @return {String} This property's listing status: current | sold | leased.
         */

    }, {
        key: 'status',
        get: function get() {
            return this.property.property_status;
        }
        /**
         * headline
         * @category Properties
         * @return {String}     This property's headline.
         */

    }, {
        key: 'headline',
        get: function get() {
            return this.property.headline;
        }
        /**
         * description
         * @category Properties
         * @return {String} This Property's description with newlines replace with paragraph tags.
         */

    }, {
        key: 'description',
        get: function get() {
            var desc = '<p>' + this.property.description + '</p>';
            return desc.replace(/\n/g, '</p><p>');
        }
        /**
         * video
         * @category Properties
         * @return {String} A url to a video of this Property, if there is one.
         */

    }, {
        key: 'video',
        get: function get() {
            return this.property.video_link;
        }
        /**
         * coords
         * @category Properties
         * @return {Object} This Property's map coordinates.
         */

    }, {
        key: 'coords',
        get: function get() {
            return {
                lat: this._data.lat,
                lon: this._data.lon
            };
        }
        /**
         * address
         * @category Properties
         * @return {Address}     This Property's Address data.
         */

    }, {
        key: 'address',
        get: function get() {
            if (!this._address) {
                this._address = new _Address2.default({
                    sub_number: this.property.address_sub_number,
                    lot_number: this.property.address_lot_number,
                    number: this.property.address_street_number,
                    street: _functions2.default.ucwords(this.property.address_street),
                    suburb: _functions2.default.ucwords(this.property.address_suburb),
                    state: this.property.address_state,
                    postcode: this.property.address_postcode,
                    coords: this.coords
                });
            }
            return this._address;
        }
        /**
         * flooplan
         * @category Properties
         * @return {String} A url to this Property's floor plan, if there is one.
         */

    }, {
        key: 'floorplan',
        get: function get() {
            var floorplan = this.property.property_objects.filter(function (a) {
                return a.object_type === 'floorplan';
            })[0];
            return floorplan !== undefined ? floorplan.object_url : null;
        }
        /**
         * images
         * @category Properties
         * @return {Array}  A list of urls for this Property's images.
         */

    }, {
        key: 'images',
        get: function get() {
            var _this = this;

            if (!this._images) {
                this._images = [];
                this.property.property_images.forEach(function (img) {
                    return _this._images.push(img.url);
                });
            }
            return this._images;
        }
        /**
         * hasInspections
         * @category Checks
         * @return {Boolean}    This Property has future inspections.
         */

    }, {
        key: 'hasInspections',
        get: function get() {
            return this.inspections[0] !== null;
        }
        /**
         * hasAuction
         * @category Checks
         * @return {Boolean} This Property has a future auction.
         */

    }, {
        key: 'hasAuction',
        get: function get() {
            return this.auction.exists ? this.auction.exists : false;
        }
        /**
         * hasInspectionsOrAuctions
         * @category Checks
         * @return {Boolean}    This Property has future inspections or auctions.
         */

    }, {
        key: 'hasInspectionsOrAuctions',
        get: function get() {
            return this.hasInspections || this.hasAuction;
        }
        /**
         * inspections
         * @category ListObjects
         * @return {Array}  This Property's upcoming inspections, if there are any.
         */

    }, {
        key: 'inspections',
        get: function get() {
            if (!this._inspections.length) {
                var _iteratorNormalCompletion = true;
                var _didIteratorError = false;
                var _iteratorError = undefined;

                try {
                    for (var _iterator = this.property.property_inspection_times[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                        var _inspection = _step.value;

                        if (_functions2.default.isAfterToday(_inspection.inspection_time)) {
                            this._inspections.push(new _Inspection2.default(_inspection));
                        }
                        this._inspections = this._inspections.length ? this._inspections : [null];
                    }
                } catch (err) {
                    _didIteratorError = true;
                    _iteratorError = err;
                } finally {
                    try {
                        if (!_iteratorNormalCompletion && _iterator.return) {
                            _iterator.return();
                        }
                    } finally {
                        if (_didIteratorError) {
                            throw _iteratorError;
                        }
                    }
                }
            }return this._inspections;
        }
        /**
         * auction
         * @category ListObjects
         * @return {String} This Property's future auction date, if there is one.
         */

    }, {
        key: 'auction',
        get: function get() {
            if (!this._auction) {
                this._auction = new _Auction2.default(this.property.auction_date);
            }
            return this._auction;
        }
        /**
         * available
         * @category Checks
         * @return {Boolean}    This Property is currently available to buy / lease.
         */

    }, {
        key: 'available',
        get: function get() {
            return ['leased', 'sold', 'withdrawn'].indexOf(this.property.property_status) < 0;
        }
    }, {
        key: 'bedrooms',
        get: function get() {
            return this.getFeature('bedrooms');
        }
        /**
         * bathrooms
         * The number of bathrooms at this property.
         * @category Properties
         * @return {Number}     How many bathrooms this property has.
         */

    }, {
        key: 'bathrooms',
        get: function get() {
            return this.getFeature('bathrooms');
        }
        /**
         * carspaces
         * The number of total car spaces at this property.
         * @category Properties
         * @return {Number}     How many car spaces this property has.
         */

    }, {
        key: 'carspaces',
        get: function get() {
            return parseInt(this.getFeature('carports')) + parseInt(this.getFeature('garages'));
        }
        /**
         * weeklyRent
         * @category Properties
         * @return {Number} This Property's weekly rent amount, if there is one.
         */

    }, {
        key: 'weeklyRent',
        get: function get() {
            var rent = this.rent;
            if (rent.rent_period === 'weekly') {
                rent = rent.rent_amount;
            } else {
                rent = rent.rent_amount / 365 * 7;
            }
            return rent;
        }
        /**
         * annualRent
         * @category Properties
         * @return {Number}     This Property's annual rent amount, if there is one.
         */

    }, {
        key: 'annualRent',
        get: function get() {
            var rent = this.rent;
            if (rent.rent_period === 'weekly') {
                rent = rent.rent_amount / 7 * 365;
            } else {
                rent = rent.rent_amount;
            }
            return parseInt(rent);
        }
        /**
         * rent
         * @category Properties
         * @return {Object}     This Property's rental data.
         */

    }, {
        key: 'rent',
        get: function get() {
            var rent = 0;
            if (this.property.property_rents) {
                rent = this.property.property_rents[0];
            }
            return rent;
        }
        /**
         * price
         * @category Properties
         * @return {String}     This Property's display price.
         */

    }, {
        key: 'price',
        get: function get() {
            var price = 0;
            if (this.property.price_display.toLowerCase() !== 'yes') {
                price = 'Contact Agent';
            } else {
                price = this.property.price_view ? this.property.price_view : '$' + this.property.price.toLocaleString(0);
            }
            return price;
        }
        /**
         * hasAgents
         * @category Checks
         * @return {Boolean}    This Property has real estate agents.
         */

    }, {
        key: 'hasAgents',
        get: function get() {
            return this.agents.length > 0;
        }
        /**
         * agents
         * @category ListObjects
         * @return {Array}  This Property's Agent data.
         */

    }, {
        key: 'agents',
        get: function get() {
            var _this2 = this;

            this._agents = [];
            if (this.property && this.property.property_agents) {
                this.property.property_agents.forEach(function (agent) {
                    return _this2._agents.push(new _Agent2.default(agent));
                });
            }
            return this._agents;
        }
    }]);

    return Property;
}();

exports.default = Property;