'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _functions = require('./functions');

var _functions2 = _interopRequireDefault(_functions);

var _Property = require('./Property');

var _Property2 = _interopRequireDefault(_Property);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Properties
 * Prototype for a list of properties obtained from the DPG Platform API.
 * Provides methods for filtering and sorting propertires based on their
 * attributes.
 *     - Sort properties by price, feature, suburb, postcode...
 *     - Filter property by category, type, price, features...
 *  @module  Properties
 *  @link https://platform.digitalpropertygroup.com/api/v1/properties/?api_token=KoAViLFXjXR5CTnSCRMaJzn7caBplNe3yvfkRCrff3thKtpQQAwTRd68Y0cd&orderBy=created_at,desc   Sample API Property request.
 */

var Properties = function () {
    /**
     * constructor ( properties )
     * Creates a new instance and adds property data from {properties}. 
     * @category Constructor
     * @param  {Array} properties 
     * @return {void}    void            
     */
    function Properties(properties) {
        _classCallCheck(this, Properties);

        this._properties = [];
        this.filters = {};
        if (properties.length !== undefined) {
            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
                for (var _iterator = properties[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                    var property = _step.value;

                    this.push(property);
                }
            } catch (err) {
                _didIteratorError = true;
                _iteratorError = err;
            } finally {
                try {
                    if (!_iteratorNormalCompletion && _iterator.return) {
                        _iterator.return();
                    }
                } finally {
                    if (_didIteratorError) {
                        throw _iteratorError;
                    }
                }
            }
        }
    }
    /**
     * push ( Property )
     * Creates new Property and if it exists,  pushes it to the property list.
     * @category Methods
     * @param  {Property} property
     * @return {void}    void
     */


    _createClass(Properties, [{
        key: 'push',
        value: function push(property) {
            property = new _Property2.default(property);
            if (property.exists) this._properties.push(property);
        }
        /**
         * length
         * The total number of properties this list contains.
         * @category Properties
         * @return {Number}
         */

    }, {
        key: 'sortByKey',

        /**
         * sortByKey ( key, asc )
         * Sort this list of properties by {key}.  
         * @category Sorting
         * @param  {String}  key
         * @param  {Boolean} asc
         * @return {Array}
         */
        value: function sortByKey(key) {
            var asc = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : true;

            return asc ? this._properties.sort(function (a, b) {
                return a[key] < b[key];
            }) : this._properties.sort(function (a, b) {
                return a[key] > b[key];
            });
        }
        /**
         * minRent
         * The property in this list with the highest rent amount.
         * @category Properties
         * @return {Property} 
         */

    }, {
        key: 'filterByCategory',

        /**
         * filterByCategory ( property )
         * {property} category matches selected category.
         * @category Filters
         * @param  {Property} property
         * @return {Boolean}
         */
        value: function filterByCategory(property) {
            return property.category.toLowerCase() === this.filters.category.toLowerCase();
        }
        /**
         * filterByType ( property )
         * {property} type matches selected type.
         * @category Filters
         * @param  {Property} property
         * @return {Boolean}
         */

    }, {
        key: 'filterByType',
        value: function filterByType(property) {
            var type = this.filters.type;
            return type ? property.type.toLowerCase() === type.toLowerCase() : true;
        }
        /**
        * filterByType ( property )
        * {property} rent exceeds minimum.
        * @category Filters
        * @param  {Property} Property
        * @return {Boolean}
        */

    }, {
        key: 'filterByMinRent',
        value: function filterByMinRent(property) {
            return this.filters.price_min ? property.weeklyRent >= this.filters.price_min : true;
        }
        /**
         * filterMinPrice ( property )
         * {property} price exceeds minimum.
         * @category Filters
         * @param  {Property} Property
         * @return {Boolean}
         */

    }, {
        key: 'filterByMinPrice',
        value: function filterByMinPrice(property) {
            return this.filters.price_min ? property.price >= this.filters.price_min : true;
        }
        /**
         * filterMaxRent ( property )
         * {property} rent does not exceed maximum.
         * @category Filters
         * @param  {Property} Property
         * @return {Boolean}
         */

    }, {
        key: 'filterByMaxRent',
        value: function filterByMaxRent(property) {
            return this.filters.price_max ? property.weeklyRent <= this.filters.price_max : true;
        }
        /**
         * filterByMaxPrice ( property )
         * {property} price does not exceed maximum.
         * @category Filters
         * @param  {Property} Property
         * @return {Boolean}
         */

    }, {
        key: 'filterByMaxPrice',
        value: function filterByMaxPrice(property) {
            return this.filters.price_max ? property.price <= this.filters.price_max : true;
        }
        /**
         * filterByMix ( property )
         * {property} price|rent exceeds minimum price | rent.
         * @category Filters
         * @param  {Property} Property
         * @return {Boolean}
         */

    }, {
        key: 'filterByMin',
        value: function filterByMin(property) {
            return property.isSales ? this.filterByMinPrice(property) : this.filterByMinRent(property);
        }
        /**
         * filterByTMax ( property )
         * {property} price|rent does not exceed maximum
         * @category Filters
         * @param  {Property} property
         * @return {Boolean}
         */

    }, {
        key: 'filterByMax',
        value: function filterByMax(property) {
            return property.isSales ? this.filterByMaxPrice(property) : this.filterByMaxRent(property);
        }
        /**
         * filterByBedrooms ( property )
         * {property} has minimum bedrooms.
         * @category Filters
         * @param  {Property} property
         * @return {Boolean}
         */

    }, {
        key: 'filterByBedrooms',
        value: function filterByBedrooms(property) {
            var bed = void 0;
            return bed && property.isResidential ? property.bedrooms >= bed : true;
        }
        /**
         * filterByBathrooms ( property )
         * {property} has minimum bathrooms.
         * @category Filters
         * @param  {Property} property
         * @return {Boolean}
         */

    }, {
        key: 'filterByBathrooms',
        value: function filterByBathrooms(property) {
            var bath = this._filters.bath;
            return bath && property.isResidential ? property.bathrooms >= bath : true;
        }
        /**
         * filterByCarspaces ( property )
         * {property} has minimum car spaces.
         * @category Filters
         * @param  {Property} property
         * @return {Boolean}
         */

    }, {
        key: 'filterByCarspaces',
        value: function filterByCarspaces(property) {
            var car = this.filters.car;
            return car && property.isResidential ? property.carspace >= car : true;
        }
        /**
         * filterByAuction ( property )
         * {property} has future auction.
         * @category Filters
         * @param  {Property} property
         * @return {Boolean}
         */

    }, {
        key: 'filterByAuction',
        value: function filterByAuction(property) {
            return this.filters.auctions ? property.hasAuction : true;
        }
        /**
         * filterByInspection ( property )
         * {property} has future inspections.
         * @category Filters
         * @param  {Property} property
         * @return {Boolean}
         */

    }, {
        key: 'filterByInspection',
        value: function filterByInspection(property) {
            return this.filters.inspections ? property.hasInspections : true;
        }
    }, {
        key: 'length',
        get: function get() {
            return this._properties.length;
        }
        /**
         * filterLength
         * The total number of properties this list has which match the current search parameters.
         * @category Properties
         * @return {Number}
         */

    }, {
        key: 'filterLength',
        get: function get() {
            return this.list.length;
        }
        /**
         * Property.hasProperties
         * This list contains properties.
         * @category Properties
         * @return {Boolean}
         */

    }, {
        key: 'hasProperties',
        get: function get() {
            return this.length > 0;
        }
        /**
         * sorted
         * This list sorted by current search options.
         * @category Sorting
         * @return {Array}
         */

    }, {
        key: 'sorted',
        get: function get() {
            return this.sortByKey(this._sort.key, this._sort.asc);
        }
        /**
         * list
         * A list of properties that match the search filters applied.
         * @category Filters
         * @return {Array}
         */

    }, {
        key: 'list',
        get: function get() {
            var props = this._properties.filter(this.filterByCategory.bind(this)).filter(this.filterByType.bind(this))
            // .filter( this.filterByMin.bind(this)        )
            // .filter( this.filterByMax.bind(this)   )
            // .filter( this.filterByBedrooms.bind(this)   )
            // .filter( this.filterByBathrooms.bind(this)  )
            // .filter( this.filterByCarspaces.bind(this)  )
            // .filter( this.filterByAuction.bind(this)    )
            // .filter( this.filterByInspection.bind(this) )
            ;
            // props.forEach(p => console.log(p.type));

            return props;
        }
        /**
         * categoryCount
         * A list of categories found in this list and the number of properties for each.
         * @category Counts
         * @return {Object}
         */

    }, {
        key: 'categoryCount',
        get: function get() {
            var counts = {
                sale: 0,
                lease: 0,
                commercial_sale: 0,
                commercial_lease: 0
            };
            this.list.forEach(function (prop) {
                return counts[prop.category] += 1;
            });
            counts.total = Object.keys(counts).reduce(function (a, val) {
                return a + counts[val];
            }, 0);
            return counts;
        }
        /**
         * typeCount
         * A list of types found in this list and the number of properties for each.
         * @category Counts
         * @return {Object} 
         */

    }, {
        key: 'typeCount',
        get: function get() {
            var counts = {};
            this.list.forEach(function (prop) {
                return counts[prop.type] = 0;
            });
            this.list.forEach(function (prop) {
                return counts[prop.type] += 1;
            });
            counts.total = Object.keys(counts).reduce(function (a, val) {
                return a + counts[val];
            }, 0);
            return counts;
        }
        /**
         * priceCount
         * The number of properties in this list priced 
         * per $100,000 - sale | per $100 - rental 
         * up to $900,000 | $900.
         * @category Counts
         * @return {Object}
         */

    }, {
        key: 'priceCount',
        get: function get() {
            var counts = {};
            for (var i = 1; i < 10; i++) {
                counts[i * 100000] = 0;
            }
            this.list.forEach(function (prop) {
                var key = parseInt(prop._data.price / 100000) * 100000;
                key = key <= 900000 ? key : 900000;
                counts[key] += 1;
            });
            return counts;
        }
        /**
         * priceAverage
         * The average property price for this list's filtered results.
         * @category Properties
         * @return {Number} 
         */

    }, {
        key: 'priceAverage',
        get: function get() {
            var avg = this.list.reduce(function (x, val) {
                return x + val._data.price;
            }, 0) / this.list.length;
            return parseInt(Math.round(avg / 1000)) * 1000;
        }
        /**
         * sorting
         * This list's current sort order.
         * @category Sorting
         * @return {Object} 
         */

    }, {
        key: 'sorting',
        get: function get() {
            return this._sorting ? this._sorting : { key: 'price', asc: true };
        }
        /**
         * sorting = Object
         * Update this list's sort order.
         * @category Sorting
         * @param  {Object} sorting 
         * @return {void}    void         
         */
        ,
        set: function set(sorting) {
            this._sorting = {
                key: sorting.key ? sorting.key : 'price',
                price: sorting.price ? sorting.price : true
            };
        }
    }, {
        key: 'minRent',
        get: function get() {
            return this.sortByKey('rent')[0];
        }
        /**
         * maxRent
         * The property in this list with the highest rent amount.
         * @category Properties
         * @return {Property} 
         */

    }, {
        key: 'maxRent',
        get: function get() {
            return this.sortByKey('rent', false)[0];
        }
        /**
         * minPrice
         * The property in this list with the lowest price.
         * @category Properties
         * @return {Property}
         */

    }, {
        key: 'minPrice',
        get: function get() {
            return this.sortByKey('price')[0];
        }
        /**
         * maxPrice
         * The property in this list with the highest price.
         * @category Properties
         * @return {Property}
         */

    }, {
        key: 'maxPrice',
        get: function get() {
            return this.sortByKey('price', false)[0];
        }
        /**
         * Property.max
         * The filtered property with the highest lease | rent amount.
         * @category Properties
         * @return {Property}
         */

    }, {
        key: 'max',
        get: function get() {
            return this.maxPrice.price > 0 ? this.maxPrice : this.leasePrice;
        }
        /**
         * filters
         * This list's current or default search filters.
         * @category Filters
         * @return {Object} 
         */

    }, {
        key: 'filters',
        get: function get() {
            if (!this._filters) {
                this._filters = {};
            }
            return this._filters;
        }
        /**
         * filters = Object
         * Update this list's search filters.
         * @category Filters
         * @param  {Object} filters 
         * @return {void}    void         
         */
        ,
        set: function set(filters) {
            this._filters = {
                category: filters.category ? filters.category : 'sale',
                type: filters.type ? filters.type : null,
                price_min: filters.price_min ? filters.price_min : 0,
                price_max: filters.price_max ? filters.price_max : 0,
                bed: filters.bed ? filters.bed : 0,
                bath: filters.bath ? filters.bath : 0,
                car: filters.car ? filters.car : 0,
                auction: filters.auction ? filters.auction : false,
                inspections: filters.inspections ? filters.inspections : false
            };
        }
    }]);

    return Properties;
}();

exports.default = Properties;