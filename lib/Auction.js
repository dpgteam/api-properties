'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _functions = require('./functions');

var _functions2 = _interopRequireDefault(_functions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Auction
 * Interact with data for a Property auction.
 * @module  Auction
 */
var Auction = function () {
    /**
     * constructor ( dateStr )
     * Parses {dateStr} and sets this Auction's properties.
     * @param  {String} dateStr A String containing the Auction's date and time.
     * @return {void}    void
     */
    function Auction(dateStr) {
        _classCallCheck(this, Auction);

        this._data = null;
        if (dateStr) {
            this.parseAuctionDate(dateStr);
        }
    }
    /**
     * parseAuctionDate ( dateStr )
     * If {dateStr} is in the future, it is parsed and an object of date and time data is created.
     * @param {String} dateStr
     * @return {Object} Pre-formatted time and date data.
     */


    _createClass(Auction, [{
        key: 'parseAuctionDate',
        value: function parseAuctionDate(dateStr) {
            var auction = null;
            if (dateStr !== null) {
                if (_functions2.default.isAfterToday(dateStr)) {
                    auction = {};
                    var date = new Date(dateStr.substring(0, 10));
                    /* Extract, time data. */
                    auction.date = date;
                    auction.time = dateStr.substring(11);
                    auction.hour = auction.time.substring(0, auction.time.indexOf(':'));
                    auction.minute = auction.time.substring(auction.time.lastIndexOf(':') - 2, auction.time.lastIndexOf(':'));
                    auction.am = auction.hour < 12 ? 'am' : 'pm';
                    auction.timestamp = auction.date.setHours(auction.hour, auction.minute, 0, 0);
                    /* Format hour if PM. */
                    if (auction.am === 'pm') {
                        auction.hour -= 12;
                    }
                    auction.str = dateStr.substring(0, 10);
                    auction.weekday = _functions2.default.weekdays()[date.getDay()];
                    auction.date = date.getDate();
                    auction.month = _functions2.default.months()[date.getMonth()];
                    auction.year = date.getFullYear();
                }
            }
            this._data = auction;
            return this._data;
        }
        /**
         * month
         * @return {String} This Auction's month formatted to a three letter String.
         */

    }, {
        key: 'month',
        get: function get() {
            return this._data ? this._data.month : null;
        }
        /**
         * weekday
         * @return {String} The weekday this Auction will take place.
         */

    }, {
        key: 'weekday',
        get: function get() {
            return this._data ? this._data.weekday : null;
        }
        /**
         * am
         * @return {String} This auction takes place in 'am' or 'pm'.
         */

    }, {
        key: 'am',
        get: function get() {
            return this._data ? this._data.am : null;
        }
        /**
         * hour
         * @return {Number} The hour this Auction takes place.
         */

    }, {
        key: 'hour',
        get: function get() {
            return this._data ? parseInt(this._data.hour) : null;
        }
        /**
         * minute
         * @return {String} The minute this auction takes place.
         */

    }, {
        key: 'minute',
        get: function get() {
            return this._data ? this._data.minute : null;
        }
        /**
         * date
         * @return {Number} This Auction's Unix timestamp.
         */

    }, {
        key: 'date',
        get: function get() {
            return this._data ? parseInt(this._data.date) : null;
        }
        /**
         * exists
         * @return {Boolean} True if this Auction has valid data.
         */

    }, {
        key: 'exists',
        get: function get() {
            return this._data !== null;
        }
    }]);

    return Auction;
}();

exports.default = Auction;