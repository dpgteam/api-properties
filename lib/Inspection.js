'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _functions = require('./functions');

var _functions2 = _interopRequireDefault(_functions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Inspection
 * Interact with data for a single Property inspection.
 * @module  Inspection
 */
var Inspection = function () {
    /**
     * constructor ( data )
     * Creates a new Inspection instance with {data}.
     * @param  {Object} data    An Object containing property inspection data.
     * @return {void}    void
     */
    function Inspection(data) {
        _classCallCheck(this, Inspection);

        this._data = data;
        this.parseInspectionDate(this._data.inspection_time);
    }
    /**
     * parseInspectionData ( dateStr )
     * If {dateStr} is in the future, it is parsed and an object of date and time data is created.
     * @param  {String} dateStr
     * @return {Object} Pre-formatted time and date data.
     */


    _createClass(Inspection, [{
        key: 'parseInspectionDate',
        value: function parseInspectionDate(dateStr) {
            /* Split string into substrings. */
            var str = dateStr.substring(0, 11),
                times = dateStr.substring(11),
                start = times.substring(0, times.indexOf('to')).replace(/ /g, ''),
                end = times.substring(times.indexOf('to') + 2).replace(/ /g, '');
            /* Parse am|pm to var. */
            var am = start.substring(start.length - 2);
            /* Create Start time Object. */
            this._start = {
                time: start,
                hour: start.substring(0, start.indexOf(':')),
                minute: start.substring(start.indexOf(':') + 1).replace(/a|A|p|P|m|M/g, ''),
                am: am
            };
            /* Create End time Object. */
            this._end = {
                time: end,
                hour: end.substring(0, end.indexOf(':')),
                minute: end.substring(end.indexOf(':') + 1).replace(/a|A|p|P|m|M/g, ''),
                am: end.substring(end.length - 2)
            };
            /* Convert date str to timestamp and human readable with start time. */
            var dateStamp = new Date(str).setHours(am = 'am' ? this.start.hour : this.start.hour + 12, this.start.minute, 0, 0);
            var date = new Date(dateStamp);
            /* Create date object. */
            this._date = {
                str: str,
                dateTime: dateStamp,
                weekday: _functions2.default.weekdays()[date.getDay()],
                date: date.getDate(),
                month: _functions2.default.months()[date.getMonth()],
                year: date.getFullYear()
            };
        }
        /**
         * date
         * @return {Object} This Inspection's date.
         */

    }, {
        key: 'date',
        get: function get() {
            return this._date;
        }
        /**
         * start
         * @return {Object} This inspection's start time.
         */

    }, {
        key: 'start',
        get: function get() {
            return this._start;
        }
        /**
         * end
         * @return {Object} This inspection's end time.
         */

    }, {
        key: 'end',
        get: function get() {
            return this._end;
        }
    }]);

    return Inspection;
}();

exports.default = Inspection;