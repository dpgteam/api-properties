'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Scratch = function () {
    function Scratch() {
        _classCallCheck(this, Scratch);
    }

    _createClass(Scratch, [{
        key: 'category',

        /**
         * This property's category: sale|lease|commercial.
         * @return {String}
         */
        get: function get() {
            var category = '';
            var categories = ['sale', 'lease', 'commercial_sale', 'commercial_lease'];
            switch (this._data.property_type) {
                case 'residential':
                    category = 'sale';
                    break;
                case 'rental':
                    category = 'lease';
                    break;
                case 'commercial':
                    category = this._data.price > 0 ? 'commercial_sale' : '_lease';
                    break;
            }
            return category;
        }
    }]);

    return Scratch;
}();