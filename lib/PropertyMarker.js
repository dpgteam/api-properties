'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _functions = require('./functions');

var _functions2 = _interopRequireDefault(_functions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Class prototype to handle map markers for a Property.
 * @module PropertyMarker
 */
var PropertyMarker = function () {
    /**
     * constructor ( data )
     * Pass in the data to use for this PropertyMarker
     * @param  {Object} data 
     * @return {void}    void      
     */
    function PropertyMarker(data) {
        _classCallCheck(this, PropertyMarker);

        this._data = data;
    }

    _createClass(PropertyMarker, [{
        key: 'marker',
        get: function get() {
            return new google.maps.Marker({
                position: this.coords,
                map: this.map,
                title: this.title,
                infoWindow: this.infoWindow
            });
        }
        /**
         * title
         * @return {String} This PropertyMarker's title.
         */

    }, {
        key: 'title',
        get: function get() {
            return this._data.title;
        }
        /**
         * title = title
         * Set a title for this PropertyMarker.
         * @param  {String} title Set a title for this PropertyMarker.
         * @return {void}    void       
         */
        ,
        set: function set(title) {
            this._data.title = title;
        }
        /**
         * coords
         * @return {Object} This PropertyMarker's map coordinates.
         */

    }, {
        key: 'coords',
        get: function get() {
            return this._data.address.coords;
        }
        /**
         * address
         * @return {Address} This PropertyMarker's address data.
         */

    }, {
        key: 'address',
        get: function get() {
            return this._data.address ? this._data.address : null;
        }
        /**
         * address = address
         * Set an Address for this PropertyMarker.
         * @param  {Address} address This PropertyMarker's Address data.
         * @return {void}    void         
         */
        ,
        set: function set(address) {
            this._data.address = address;
        }
        /**
         * map
         * @return {google.map} The map this PropertyMarker is attached to.
         */

    }, {
        key: 'map',
        get: function get() {
            return this._data.map ? this._data.map : null;
        }
        /**
         * map = map
         * @param  {google.map} map The map to attach this PropertyMarker to.
         * @return {void}    void     
         */
        ,
        set: function set(map) {
            this._data.map = map;
        }
        /**
         * template
         * @return {String} A template string for this PropertyMarker's InfoWindow.
         */

    }, {
        key: 'template',
        get: function get() {
            return this._data.template ? this._data.map : '\n            <div class="info-window">\n                <strong>' + this.address.full + '</strong>\n            </div>\n        ';
        }
        /**
         * template = template
         * Set a template for this PropertyMarker's InforWindow.
         * @param  {String} template A template String to use for this PropertyMarker's InfoWindow.
         * @return {void}    void          
         */
        ,
        set: function set(template) {
            this._data.template = template;
        }
        /**
         * infoWindow
         * @return {google.InfoWindow} This PropertyMarker's Google Maps InfoWindow.
         */

    }, {
        key: 'infoWindow',
        get: function get() {
            return this.template;
        }
    }]);

    return PropertyMarker;
}();

exports.default = PropertyMarker;