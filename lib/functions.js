'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});
/**
 * functions
 * A set of helper mehtods for use in DPG api objects.
 * @module  functions
 */
exports.default = {
    /**
      * fn.months()
    * Get an array of three letter month names.
    * @return {Array}   'Jan', 'Feb', 'Mar'...
    */
    months: function months() {
        return ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    },

    /**
    * fn.weekdays()
    * An array of weekday names.
    * @return {Array} 'Sunday', 'Monday', 'Tuesday'...
    */
    weekdays: function weekdays() {
        return ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    },

    /**
     * fn.ucwords( str )
     * Capitalizes {str}'s words.
     * @param  {String} str     a string to capitalize
     * @return {String}         A String To Capitalize
     */
    ucwords: function ucwords(str) {
        return str.toLowerCase().replace(/\b./g, function (a) {
            return a.toUpperCase();
        });
    },

    /**
     * fn.isAfterToday( dateStr )
     * Checks if a dateStr occurs today or in the future.  
     * @param  {String} dateStr A dateStr to compare to today.  
     * @return {Boolean}    This date is today or later.
     */
    isAfterToday: function isAfterToday(dateStr) {
        var afterToday = false;
        if (dateStr) {
            var date = new Date(dateStr.substring(0, 11)).setHours(0, 0, 0, 0);
            afterToday = date >= new Date().setHours(0, 0, 0, 0);
        }
        return afterToday;
    }
};