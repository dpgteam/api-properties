'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _functions = require('./functions');

var _functions2 = _interopRequireDefault(_functions);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/**
 * Agent
 * Interact with data for a single real estate agent.
 * @module  Agent
 */
var Agent = function () {
    /**
     * constructor ( data )
     * Passes {data} to instance.
     * @category Constructor
     * @param  {Object} data    An Object containing data about a real estate agent.
     * @return {void}    void
     */
    function Agent(data) {
        _classCallCheck(this, Agent);

        this.agent = data;
    }
    /**
     * agent
     * The agent's full data.
     * @category Data
     * @return {Object}     This Agent's imported data.
     */


    _createClass(Agent, [{
        key: 'agent',
        get: function get() {
            return this._agent;
        }
        /**
         * agent = data
         * Sets new data for this Agent.
         * @category Data
         * @param  {Object} data The real estate agent data to set.
         * @return {void}    void      
         */
        ,
        set: function set(data) {
            this._agent = {
                id: data.agent.id,
                name: data.agent.name,
                mobile: data.agent.mobile,
                email: data.agent.email_aliases[0].email,
                photo_url: data.agent.photo_url
            };
        }
        /**
         * id
         * This agent's unique id.
         * @category Properties
         * @return {Number} This Agent's unique id.
         */

    }, {
        key: 'id',
        get: function get() {
            return this._agent.id;
        }
        /**
         * name
         *
         * @category Properties
         * @return {String} This Agent's full name.
         */

    }, {
        key: 'name',
        get: function get() {
            return this._agent.name;
        }
        /**
         * mobile
         *
         * @category Properties
         * @return {String} This Agen't mobile telephone number.
         */

    }, {
        key: 'mobile',
        get: function get() {
            return this._agent.mobile;
        }
        /**
         * email
         *
         * @category Properties
         * @return {String} This Agent's email address.
         */

    }, {
        key: 'email',
        get: function get() {
            return this._agent.email;
        }
        /**
         * photo_url
         *
         * @category Properties
         * @return {String} A url for this Agent's photograph.
         */

    }, {
        key: 'photo_url',
        get: function get() {
            return this._agent.photo_url;
        }
    }]);

    return Agent;
}();

exports.default = Agent;