var fs = require('fs');
var DocBlock = require('docblock');
var srcList = [
	'Property',
	'Dpg',
	'functions',
	'Properties',
	'Agent',
	'Address',
	'PropertyMarker',
	'Auction',
	'Inspection',
];
let cb = (src) => { 
	console.log('Writing docs for ' + src + '...');
};
let db = new DocBlock();
let docs = {};
for ( let src of srcList ) {
	let code = fs.readFileSync( './src/' + src + '.js');
	docs[src] = db.parse(code, 'js');
	fs.writeFileSync('../dpg-docs/src/docs/data/docs.json', JSON.stringify(docs, null, '    '), cb(src));
}