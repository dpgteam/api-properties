import fn from './functions';
/**
 * Address
 * Interact with data for a single address.
 * @module  Address
 * @link https://platform.digitalpropertygroup.com/api/v1/properties/23199?api_token=KoAViLFXjXR5CTnSCRMaJzn7caBplNe3yvfkRCrff3thKtpQQAwTRd68Y0cd  Example Request
 */
class Address {
    /**
     * constructor ( data )
     * 
     * @category Constructor
     * @param  {Object} data An Object containing Address data.
     * @return {void}    void      
     */
    constructor ( data ) {
        this._data = data ? data : {};
    }
    /**
     * data
     * The input data for this address.
     * @category Properties
     * @return {Object}
     */
    get data() {
      return this._data;
    }
    /**
     * data = data
     * Sets this Address's data.
     * @category Setters
     * @param  {Object} data    The data you want to mass assign.
     * @return {void}    void
     */
    set data(data) {
      this._data = data;
    }
    /**
     * exists
     * @category Checks
     * @return {Boolean}    This Address has data.
     */
    get exists () {
      return Object.keys(this._data).filter(key => this._data[key] !== null).length > 0;
    }
    /**
     * full
     * @category Computed
     * @return {String}     Returns this Address as a pre-formatted string.
     */
    get full () {
        let address = '';
        if ( this.exists ) {
            if ( this.sub_number ) {
                address+= this.sub_number + '/';
            }
            if ( this.number ) {
                address+= this.number + ' ';
            }
            address+= this.street + ', ';
            address+= this.suburb + ' ';
            address+= this.postcode;
        }
        return address;
    }
    /**
     * sub_number
     * @category Properties
     * @return {Number}     This address's apartment / unit number, if it has one.
     */
    get sub_number () {
      return this._data.sub_number ? parseInt( this._data.sub_number ) : null;
    }
    /**
     * lot_number
     * @category Properties
     * @return {Number}     This address's street lot number, if it has one.
     */
    get lot_number () {
      return this._data.lot_number ? parseInt( this._data.lot_number ) : null;
    }
    /**
     * number
     * @category Properties 
     * @return {Number} This address's street number.
     */
    get number () {
      return this._data.number ? parseInt( this._data.number ) : null;
    }
    /**
     * street
     * @category Properties
     * @return {String}    This address's street.
     */
    get street () {
      return this._data.street ? fn.ucwords(this._data.street) : null;
    }
    /**
     * suburb
     * @category Properties
     * @return {String}     This address's suburb.
     */
    get suburb () {
      return this._data.suburb ? fn.ucwords(this._data.suburb) : null;
    }
    /**
     * state
     * @category Properties
     * @return {String}     This address's state acronym.
     */
    get state () {
      return this._data.state ? this._data.state : null;
    }
    /**
     * postcode
     * @category Properties
     * @return {String} This address's postcode.
     */
    get postcode () {
      return this._data.postcode ? this._data.postcode : null;
    }
    /**
     * lat
     * @category Properties
     * @return {String} This address's latitude.
     */
    get lat () {
      return this._data.lat ? this._data.lat : null;
    }
    /**
     * lon
     * @category Properties
     * @return {String} This address's longitude.
     */
    get lon () {
      return this._data.lon ? this._data.lon : null;
    }
    /**
     * coords
     * @category ComputedProperties
     * @return {Object} This address's lat / lon coordinates.
     */
    get coords () {
      return ( this.lat && this.lon ) ? { lat: this.lat,  lon: this.lon } : null;
    }
}
export default Address;
