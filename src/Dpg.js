import fn from './functions';
import Address from './Address';
import Agent from './Agent';
import Auction from './Auction';
import Inspection from './Inspection';
import Properties from './Properties';
import Property from './Property';
import PropertyMarker from './PropertyMarker';
import Scratch from './Scratch';

/**
 * Dpg Bootstrap
 * Bootstraps Dpg Classes to Dpg Object.
 * @module  Dpg
 * @link functions
 * @link Address
 * @link Agent
 * @link Auction
 * @link Inspection
 * @link Properties
 * @link Property
 * @link PropertyMarker
 */
let Dpg = {
    fn,
    Address,
    Agent,
    Auction,
    Inspection,
    Properties,
    Property,
    PropertyMarker,
    Scratch,
};
export default Dpg;
