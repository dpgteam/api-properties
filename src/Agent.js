import fn from './functions';
/**
 * Agent
 * Interact with data for a single real estate agent.
 * @module  Agent
 */
class Agent {
    /**
     * constructor ( data )
     * Passes {data} to instance.
     * @category Constructor
     * @param  {Object} data    An Object containing data about a real estate agent.
     * @return {void}    void
     */
    constructor ( data ) {
        this.agent = data;
    }
    /**
     * agent
     * The agent's full data.
     * @category Data
     * @return {Object}     This Agent's imported data.
     */
    get agent() {
        return this._agent;
    }
    /**
     * agent = data
     * Sets new data for this Agent.
     * @category Data
     * @param  {Object} data The real estate agent data to set.
     * @return {void}    void      
     */
    set agent(data) {
        this._agent = {
            id       : data.agent.id,
            name     : data.agent.name,
            mobile   : data.agent.mobile,
            email    : data.agent.email_aliases[0].email,
            photo_url: data.agent.photo_url,
        };
    }
    /**
     * id
     * This agent's unique id.
     * @category Properties
     * @return {Number} This Agent's unique id.
     */
    get id () {
        return this._agent.id;
    }
    /**
     * name
     *
     * @category Properties
     * @return {String} This Agent's full name.
     */
    get name () {
        return this._agent.name;
    }
    /**
     * mobile
     *
     * @category Properties
     * @return {String} This Agen't mobile telephone number.
     */
    get mobile () {
        return this._agent.mobile;
    }
    /**
     * email
     *
     * @category Properties
     * @return {String} This Agent's email address.
     */
    get email () {
        return this._agent.email;
    }
    /**
     * photo_url
     *
     * @category Properties
     * @return {String} A url for this Agent's photograph.
     */
    get photo_url () {
        return this._agent.photo_url;
    }
}
export default Agent;
