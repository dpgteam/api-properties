
class Scratch {
    /**
     * This property's category: sale|lease|commercial.
     * @return {String}
     */
    get category() {
        let category = '';
        let categories = [ 'sale', 'lease', 'commercial_sale', 'commercial_lease' ];
        switch ( this._data.property_type ) {
            case 'residential':
                category = 'sale';
                break;
            case 'rental':
                category = 'lease';
                break;
            case 'commercial':
                category = this._data.price > 0 ? 'commercial_sale' : '_lease';
                break;
        }
        return category;
    }
}
