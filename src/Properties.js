import fn from './functions';
import Property from './Property';
/**
 * Properties
 * Prototype for a list of properties obtained from the DPG Platform API.
 * Provides methods for filtering and sorting propertires based on their
 * attributes.
 *     - Sort properties by price, feature, suburb, postcode...
 *     - Filter property by category, type, price, features...
 *  @module  Properties
 *  @link https://platform.digitalpropertygroup.com/api/v1/properties/?api_token=KoAViLFXjXR5CTnSCRMaJzn7caBplNe3yvfkRCrff3thKtpQQAwTRd68Y0cd&orderBy=created_at,desc   Sample API Property request.
 */
 
class Properties {
    /**
     * constructor ( properties )
     * Creates a new instance and adds property data from {properties}. 
     * @category Constructor
     * @param  {Array} properties 
     * @return {void}    void            
     */
     constructor ( properties ) {
        this._properties = [];
        this.filters     = {};
        if ( properties.length !== undefined ) {
          for ( let property of properties ) {
            this.push(property);
          }
        }
    }
    /**
     * push ( Property )
     * Creates new Property and if it exists,  pushes it to the property list.
     * @category Methods
     * @param  {Property} property
     * @return {void}    void
     */
    push( property ) {
        property = new Property(property);
        if ( property.exists )
            this._properties.push(property);
    }
    /**
     * length
     * The total number of properties this list contains.
     * @category Properties
     * @return {Number}
     */
    get length () {
        return this._properties.length;
    }
    /**
     * filterLength
     * The total number of properties this list has which match the current search parameters.
     * @category Properties
     * @return {Number}
     */
    get filterLength () {
        return this.list.length;
    }
    /**
     * Property.hasProperties
     * This list contains properties.
     * @category Properties
     * @return {Boolean}
     */
    get hasProperties () {
        return this.length > 0;
    }
    /**
     * sorted
     * This list sorted by current search options.
     * @category Sorting
     * @return {Array}
     */
    get sorted () {
        return this.sortByKey( this._sort.key, this._sort.asc );
    }
    /**
     * list
     * A list of properties that match the search filters applied.
     * @category Filters
     * @return {Array}
     */
    get list () {
        let props = this._properties
                .filter( this.filterByCategory.bind(this) )
                .filter( this.filterByType.bind(this) )
                // .filter( this.filterByMin.bind(this)        )
                // .filter( this.filterByMax.bind(this)   )
                // .filter( this.filterByBedrooms.bind(this)   )
                // .filter( this.filterByBathrooms.bind(this)  )
                // .filter( this.filterByCarspaces.bind(this)  )
                // .filter( this.filterByAuction.bind(this)    )
                // .filter( this.filterByInspection.bind(this) )
                ;
        // props.forEach(p => console.log(p.type));
            
        return props;
    }
    /**
     * categoryCount
     * A list of categories found in this list and the number of properties for each.
     * @category Counts
     * @return {Object}
     */
    get categoryCount() {
        let counts = {
            sale : 0, 
            lease: 0,
            commercial_sale : 0,
            commercial_lease: 0,
        };
        this.list.forEach( prop => counts[prop.category] += 1 );
        counts.total = Object.keys(counts).reduce((a, val) => a + counts[val], 0);
        return counts;
    }
    /**
     * typeCount
     * A list of types found in this list and the number of properties for each.
     * @category Counts
     * @return {Object} 
     */
    get typeCount() {
        let counts = {};
        this.list.forEach( prop => counts[prop.type] = 0 );
        this.list.forEach( prop => counts[prop.type] += 1 );
        counts.total = Object.keys(counts).reduce((a, val) => a + counts[val], 0);
        return counts;
    }
    /**
     * priceCount
     * The number of properties in this list priced 
     * per $100,000 - sale | per $100 - rental 
     * up to $900,000 | $900.
     * @category Counts
     * @return {Object}
     */
    get priceCount() {
        let counts = {};
        for ( let i = 1; i < 10; i++) {
            counts[i * 100000] = 0;
        }
        this.list.forEach( prop => {
            let key = parseInt(prop._data.price / 100000) * 100000;
            key = key <= 900000 ? key : 900000;
            counts[key] += 1;
        } );
        return counts;
    }
    /**
     * priceAverage
     * The average property price for this list's filtered results.
     * @category Properties
     * @return {Number} 
     */
    get priceAverage() {
        let avg = this.list.reduce( (x, val) => x + val._data.price, 0) / this.list.length;
        return parseInt(Math.round(avg / 1000)) * 1000; 
    }
    /**
     * sorting
     * This list's current sort order.
     * @category Sorting
     * @return {Object} 
     */
    get sorting () {
        return this._sorting ? this._sorting : { key: 'price', asc: true };
    }
    /**
     * sorting = Object
     * Update this list's sort order.
     * @category Sorting
     * @param  {Object} sorting 
     * @return {void}    void         
     */
    set sorting ( sorting ) {
        this._sorting = {
            key  : sorting.key   ? sorting.key   : 'price',
            price: sorting.price ? sorting.price : true,
        };
    }
    /**
     * sortByKey ( key, asc )
     * Sort this list of properties by {key}.  
     * @category Sorting
     * @param  {String}  key
     * @param  {Boolean} asc
     * @return {Array}
     */
    sortByKey(key, asc = true) {
        return asc ? this._properties.sort( (a, b) => a[key] < b[key] ) 
                   : this._properties.sort( (a, b) => a[key] > b[key] );
    }
    /**
     * minRent
     * The property in this list with the highest rent amount.
     * @category Properties
     * @return {Property} 
     */
    get minRent () {
        return this.sortByKey('rent')[0];
    }
    /**
     * maxRent
     * The property in this list with the highest rent amount.
     * @category Properties
     * @return {Property} 
     */
    get maxRent () {
        return this.sortByKey('rent', false)[0];
    }
    /**
     * minPrice
     * The property in this list with the lowest price.
     * @category Properties
     * @return {Property}
     */
    get minPrice () {
        return this.sortByKey('price')[0];
    }
    /**
     * maxPrice
     * The property in this list with the highest price.
     * @category Properties
     * @return {Property}
     */
    get maxPrice () {
        return this.sortByKey('price', false)[0];
    }
    /**
     * Property.max
     * The filtered property with the highest lease | rent amount.
     * @category Properties
     * @return {Property}
     */
    get max () {
        return this.maxPrice.price > 0 ? this.maxPrice : this.leasePrice;
    }
    /**
     * filters
     * This list's current or default search filters.
     * @category Filters
     * @return {Object} 
     */
    get filters () {
        if ( ! this._filters ) {
            this._filters = {};
        }
        return this._filters;
    }
    /**
     * filters = Object
     * Update this list's search filters.
     * @category Filters
     * @param  {Object} filters 
     * @return {void}    void         
     */
    set filters ( filters ) {
        this._filters = {
            category   : filters.category    ? filters.category    : 'sale',
            type       : filters.type        ? filters.type        : null,
            price_min  : filters.price_min   ? filters.price_min   : 0,
            price_max  : filters.price_max   ? filters.price_max   : 0,
            bed        : filters.bed         ? filters.bed         : 0,
            bath       : filters.bath        ? filters.bath        : 0,
            car        : filters.car         ? filters.car         : 0,
            auction    : filters.auction     ? filters.auction     : false,
            inspections: filters.inspections ? filters.inspections : false,
        };
    }
    /**
     * filterByCategory ( property )
     * {property} category matches selected category.
     * @category Filters
     * @param  {Property} property
     * @return {Boolean}
     */
     filterByCategory ( property ) {
        return property.category.toLowerCase() === this.filters.category.toLowerCase();
    }
    /**
     * filterByType ( property )
     * {property} type matches selected type.
     * @category Filters
     * @param  {Property} property
     * @return {Boolean}
     */
     filterByType ( property ) {
        let type = this.filters.type;
        return type ? property.type.toLowerCase() === type.toLowerCase() : true;
    }
     /**
     * filterByType ( property )
     * {property} rent exceeds minimum.
     * @category Filters
     * @param  {Property} Property
     * @return {Boolean}
     */   
    filterByMinRent( property ) {
        return this.filters.price_min ? property.weeklyRent >= this.filters.price_min : true;
    }
    /**
     * filterMinPrice ( property )
     * {property} price exceeds minimum.
     * @category Filters
     * @param  {Property} Property
     * @return {Boolean}
     */   
    filterByMinPrice( property ) {
        return this.filters.price_min ? property.price >= this.filters.price_min : true;
    }
    /**
     * filterMaxRent ( property )
     * {property} rent does not exceed maximum.
     * @category Filters
     * @param  {Property} Property
     * @return {Boolean}
     */
    filterByMaxRent( property ) {
        return this.filters.price_max ? property.weeklyRent <= this.filters.price_max : true;
    }
    /**
     * filterByMaxPrice ( property )
     * {property} price does not exceed maximum.
     * @category Filters
     * @param  {Property} Property
     * @return {Boolean}
     */
    filterByMaxPrice( property ) {
        return this.filters.price_max ? property.price <= this.filters.price_max : true;
    }
    /**
     * filterByMix ( property )
     * {property} price|rent exceeds minimum price | rent.
     * @category Filters
     * @param  {Property} Property
     * @return {Boolean}
     */
     filterByMin ( property ) {
        return property.isSales ? this.filterByMinPrice(property) : this.filterByMinRent(property);
    }
    /**
     * filterByTMax ( property )
     * {property} price|rent does not exceed maximum
     * @category Filters
     * @param  {Property} property
     * @return {Boolean}
     */
     filterByMax ( property ) {
        return property.isSales ? this.filterByMaxPrice(property) : this.filterByMaxRent(property);
    }
    /**
     * filterByBedrooms ( property )
     * {property} has minimum bedrooms.
     * @category Filters
     * @param  {Property} property
     * @return {Boolean}
     */
     filterByBedrooms ( property ) {
        let bed;
        return bed && property.isResidential ? property.bedrooms >= bed : true;
    }
    /**
     * filterByBathrooms ( property )
     * {property} has minimum bathrooms.
     * @category Filters
     * @param  {Property} property
     * @return {Boolean}
     */
     filterByBathrooms ( property ) {
        let bath = this._filters.bath;
        return bath && property.isResidential ? property.bathrooms >= bath : true;
    }
    /**
     * filterByCarspaces ( property )
     * {property} has minimum car spaces.
     * @category Filters
     * @param  {Property} property
     * @return {Boolean}
     */
     filterByCarspaces ( property ) {
        let car = this.filters.car;
        return car && property.isResidential ? property.carspace >= car : true;
    }
    /**
     * filterByAuction ( property )
     * {property} has future auction.
     * @category Filters
     * @param  {Property} property
     * @return {Boolean}
     */
     filterByAuction ( property ) {
        return this.filters.auctions ? property.hasAuction : true;
    }
    /**
     * filterByInspection ( property )
     * {property} has future inspections.
     * @category Filters
     * @param  {Property} property
     * @return {Boolean}
     */
     filterByInspection ( property ) {
        return this.filters.inspections ? property.hasInspections : true;
    }
}
export default Properties;
