import fn from './functions';
/**
 * Auction
 * Interact with data for a Property auction.
 * @module  Auction
 */
class Auction {
    /**
     * constructor ( dateStr )
     * Parses {dateStr} and sets this Auction's properties.
     * @param  {String} dateStr A String containing the Auction's date and time.
     * @return {void}    void
     */
    constructor ( dateStr ) {
        this._data = null;
        if ( dateStr ) {
            this.parseAuctionDate( dateStr );
        }
    }
    /**
     * parseAuctionDate ( dateStr )
     * If {dateStr} is in the future, it is parsed and an object of date and time data is created.
     * @param {String} dateStr
     * @return {Object} Pre-formatted time and date data.
     */
    parseAuctionDate(dateStr) {
        let auction = null;
        if ( dateStr !== null ) {
            if ( fn.isAfterToday(dateStr) ) {
                auction = {};
                let date = new Date(dateStr.substring(0, 10));
                /* Extract, time data. */
                auction.date     = date;
                auction.time     = dateStr.substring(11);
                auction.hour     = auction.time.substring(0, auction.time.indexOf(':'));
                auction.minute   = auction.time.substring( auction.time.lastIndexOf(':') -2, auction.time.lastIndexOf(':') );
                auction.am       = auction.hour < 12 ? 'am' : 'pm';
                auction.timestamp = auction.date.setHours(auction.hour, auction.minute, 0, 0);
                /* Format hour if PM. */
                if ( auction.am === 'pm' ) {
                    auction.hour-=12;
                }
                auction.str     = dateStr.substring(0, 10);
                auction.weekday = fn.weekdays()[date.getDay()];
                auction.date    = date.getDate();
                auction.month   = fn.months()[date.getMonth()];
                auction.year    = date.getFullYear();
            }
        }
        this._data = auction;
        return this._data;
    }
    /**
     * month
     * @return {String} This Auction's month formatted to a three letter String.
     */
    get month() {
        return this._data ? this._data.month : null;
    }
    /**
     * weekday
     * @return {String} The weekday this Auction will take place.
     */
    get weekday() {
        return this._data ? this._data.weekday : null;
    }
    /**
     * am
     * @return {String} This auction takes place in 'am' or 'pm'.
     */
    get am() {
        return this._data ? this._data.am : null;
    }
    /**
     * hour
     * @return {Number} The hour this Auction takes place.
     */
    get hour() {
        return this._data ? parseInt(this._data.hour) : null;
    }
    /**
     * minute
     * @return {String} The minute this auction takes place.
     */
    get minute() {
        return this._data ? this._data.minute : null;
    }
    /**
     * date
     * @return {Number} This Auction's Unix timestamp.
     */
    get date() {
        return this._data ? parseInt(this._data.date) : null;
    }
    /**
     * exists
     * @return {Boolean} True if this Auction has valid data.
     */
    get exists () {
        return this._data !== null;
    }
}
export default Auction;
