import fn from './functions';
/**
 * Inspection
 * Interact with data for a single Property inspection.
 * @module  Inspection
 */
class Inspection {
    /**
     * constructor ( data )
     * Creates a new Inspection instance with {data}.
     * @param  {Object} data    An Object containing property inspection data.
     * @return {void}    void
     */
    constructor ( data ) {
        this._data = data;
        this.parseInspectionDate(this._data.inspection_time);
    }
    /**
     * parseInspectionData ( dateStr )
     * If {dateStr} is in the future, it is parsed and an object of date and time data is created.
     * @param  {String} dateStr
     * @return {Object} Pre-formatted time and date data.
     */
    parseInspectionDate(dateStr) {
        /* Split string into substrings. */
        let str   = dateStr.substring(0, 11),
            times = dateStr.substring(11),
            start = times.substring(0, times.indexOf('to')).replace(/ /g,''),
            end   = times.substring(times.indexOf('to') + 2).replace(/ /g,'');
        /* Parse am|pm to var. */
        let am = start.substring(start.length - 2);
        /* Create Start time Object. */
        this._start = {
            time  : start,
            hour  : start.substring(0, start.indexOf(':')),
            minute: start.substring(start.indexOf(':') + 1).replace(/a|A|p|P|m|M/g,''),
            am    : am,
        };
        /* Create End time Object. */
        this._end = {
            time  : end,
            hour  : end.substring( 0, end.indexOf(':') ),
            minute: end.substring( end.indexOf(':') + 1 ).replace( /a|A|p|P|m|M/g,'' ),
            am    : end.substring( end.length - 2 ),
        };
        /* Convert date str to timestamp and human readable with start time. */
        let dateStamp = new Date(str).setHours( am = 'am' ? this.start.hour : this.start.hour + 12, this.start.minute, 0 , 0 );
        let date = new Date(dateStamp);
        /* Create date object. */
        this._date = {
            str,
            dateTime: dateStamp,
            weekday: fn.weekdays()[date.getDay()],
            date: date.getDate(),
            month: fn.months()[date.getMonth()],
            year: date.getFullYear(),
        };
    }
    /**
     * date
     * @return {Object} This Inspection's date.
     */
    get date() {
        return this._date;
    }
    /**
     * start
     * @return {Object} This inspection's start time.
     */
    get start() {
        return this._start;
    }
    /**
     * end
     * @return {Object} This inspection's end time.
     */
    get end() {
        return this._end;
    }
}
export default Inspection;
