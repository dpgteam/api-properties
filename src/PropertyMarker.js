import fn from './functions';
/**
 * Class prototype to handle map markers for a Property.
 * @module PropertyMarker
 */
class PropertyMarker {
    /**
     * constructor ( data )
     * Pass in the data to use for this PropertyMarker
     * @param  {Object} data 
     * @return {void}    void      
     */
    constructor ( data ) {
        this._data = data;
    }
    get marker () {
        return new google.maps.Marker({
            position  : this.coords,
            map       : this.map,
            title     : this.title,
            infoWindow: this.infoWindow,
        });
    }
    /**
     * title
     * @return {String} This PropertyMarker's title.
     */
    get title () {
        return this._data.title;
    }
    /**
     * title = title
     * Set a title for this PropertyMarker.
     * @param  {String} title Set a title for this PropertyMarker.
     * @return {void}    void       
     */
    set title ( title ) {
        this._data.title = title;
    }
    /**
     * coords
     * @return {Object} This PropertyMarker's map coordinates.
     */
    get coords () {
       return this._data.address.coords;
    }
    /**
     * address
     * @return {Address} This PropertyMarker's address data.
     */
    get address () {
        return this._data.address ? this._data.address : null;
    }
    /**
     * address = address
     * Set an Address for this PropertyMarker.
     * @param  {Address} address This PropertyMarker's Address data.
     * @return {void}    void         
     */
    set address ( address ) {
        this._data.address = address;
    }
    /**
     * map
     * @return {google.map} The map this PropertyMarker is attached to.
     */
    get map () {
        return this._data.map ? this._data.map : null;
    }
    /**
     * map = map
     * @param  {google.map} map The map to attach this PropertyMarker to.
     * @return {void}    void     
     */
    set map ( map ) {
        this._data.map = map;
    }
    /**
     * template
     * @return {String} A template string for this PropertyMarker's InfoWindow.
     */
    get template() {
        return this._data.template ? this._data.map : `
            <div class="info-window">
                <strong>${ this.address.full }</strong>
            </div>
        `;
    }
    /**
     * template = template
     * Set a template for this PropertyMarker's InforWindow.
     * @param  {String} template A template String to use for this PropertyMarker's InfoWindow.
     * @return {void}    void          
     */
    set template ( template ) {
        this._data.template = template;
    }
    /**
     * infoWindow
     * @return {google.InfoWindow} This PropertyMarker's Google Maps InfoWindow.
     */
    get infoWindow() {
        return this.template;
    }
}
export default PropertyMarker;
