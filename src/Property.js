import fn from './functions';
import PropertyMarker from './PropertyMarker';
import Inspection from './Inspection';
import Auction from './Auction';
import Agent from './Agent';
import Address from './Address';
/**
 * Property
 * Contains data for a single property and a set of dynamic getter to 
 * retrieve only relevant property data.
 *     Examples: 
 *         - Only future auctions | inspections.
 *         - Correct price | rent to display.
 * @link https://platform.digitalpropertygroup.com/api/v1/properties/23199?api_token=KoAViLFXjXR5CTnSCRMaJzn7caBplNe3yvfkRCrff3thKtpQQAwTRd68Y0cd  Example Request
 * @type {Object}
 */
 
class Property {
    /**
     * constructor ( data )
     * Creates a new Property instance and setups empty properties.
     * @category Constructor
     * @param  {Object} data An object containing Property data.
     * @return {void}    void      
     */
    constructor ( data ) {
        this._data        = data;
        this._inspections = [];
        this._auction     = null;
        this._address     = null;
        this._images      = null;
    }
    /**
     * property
     * @category Data
     * @return {Object} The property's data.
     */
    get property () {
        return this._data ? this._data : null;
    }
    /**
     * exists
     * @category Checks
     * @return {Boolean}    This property contains data.
     */
    get exists () {
      return this.property && Object.keys(this.property).length > 0;
    }
    /**
     * isSale
     * @category Checks
     * @return {Boolean}    This property is for sale.
     */
    get isSale () {
        return this._data.price > 0;
    }
    /**
     * isLease
     * @category Checks
     * @return {Boolean}    This property is for lease.
     */
    get isLease () {
        return ! this.isSale;
    }
    /**
     * isCommercial
     * @category Checks
     * @return {Boolean}    This is a commercial site.
     */
    get isCommercial () {
        return this._data.property_type === 'commercial';
    }
    /**
     * isResidential
     * @category Checks
     * @return {Boolean}    This is a residential site.
     */
    get isResidential () {
        return ['residential', 'rental'].indexOf(this.property.property_type.toLowerCase()) > -1;
    }
    /**
     * category
     * @category Properties
     * @return {String} This property's category: sale|lease|commercial.
     */
    get category () {
        let category = '';
        if ( this.isResidential ) {
            category = this.isSale ? 'sale' : 'lease';
        } else {
            category = this.isSale ? 'commercial_sale' : 'commercial_lease';
        }
        return category;
    }
    /**
     * type
     * @category Properties
     * @return {String}     This Property's type.
     */
    get type () {
        let type = this._data.category;
        if ( ! type ) // fallback to property_type if null.
            type = this._data.property_type;
        return type.toLowerCase() === 'apartment' ? 'unit' : type;
    }
    /**
     * id
     * @category Properties
     * @return {String} This property's unique id.
     */
    get id () {
        return this.property.property_unique_id;
    }
    /**
     * branch
     * @category Properties
     * @return {String}     The unique id for the branch this property belongs to.
     */
    get branch () {
        return this.property.branch_id;
    }
    /**
     * status
     * @category Properties
     * @return {String} This property's listing status: current | sold | leased.
     */
    get status () {
        return this.property.property_status;
    }
    /**
     * headline
     * @category Properties
     * @return {String}     This property's headline.
     */
    get headline () {
        return this.property.headline;
    }
    /**
     * description
     * @category Properties
     * @return {String} This Property's description with newlines replace with paragraph tags.
     */
    get description () {
        let desc = '<p>' + this.property.description + '</p>';
        return desc.replace(/\n/g, '</p><p>');
    }
    /**
     * video
     * @category Properties
     * @return {String} A url to a video of this Property, if there is one.
     */
    get video () {
      return this.property.video_link;
    }
    /**
     * coords
     * @category Properties
     * @return {Object} This Property's map coordinates.
     */
    get coords () {
        return {
            lat: this._data.lat,
            lon: this._data.lon,
        };
    }
    /**
     * address
     * @category Properties
     * @return {Address}     This Property's Address data.
     */
    get address () {
        if ( ! this._address ) {
            this._address = new Address({
                sub_number: this.property.address_sub_number,
                lot_number: this.property.address_lot_number,
                number    : this.property.address_street_number,
                street    : fn.ucwords(this.property.address_street),
                suburb    : fn.ucwords(this.property.address_suburb),
                state     : this.property.address_state,
                postcode  : this.property.address_postcode,
                coords    : this.coords,
            });
        }
        return this._address;
    }
    /**
     * flooplan
     * @category Properties
     * @return {String} A url to this Property's floor plan, if there is one.
     */
    get floorplan () {
        let floorplan = this.property.property_objects.filter( a => a.object_type === 'floorplan')[0];
        return floorplan !== undefined ? floorplan.object_url : null;
    }
    /**
     * images
     * @category Properties
     * @return {Array}  A list of urls for this Property's images.
     */
    get images () {
        if ( ! this._images ) {
            this._images = [];
            this.property.property_images.forEach( img => this._images.push(img.url) );
        }
        return this._images;
    }
    /**
     * hasInspections
     * @category Checks
     * @return {Boolean}    This Property has future inspections.
     */
    get hasInspections () {
        return this.inspections[0] !== null;
    }
    /**
     * hasAuction
     * @category Checks
     * @return {Boolean} This Property has a future auction.
     */
    get hasAuction () {
        return this.auction.exists ? this.auction.exists : false;
    }
    /**
     * hasInspectionsOrAuctions
     * @category Checks
     * @return {Boolean}    This Property has future inspections or auctions.
     */
    get hasInspectionsOrAuctions () {
        return this.hasInspections || this.hasAuction;
    }
    /**
     * inspections
     * @category ListObjects
     * @return {Array}  This Property's upcoming inspections, if there are any.
     */
    get inspections () {
        if ( ! this._inspections.length )
            for ( let inspection of this.property.property_inspection_times ) {
                if ( fn.isAfterToday( inspection.inspection_time ) ) {
                    this._inspections.push( new Inspection(inspection) );
                }
                this._inspections = this._inspections.length ? this._inspections : [null];
            }
        return this._inspections;
    }
    /**
     * auction
     * @category ListObjects
     * @return {String} This Property's future auction date, if there is one.
     */
    get auction () {
        if ( ! this._auction ) {
            this._auction = new Auction(this.property.auction_date);
        }
        return this._auction;
    }
    /**
     * available
     * @category Checks
     * @return {Boolean}    This Property is currently available to buy / lease.
     */
    get available () {
        return ['leased', 'sold', 'withdrawn'].indexOf(this.property.property_status) < 0;
    }
    /**
     * getFeature ( featureName )
     * Checks this Property's features and returns the value of {featureName}, if there is one.
     * @category Methods
     * @param  {String} featureName     The name of this Property's feature to retrieve.
     * @return {Number|String}          This Property feature's value.
     */
    getFeature ( featureName ) {
        if ( this.isCommerical ) 
            return;

        let feature = this.property.property_features.filter(
            a => a.feature_name.toLowerCase() === featureName.toLowerCase()
        )[0];
        if ( parseInt(feature.feature_value) ) {
            feature = parseInt(feature.feature_value);
        }

        return feature.feature_value ? feature.feature_value : feature;
    }
    /**
     * bedrooms
     * The number of bedrooms at this property.
     * @category Properties
     * @return {Number}     How many bedrooms this property has.
     */
    get bedrooms () {
        return this.getFeature('bedrooms');
    }
    /**
     * bathrooms
     * The number of bathrooms at this property.
     * @category Properties
     * @return {Number}     How many bathrooms this property has.
     */
    get bathrooms () {
        return this.getFeature('bathrooms');
    }
    /**
     * carspaces
     * The number of total car spaces at this property.
     * @category Properties
     * @return {Number}     How many car spaces this property has.
     */
    get carspaces () {
        return parseInt( this.getFeature('carports') ) + parseInt( this.getFeature('garages') );
    }
    /**
     * weeklyRent
     * @category Properties
     * @return {Number} This Property's weekly rent amount, if there is one.
     */
    get weeklyRent () {
        let rent = this.rent;
        if ( rent.rent_period === 'weekly') {
            rent = rent.rent_amount;
        } else {
            rent = ( rent.rent_amount / 365 ) * 7;
        }
        return rent;
    }
    /**
     * annualRent
     * @category Properties
     * @return {Number}     This Property's annual rent amount, if there is one.
     */
    get annualRent () {
        let rent = this.rent;
        if ( rent.rent_period === 'weekly') {
            rent = ( rent.rent_amount / 7 ) * 365;
        } else {
            rent = rent.rent_amount;
        }
        return parseInt(rent);
    }
    /**
     * rent
     * @category Properties
     * @return {Object}     This Property's rental data.
     */
    get rent () {
        let rent = 0;
        if ( this.property.property_rents ) {
            rent = this.property.property_rents[0];
        }
        return rent;
    }
    /**
     * price
     * @category Properties
     * @return {String}     This Property's display price.
     */
    get price () {
        let price = 0;
        if ( this.property.price_display.toLowerCase() !== 'yes') {
            price = 'Contact Agent';
        } else {
            price = this.property.price_view ? this.property.price_view : '$' + this.property.price.toLocaleString(0);
        }
        return price;
    }
    /**
     * hasAgents
     * @category Checks
     * @return {Boolean}    This Property has real estate agents.
     */
    get hasAgents () {
        return this.agents.length > 0;
    }
    /**
     * agents
     * @category ListObjects
     * @return {Array}  This Property's Agent data.
     */
    get agents () {
        this._agents = [];
        if ( this.property && this.property.property_agents ) {
          this.property.property_agents.forEach(
              agent => this._agents.push( new Agent(agent ) )
          );
        }
        return this._agents;
    }
    /**
     * hasCategory ( category )
     * @category Methods
     * @param  {String}  category The category to check.
     * @return {Boolean}          This Property is in {category}
     */
    hasCategory(category) {
        return category  === null || this.category.toLowerCase() === category.toLowerCase();
    }
    /**
     * hasType ( type )
     * @category Methods
     * @param  {String}  type   The type to check.
     * @return {Boolean}        This Property is in {type}
     */
    hasType(type) {
        return type === null || this.type.toLowerCase() === type.toLowerCase();
    }
    /**
     * hasPriceMin ( min )
     * @category Methods
     * @param  {Number}  min    The minimum Price to check.
     * @return {Boolean}        This Property's price is at least {min} or greater.
     */
    hasMinPrice(min) {
        if ( this.isSale ) {
            return ! min || this.price >= min;
        } else {
            return ! min || this.weeklyRent >= min;
        }
    }
    /**
     * hasPriceMax ( max )
     * @category Methods
     * @param  {Number}  max    The maximum Price to check.
     * @return {Boolean}        This Property's price is no greater than {max}.
     */
    hasMaxPrice(max) {
        if ( this.isSale ) {
            return !max || this.price <= max;
        } else {
            return !max || this.weeklyRent <= max;
        }
    }
    /**
     * hasFeature ( val, featureName )
     * Checks this Property's features for {featureName} and returns true if there are {val} or more.
     * @category Methods
     * @param  {Number}  val         The minimum value this Property's feature should have.
     * @param  {String}  featureName The name of this Property's feature to check.
     * @return {Boolean}             This Property's {featureName} is at least {val}.
     */
    hasFeature(val, featureName) {
        return this.getFeature(featureName) >= val;
    }
}

export default Property;